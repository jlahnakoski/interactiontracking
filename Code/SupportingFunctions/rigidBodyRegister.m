function [R,t,e,out] = rigidBodyRegister(targetPoints,sourcePoints)
%out = rigidBodyRegister(targetPoints,sourcePoints)
%---------------------------------------------------------
%Finds the optimal rigid body transform for minimizing the least squares
%error between two point clouds. The points are assumed to be in the same
%order in both target and source arrays.
%
%Inputs:
%targetPoints:  The target to which the sourcePoints are fitted
%sourcePoints:  The points which must be registered to the target
%
%Outputs:
%R:     Rotation matrix for the rigid body transform
%t:     Translation vector for the rigid body transform
%e:     Mean squared error of the final transformed data
%out:   The tranformed coordinates of the sourcePoints in target space
%
%4.4.2012 Juha Lahnakoski
%juha.lahnakoski@aalto.fi

%Remove the means
targetCenter=mean(targetPoints,2);
sourceCenter=mean(sourcePoints,2);
targetDiffs=targetPoints-repmat(targetCenter,[1 size(targetPoints,2)]);
sourceDiffs=sourcePoints-repmat(sourceCenter,[1 size(sourcePoints,2)]);

%Calculate the covariance matrix
covMtx=(targetDiffs*sourceDiffs.')/size(sourcePoints,2);

%Do the singular value decomposition and create the D and R matrices
[U,S,V]=svd(covMtx);
D=diag([ones(size(sourceCenter,1)-1,1); det(U)*det(V)]);
R=U*D*V.';

%Calculate the translation vector
t=targetCenter-R*sourceCenter;

%Output the transformed points...
out=R*sourcePoints+repmat(t,[1 size(sourcePoints,2)]);
%...and squared errors in a vector
e=(sum((out-targetPoints).^2));

end