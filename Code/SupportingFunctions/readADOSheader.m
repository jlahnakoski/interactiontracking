function [hdr,t0,t02] = readADOSheader(in)
%% [data,hdr] = readADOSheader(in)
% TODO:
% Break up IDs to multiple different ones if there is a gap in the data
% (either in time or place?)

data=[];
f=fopen(in);
line=fgetl(f);
dataS2=[];
k=1;
while ~strcmp(line(1),'!')
    hdr{k}=line;
    if any(strfind(hdr{k},'Local time'))
        t0=str2num(hdr{k}(strfind(hdr{k},':')+1:end));
    end
    k=k+1;
    line=fgetl(f);
end
% first=true;
fprintf('Headers read...\n');
line=fgetl(f);

% lineLen=length(str2num(line));
temp=str2num(line);
t02=temp(2)-(temp(3)-t0);


end
