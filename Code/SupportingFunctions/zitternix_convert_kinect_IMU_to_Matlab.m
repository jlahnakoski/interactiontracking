baseDir='/nas/Juha/ADOS_zitternix/Subjects/DataShare/';
addpath(genpath([baseDir '/Code']))
if ~exist([baseDir '/matDataKinectAndIMU/'],'Dir')
    mkdir([baseDir '/matDataKinectAndIMU/']);
end

d=dir([baseDir '/OriginalData/F*']);
d2=dir([baseDir '/OriginalData/M*']);
d=[d; d2];

subjectNo=1;
%%
for subjectNo=subjectNo:length(d)
    
    subname=d(subjectNo).name;
    d3=dir(sprintf('%s/OriginalData/%s/*.txt',baseDir,subname));
    fprintf('---------------------------------------------------------\n');
    fprintf('Processing subject %s : %i / %i\n',subname,subjectNo,length(d));
    fprintf('---------------------------------------------------------\n');
    [dataSens,keyPresses]=read_zitternix_sensorlog(sprintf('%s/OriginalData/%s/%s',baseDir,subname,d3(1).name));
    
    [dataKin,hedrMaster,t0,t02]=readADOSlog(sprintf('%s/OriginalData/%s/Master/%s',baseDir,subname,d3(1).name));
    [dataKinSlave,hedrSlave,t0s,t02s]=readADOSlog(sprintf('%s/OriginalData/%s/Slave/%s',baseDir,subname,d3(1).name));
    roundtrip=str2num(hedrMaster{4}(strfind(hedrMaster{4},':')+1:end))-str2num(hedrMaster{2}(strfind(hedrMaster{2},':')+1:end));
    alltrip(subjectNo)=roundtrip;
     
    save([baseDir '/matDataKinectAndIMU/' d(subjectNo).name '.mat'],'dataKin','dataKinSlave','dataSens','roundtrip','subname','t0','t02','t0s','t02s','keyPresses');%,'tpoints'
end
save([baseDir '/PreprocessedData/roundTripTimes.mat'],'alltrip');