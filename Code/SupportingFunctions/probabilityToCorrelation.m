function outputVar = probabilityToCorrellation(samplesize,probability)
%Converts probability to correlation coefficient
%i.e. inverse of t-test used in matlab corr-function

DegreesOfFreedom=samplesize-2;
%DegreesOfFreedom=samplesize;
tValue=tinv(probability,DegreesOfFreedom);

outputVar=sqrt((tValue.^2)./(samplesize-2+(tValue.^2)));

end
