function h=BML_violinPlot(inArray,numBins,fCol,nosmooth,boxOff,rawdata,leftHanded)
%Thingy for plotting distributions instead of bar graphs.. might be
%useful at some point.
%Only for 2D (or 1D) data by the way.
%
%REQUIRES:
%Curve Fitting Toolbox
%
%Inputs:
%inArray is the 2D data
%numBins is the number of histogram bins for distribution estimation
%fCol is the face color of the violin plots
%nosmooth can be set to 1 to plot unsmoothed histograms
%
%Output is the handle of the axis, if you need it...
%
%Version 0.0001
%02.04.2013 Juha Lahnakoski

if length(size(inArray))>2
    error('I have no idea what to do with multidimensional arrays, please make the data 2D or something?');
end
if nargin>=4 && ~isempty(nosmooth) && nosmooth==1
    rectPlot=true;
else
    rectPlot=false;
end
if nargin>=5 && ~isempty(boxOff) && boxOff==0
    boxPlots=true;
else
    boxPlots=false;
end
if nargin>=6 && ~isempty(rawdata) && rawdata~=0
    rawdata=true;
else
    rawdata=false;
end
if ~(nargin>=7 && ~isempty(leftHanded))
    leftHanded=[];
end
%if diff(size(inArray))>0
%    inArray=inArray.';
%end;
if nargin<2 || isempty(numBins)
    numBins=9;
elseif max(size(numBins))==1
    numBins=linspace(min(inArray(:)),max(inArray(:)),numBins);
end
if nargin<3 || isempty(fCol)
    fCol=[.7 .7 .7];%zeros(3,1);
end

hold on;
h=gca;
chld=get(h,'children');
xx=0;
for kk=1:length(chld)
    if strcmpi(chld(kk).Type,'patch') %This goes through the patch objects
        xx(kk)=mean(chld(kk).XData); %#ok<AGROW>
    end
end

x=max(xx);%ength(get(h,'children'))/7;%4;

for k=1:size(inArray,2)
    if length(numBins)==1
        %Make the histogram and setup some parameters
        [hbar,hbin]=hist(inArray(:,k),numBins);
        hbin=hbin(:);
        hbar=hbar(:);
        %n = length(hbin);
        w = mean(abs(diff(hbin)));
        %We want to spread the histogram a bit, because the max and min
        %fall at the very edge of the histogram rather than the last bin
        t = [hbin(1)-w/2; (hbin(1:end-1)+hbin(2:end))/2; hbin(end)+w/2];
        dt = diff(t);
        diffs=inArray(:)-round(inArray(:));
        %max(diffs(~isnan(diffs)))
        
        
        
        if max(abs(diffs(~isnan(diffs))))~=0 && ~rectPlot
            
            %This sets the sampling interval for the smoothed histogram
            tVec=linspace(min(t),max(t),5*length(t));%t(1):((t(end)-t(1))/(5*numBins)):t(end);
            %Mirror the datapoints to make a nice two-sided plot
            tVecBF=[tVec tVec(end-1:-1:1)];
            %Make the cumulative distribution
            Fvals = cumsum([0;hbar(:).*dt(:)]);
            %Fit the spline to the "integral"
            F = spline(t(:), [0; Fvals; 0]);
            
            %Calculate the derivative of the spline
            DF = fnder(F(:));
            
            %Sample the values of the derivative at the preset points (tVec)
            DFval=fnval(DF(:),tVec(:)).*(fnval(DF(:),tVec(:))>0);
            %Mirror the distribution similarly as the tVec-points
            DFvalBF=[DFval(:); -DFval(end-1:-1:1)];
            
            %Plot away...
            
            fill(DFvalBF/(3*max(DFvalBF))+k+x,tVecBF,zeros(size(tVecBF)),'faceColor',fCol);
        else
            [hbar,hbin]=hist(inArray(:,k),linspace(min(inArray(:)),max(inArray(:)),numBins));%min(inArray(:)):max(inArray(:)));
            %         dt=diff(hbin(1:2));
            %         tVec=min(hbin)-dt/2:dt:max(hbin)+dt/2;
            DFval(1)=0;
            dt=diff(hbin(1:2));
            tVec=reshape(repmat(min(hbin)-dt/2:dt:max(hbin)+dt/2,[2 1]),[],1)';
            tVecBF=[tVec tVec(end-1:-1:1)];
            DFval(1)=0;
            DFval(length(tVec))=0;
            for hh=1:length(hbin)
                DFval(2*(hh-1)+(2:3))=hbar(floor((hh-1))+1);
            end
            DFvalBF=[DFval -DFval(end-1:-1:1)];
            
            %fill(DFvalBF/(3*max(DFvalBF))+k+x,tVecBF,zeros(size(tVecBF)),'faceColor',fCol);
            fill(DFvalBF/(15*sum(hbar)/length(hbar))+k+x,tVecBF,zeros(size(tVecBF)),'faceColor',fCol);
            
            %barh(hbin,hbar/(3*max(hbar)));
        end
        
    else %Here is for given bin centers rather than equally distributed
        
        %Make the histogram and setup some parameters
        [hbar,hbin]=hist(inArray(:,k),numBins);
        hbin=hbin(:);
        hbar=hbar(:);
        %         n = length(hbin);
        w = mean(abs(diff(hbin)));
        t = [hbin(1)-w/2; (hbin(1:end-1)+hbin(2:end))/2; hbin(end)+w/2];
        dt = diff(t);
        diffs=inArray(:)-round(inArray(:));
        %max(diffs(~isnan(diffs)))
        
        if max(abs(diffs(~isnan(diffs))))~=0 && ~rectPlot
            
            %This sets the sampling interval for the smoothed histogram
            tVec=linspace(min(t),max(t),5*length(t));%t(1):((t(end)-t(1))/(5*numBins)):t(end);
            %Mirror the datapoints to make a nice two-sided plot
            tVecBF=[tVec tVec(end-1:-1:1)];
            %Make the cumulative distribution
            Fvals = cumsum([0;hbar(:).*dt(:)]);
            %Fit the spline to the "integral"
            F = spline(t(:), [0; Fvals; 0]);
            
            %Calculate the derivative of the spline
            DF = fnder(F(:));
            
            %Sample the values of the derivative at the preset points (tVec)
            DFval=fnval(DF(:),tVec(:)).*(fnval(DF(:),tVec(:))>0);
            %Mirror the distribution similarly as the tVec-points
            DFvalBF=[DFval(:); -DFval(end-1:-1:1)];
            
            %Plot away...
            
            fill(DFvalBF/(3*max(DFvalBF))+k+x,tVecBF,zeros(size(tVecBF)),'faceColor',fCol);
        else
            
            [hbar,hbin]=hist(inArray(:,k),numBins);%min(inArray(:)):max(inArray(:)));
            %         dt=diff(hbin(1:2));
            %         tVec=min(hbin)-dt/2:dt:max(hbin)+dt/2;
            DFval(1)=0;
            dt=diff(hbin(1:2));
            tVec=reshape(repmat(min(hbin)-dt/2:dt:max(hbin)+dt/2,[2 1]),[],1)';
            tVecBF=[tVec tVec(end-1:-1:1)];
            DFval(1)=0;
            DFval(length(tVec))=0;
            for hh=1:length(hbin)
                DFval(2*(hh-1)+(2:3))=hbar(floor((hh-1))+1);
            end
            DFvalBF=[DFval -DFval(end-1:-1:1)];
            
            %fill(DFvalBF/(3*max(DFvalBF))+k+x,tVecBF,zeros(size(tVecBF)),'faceColor',fCol);
            fill(DFvalBF/(15*sum(hbar)/length(hbar))+k+x,tVecBF,zeros(size(tVecBF)),'faceColor',fCol);
            
            %barh(hbin,hbar/(3*max(hbar)));
        end
        
    end
    %...mean and median too
    plot(x+k,mean(inArray(~isnan(inArray(:,k)),k)),'k+','markerSize',12,'linewidth',2);%,'line','none'
    plot(x+k,median(inArray(~isnan(inArray(:,k)),k)),'marker','square','markerSize',12,'linewidth',2,'color',[.3 .3 .3]);%,'line','none'
    plot([-.1 .1]+k+x,max(inArray(:,k))*[1 1],'k');
    plot([-.1 .1]+k+x,min(inArray(:,k))*[1 1],'k');
    if boxPlots
        plot([-.25 .25 .25 -.25 -.25]+k+x,[quantile(inArray(:,k),.25)*[1 1] quantile(inArray(:,k),.75)*[1 1] quantile(inArray(:,k),.25)],'k');
    end
    %Plot raw datapoints as dots
    if rawdata
        plot(k+x-.125+.25*(1:length(inArray(:,k)))/length(inArray(:,k)),inArray(:,k),'.','color',[.3 .3 .3]);
    end
    %Plot left-handers or other outlier groups with a larger circle
    if any(leftHanded)
        plot(k+x-.125+.25*(leftHanded/length(inArray(:,k))),inArray(leftHanded,k),'.','color',[0 0 0],'markerSize',20,'markerFaceColor',[0 0 0]);
    end
end

%Then just set the axis and the x-tick locations
if min(inArray(:))>=0
    axis([0 size(inArray,2)+1+x 0 ceil(max(inArray(:))*10)/10]);
else
    axis([0 size(inArray,2)+1+x floor(min(inArray(:))*10)/10 ceil(max(inArray(:))*10)/10]);
end
set(h,'xtick',1:size(inArray,2)+x);


