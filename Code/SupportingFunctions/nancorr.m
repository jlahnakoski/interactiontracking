function rho = nancorr(tc1,tc2)
% out = nancorr(tc1,tc2)
% Calculates Pearson product moment correlation coefficient disregarding
% any NaN entries.
%
% Inputs:
% tc1:  Single vector or matrix of vectors. Correlation is calculated over
%       the first dimension. Full correlation matrix is returned if tc1 is
%       a matrix.
% tc2:  Similar to tc1. If both tc1 and tc2 are matrices all pairwise
%       correlations will be returned.
%
% Output:
% rho: Correlation coefficient or vector/matrix of correlation
%      coefficients.
%
% 29.10.2013 Juha Lahnakoski
% juha.lahnakoski@aalto.fi

if nargin<2
    if size(tc1,2)<2
        error('Single vector passed as input, I think you know what the correlation of something with itself is, let''s not waste processor time on this?');
    end;
    tc1z=tc1-repmat(nanmean(tc1),[size(tc1,1) 1]);
    tc1z=tc1z./repmat(nanstd(tc1),[size(tc1,1) 1]);
    covMat=zeros(size(tc1z,2),size(tc1z,2),size(tc1z,1));
    for k=1:size(tc1z,1)
        covMat(:,:,k)=tc1z(k,:)'*tc1z(k,:);
    end;
    rho=nansum(covMat,3)/(size(tc1z,1)-1);
    
else
    
    if size(tc1,1)~=size(tc2,1)
        error('First dimension length must be equal');
    end;
    tc1z=tc1-repmat(nanmean(tc1),[size(tc1,1) 1]);
    tc2z=tc2-repmat(nanmean(tc2),[size(tc2,1) 1]);
    tc1z=tc1z./repmat(nanstd(tc1),[size(tc1,1) 1]);
    tc2z=tc2z./repmat(nanstd(tc2),[size(tc2,1) 1]);
    covMat=zeros(size(tc1z,2),size(tc2z,2),size(tc1z,1));
    for k=1:size(tc1z,1)
        covMat(:,:,k)=tc1z(k,:)'*tc2z(k,:);
    end;
    rho=nansum(covMat,3)/(size(tc1z,1)-1);
    
end;

end