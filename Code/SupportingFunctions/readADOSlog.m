function [data,hdr,t0,t02] = readADOSlog(in)
%% [data,hdr] = readADOSlog(in)
% TODO:
% Break up IDs to multiple different ones if there is a gap in the data
% (either in time or place?)

data=[];
f=fopen(in);
line=fgetl(f);
dataS2=[];
k=1;
while ~strcmp(line(1),'!')
    hdr{k}=line;
    if any(strfind(hdr{k},'Local time'))
        t0=str2num(hdr{k}(strfind(hdr{k},':')+1:end));
    end
    k=k+1;
    line=fgetl(f);
end
first=true;
fprintf('Headers read...\n');

while 1
    line=fgetl(f);
    
    if first
        if (line==-1)
            fprintf(1,'File empty.\n');
            return;
        end
        lineLen=length(str2num(line));
        temp=str2num(line);
        t02=temp(2)-(temp(3)-t0);
        first=false;
        fprintf(1,'Start time %i / %i\n',t0,t02);
    end
            
    if isempty(strfind(line,'∞'))
        if (line==-1)
            fprintf(1,'All lines read.\n');
            break;
        end
%         tabs=find(line==9);
%         if length(tabs)>=9
%             line(tabs(9):end)=[];
%         end
        if (length(str2num(line))<lineLen)
            fprintf(1,'All lines read.\n');
            break;
        end
        data(:,end+1)=str2num(line);
        data(2,end)=data(2,end)-t02;
        data(3,end)=data(3,end)-t0;
        curt=size(data,2);
        if mod(curt,1000)==0
            fprintf(1,'%i, ',curt);
            if mod(curt,10000)==0
                fprintf(1,'\n');
            end
        end
    end
end

%Go through all IDs and break them apart if there is a gap of more than 2
%seconds in time or 0.25 meters in space
fprintf('ASDGSADGAG\n');
IDs=unique(data(1,:));
for k=1:length(IDs)
   cidx=data(1,:)==IDs(k);
   maxID=max(data(1,:));
   spSkip=sqrt(nansum(diff(data(4:6,cidx),[],2).^2))'>.25;
   tSkip=diff(data(2,cidx))'>2000;
   %Split data only if there are skips (otherwise we get errors)
   if any(spSkip) || any(tSkip)
        skips=[false; or(spSkip,tSkip)];
        data(1,cidx)=data(1,cidx).*(cumsum(skips(:).')==0)+cumsum(skips(:).')+(cumsum(skips(:).')>0).*maxID; %#ok<AGROW>
   end
   
end
end
