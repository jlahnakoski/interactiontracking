function outputVar = correlationToProbability(samplesize,rho)%,cumul)
%Converts probability to correlation coefficient
%i.e. inverse of t-test used in matlab corr-function

n=samplesize;
t = rho.*sqrt((n-2)./(1-rho.^2));

outputVar = tcdf(-abs(t),n-2);
