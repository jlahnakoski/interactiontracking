%Run zitternix_paper_analyses.m first (at least the main loop of it)
tWinAllNew=tWinAll;
ordNum=1;
%% Separate the loop in a separate section so you don't need to start from the beginning if you make a mistake
for ordNum=ordNum:length(d)
    %%
    subjectNo=ord(ordNum);
    
    %dataAll{subjectNo,1}: %Data for the participant 1 (target for slave Kinect) from slave Kinect
    %dataAll{subjectNo,2}: Data for participant 2 from slave Kinect
    %dataAll{subjectNo,3}: Data for participant 2 from master Kinect (target of master) registered to Slave coordinate system
    %In columns: Host time, head X,Y,Z, all joints and gaze point X,Y,Z
    %twinFull{subjectNo}: Linearly spaced time points for interpolation
    %twinFullKin{subjectNo}: Time points in Kinect time for loading video
    %frames (two columns, first for master second for slave)
    
    %List saved video frame timestamps for Master Kinect
    dtemp=dir(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Master/%s*',d(subjectNo).name,d(subjectNo).name));
    dtemp(~[dtemp(:).isdir])=[];
    dfrm1=dir(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Master/%s/*.png',d(subjectNo).name,dtemp.name));
    nfrm1=zeros(length(dfrm1),1);
    for k=1:length(dfrm1)
        nfrm1(k)=str2double(dfrm1(k).name(1:end-4));
    end
    nfrm1=sort(nfrm1,'ascend');
    [hdrMast{subjectNo},t0mast{subjectNo},t02mast{subjectNo}]=readADOSheader(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Master/%s.txt',d(subjectNo).name,dtemp.name));
    
    %List saved video frame timestamps for Slave Kinect
    dtemp=dir(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Slave/%s*',d(subjectNo).name,d(subjectNo).name));
    dtemp(~[dtemp(:).isdir])=[];
    dfrm2=dir(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Slave/%s/*.png',d(subjectNo).name,dtemp.name));
    nfrm2=zeros(length(dfrm2),1);
    for k=1:length(dfrm2)
        nfrm2(k)=str2double(dfrm2(k).name(1:end-4));
    end
    nfrm2=sort(nfrm2,'ascend');
    [hdr{subjectNo},t0slav{subjectNo},t02slav{subjectNo}]=readADOSheader(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Slave/%s.txt',d(subjectNo).name,dtemp.name)); %#ok<*SAGROW>
    
    prevTime=0;
    for trial=1:9
        ctime=find(tWinAllNew{subjectNo}(:,trial),1,'first')*100; %Current time in milliseconds

        hfig=figure;
        set(hfig,'numberTitle','off');
        hax=axes;
        him=image(zeros(180,640,3));
        set(hfig,'position',[400 700 640*2 180*2]);
        set(hax,'position',[0 0 (640*2)/(640*2) 1]);
        %Loop until the user indicates the start/end frame is good (inputs
        %0 as the number of seconds to move)
        while 1
            set(hfig,'name',sprintf('Trial %i, start',trial));
            distTemp1=abs(nfrm1-(ctime+t02mast{subjectNo}));
            distTemp2=abs(nfrm2-(ctime+t02slav{subjectNo}));
            t1=find(distTemp1==min(distTemp1),1);
            t2=find(distTemp2==min(distTemp2),1);
            im1=imread(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Master/%s/%i.png',d(subjectNo).name,dtemp.name,nfrm1(t1)));
            im2=imread(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Slave/%s/%i.png',d(subjectNo).name,dtemp.name,nfrm2(t2)));
            set(him,'cdata',cat(2,im1,im2));
            drawnow;
            temp=input(sprintf('Cur-prev: %.1f: Move backwards (-) or forwards (+) in seconds?\n',(ctime-prevTime)/1000));
            %tWinAll to milliseconds: multiply by 100, seconds (temp) to
            %ms: multiply by 1000
            ctime=min(length(tWinAllNew{subjectNo})*100,max(1,round(ctime+(temp*1000))));
            if temp==0
                %Make changes to the time windows
                prevTime=ctime;
                origStart=find(tWinAllNew{subjectNo}(:,trial),1,'first');
                newStart=ctime/100;
                
                % Same for the end timepoint
                set(hfig,'name',sprintf('Trial %i, end',trial));
                ctime=find(tWinAllNew{subjectNo}(:,trial),1,'last')*100;
                while 1
                    distTemp1=abs(nfrm1-(ctime+t02mast{subjectNo}));
                    distTemp2=abs(nfrm2-(ctime+t02slav{subjectNo}));
                    t1=find(distTemp1==min(distTemp1),1);
                    t2=find(distTemp2==min(distTemp2),1);
                    im1=imread(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Master/%s/%i.png',d(subjectNo).name,dtemp.name,nfrm1(t1)));
                    im2=imread(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Slave/%s/%i.png',d(subjectNo).name,dtemp.name,nfrm2(t2)));
                    set(him,'cdata',cat(2,im1,im2));
                    drawnow;
                    temp=input(sprintf('Cur-prev: %.1f: Move backwards (-) or forwards (+) in seconds?\n',(ctime-prevTime)/1000));
                    ctime=min(length(tWinAllNew{subjectNo})*100,max(1,round(ctime+(temp*1000))));
                    
                    %All done, let's make the new time window and break the loop
                    if temp==0
                        %Make changes to the time windows
                        prevTime=ctime;
                        newEnd=ctime/100;
                        tWinAllNew{subjectNo}(:,trial)=0;
                        tWinAllNew{subjectNo}(round(newStart):round(newEnd),trial)=1;
                        %...aaaand done!
                        break;
                    end
                    
                end
                %The outer while loop should also be done now, since both
                %the start and the end has been found
                break;
            end
        end
        fprintf('done\n');
        
        close(hfig);
    end
end
%%
save('/nas/Juha/ADOS_zitternix/Subjects/timeWindowsWihtManualAdjustment.mat','tWinAllNew');