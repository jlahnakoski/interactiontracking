    function [dataSens,keyPresses] = read_zitternix_sensorlog(fileIn,curDataPoint)
% [dataSens,keyPresses] = read_zitternix_sensorlog(fileIn,curDataPoint)
% Reads log file saved by IMU logging software for TinkerForge IMU Brick
% 2.0.
%
% Reads all 3-component data and converts quaternions to 3-component format
% comparable to the Euler angles, but possibly with less wrapping problems.
% Temperature and calibration state data are omitted for now.

if nargin<1 || isempty(fileIn)
    fileIn='/nas/Juha/ADOS_zitternix/Subjects/F3/F3_log_02_06_2017_01_11_16.txt';%'/nas/Juha/ADOS_zitternix/UtaAndChris/chris_and_uta_log_23_05_2017_11_09_23.txt';
end
%Datapoints:
%dataSenspoints(1):  Sample
%dataSenspoints(2):  Acceleration
%dataSenspoints(3):  Magnetic Field
%dataSenspoints(4):  Angular Velocity
%dataSenspoints(5):  Euler Angle
%dataSenspoints(6):  Quaternion
%dataSenspoints(7):  Linear Acceleration
%dataSenspoints(8):  Gravity Vector
%dataSenspoints(9):  Temperature
%dataSenspoints(10): Calibration Status
%Ascii code for tab is 9
if nargin<2 || isempty(curDataPoint) %If data column is not specified, read all (except temperature and calibration status)
    curDataPoint=2:8;
end
%%
fid=fopen(fileIn);
% figure;
% for k=1:4
%     subplot(2,2,k);
%     hp(k)=plot(0);
% end
prevSamp=zeros(4,1);
dataSensStarted=false;
keyPresses=cell(0,2);
prevPrint=0;
d=dir(fileIn); %Check file properties to get the size in bytes
dataSens{1}=nan(round(d.bytes/500/4),4,length(curDataPoint)); %Preallocate for speed, each line is approximately 500 bytes and there are four lines/sensors for each sample index
dataSens{2}=nan(round(d.bytes/500/4),4,length(curDataPoint)); %Preallocate for speed
dataSens{3}=nan(round(d.bytes/500/4),4,length(curDataPoint)); %Preallocate for speed
dataSens{4}=nan(round(d.bytes/500/4),4,length(curDataPoint)); %Preallocate for speed
tic
while 1
    
    line=fgetl(fid);
    
    if any(strfind(line,'Log start time:'))
        t0=str2double(line(strfind(line,':')+1:end));
    end
%     if any(strfind(line,'Local time:'))
%         locTime=str2double(line(strfind(line,':')+1:end));
%     end
%     if any(strfind(line,'Remote time:'))
%         remTime=str2double(line(strfind(line,':')+1:end));
%     end
    
    if any(strfind(line,'------------------'))
        dataSensStarted=~dataSensStarted;
        line=fgetl(fid);
    end
    if any(line==-1) || any(strfind(line,'End of recording'))
        %Drop any unfilled datapoints
        dataSens{1}(logical(max(max(isnan(dataSens{1}),[],3),[],2)),:,:)=[];
        dataSens{2}(logical(max(max(isnan(dataSens{2}),[],3),[],2)),:,:)=[];
        dataSens{3}(logical(max(max(isnan(dataSens{3}),[],3),[],2)),:,:)=[];
        dataSens{4}(logical(max(max(isnan(dataSens{4}),[],3),[],2)),:,:)=[];
        break;
    end
    if dataSensStarted
        
        line(strfind(line,','))='.';
        
        %         if samp>500
        %             break;
        %         end
        
        dataSenspoints=strfind(line,['|' 9 '|']);
            %             ndataSens=length(dataSenspoints);
        tabs=strfind(line,9);
        
        for cp=1:length(curDataPoint)
            
            if any(strfind(line,'Sensor H1'))%any(strfind(line,'Accelerometer 1'))
                col=1;
            elseif any(strfind(line,'Sensor H2'))%any(strfind(line,'Accelerometer 2'))
                col=2;
            elseif any(strfind(line,'Sensor 1'))
                col=3;
            elseif any(strfind(line,'Sensor 2'))
                col=4;
            elseif any(strfind(line,'Key')) %This should be moved outside the loop going through the different data (acceleration, orientation etc.). Currently, it produces 8 copies of each key press, which is slightly annoying.
                col=nan;
                dlm=strfind(line,'|');
                keyPresses{end+1,1}=str2double(line(1:dlm()-1))-t0; %#ok<AGROW>
                keyPresses{end,2}=line(dlm(end-1)+2:dlm(end)-2);
                %             break
            else
                col=nan;
            end
            if ~isnan(col)
                curSamp=str2double(line(tabs(max(find(tabs>dataSenspoints(1),2))):tabs(max(find(tabs>dataSenspoints(1),3)))));
                if ~isempty(curSamp)
                    if curSamp>prevSamp(col)+1
                        dataSens{col}(prevSamp+1:curSamp,:,cp)=nan;
                    end
                    
                    t=str2double(line(1:tabs(1)-1))-t0;
                    %             if col<3
                    %                 x=str2num(line(tabs(max(find(tabs>dataSenspoints(2),3))):tabs(max(find(tabs>dataSenspoints(2),4)))));
                    %                 y=str2num(line(tabs(max(find(tabs>dataSenspoints(2),5))):tabs(max(find(tabs>dataSenspoints(2),6)))));
                    %                 z=str2num(line(tabs(max(find(tabs>dataSenspoints(2),7))):tabs(max(find(tabs>dataSenspoints(2),8)))));
                    %                 dataSens{col}(curSamp,:)=[t x y z];
                    %             else
                    if curDataPoint(cp)==6
                        
%                         x=str2double(line(tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),3))):tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),4)))));
%                         y=str2double(line(tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),5))):tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),6)))));
%                         z=str2double(line(tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),7))):tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),8)))));
%                         w=str2double(line(tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),9))):tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),10)))));
                        
                        tempx=str2num(line([tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),3))):tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),4)))...
                                            tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),5))):tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),6)))...
                                            tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),7))):tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),8)))...
                                            tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),9))):tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),10)))])); %#ok<ST2NM>
                        
                        pmat=tempx(:)*tempx;
                        
                        temp1=2*pmat(1,2) + 2*pmat(3,4);%2*x*y + 2*w*z;
                        temp2=pmat(4,4) + pmat(1,1) - pmat(2,2) - pmat(3,3);%w*w + x*x - y*y - z*z;
                        temp1=min(1,max(-1,temp1));
                        temp2=min(1,max(-1,temp2));
                        
                        yaw   =  atan2(temp1,temp2);
                        
                        temp=2*pmat(2,4) - 2*pmat(1,3);%2*w*y - 2*x*z;
                        temp=min(1,max(-1,temp));
                        pitch = -asin(temp);
                        
                        temp1=2*pmat(2,3) + 2*pmat(1,4);%2*y*z + 2*w*x;
                        temp2=-pmat(4,4) + pmat(1,1) + pmat(2,2) - pmat(3,3);%w*w + x*x + y*y - z*z;
                        temp1=min(1,max(-1,temp1));
                        temp2=min(1,max(-1,temp2));
                        roll  = -atan2(temp1,temp2);
                        dataSens{col}(curSamp,:,cp)=[t yaw pitch roll];
                    else
                        x=str2double(line(tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),3))):tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),4)))));
                        y=str2double(line(tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),5))):tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),6)))));
                        z=str2double(line(tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),7))):tabs(max(find(tabs>dataSenspoints(curDataPoint(cp)),8)))));
                        %                     if max(size([t x y z]))<4
                        %                         return;
                        %                     end
                        dataSens{col}(curSamp,:,cp)=[t x y z];
                    end
                    %             end
                    prevSamp(col)=curSamp;
                    if curSamp~=prevPrint
                        if mod(curSamp,1000)==0
                            prevPrint=curSamp;
                            fprintf('%i, ',curSamp)
                            if mod(curSamp,10000)==0
                                toc;
                                tic
                            end
                        end
                    end
                end
                %                 if any(~isreal(sqrt(sum(dataSens{col}(:,2:4,cp).^2,2))))
                %                     fprintf('complex number, stopping\n');
                %                     return;
                %                 end
                %             if mod(curSamp,1000)==0
                % %                 for kk=1:4
                % %                     if curDataPoint(cp)==5
                % %                         dataSens{kk}(:,2:4)=360/2/pi*unwrap(dataSens{kk}(:,2:4)*2*pi/360);
                % %                     end
                % %                 end
                %                 set(hp(col),'xdata',dataSens{col}(:,1),'ydata',sqrt(sum(dataSens{col}(:,2:4).^2,2)));
                % %                 set(hp(col),'ydata',sqrt(sum(dataSens{col}(:,2:4).^2,2)));
                %                 drawnow;
                %             end
            end
        end
    end
    
end
fprintf('done\n')