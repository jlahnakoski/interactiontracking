function g = plotComponentErrors2(comps,times,u,p,varargin)

%Plots sweet graphs with errorbars (95% confidence interval)
%comps=Array containing subjects time courses for a single component
%      format: comps(subject,time)
%times=timepoints corresponding to the samples in comps
%
%Optional:
%If you want to calculate and plot significance bars the following are
%necessary. If they are not set, bars will not be plotted.
%u=number of subjects showing significant activity
%p=probability level (default: 0.05)
%
%varargin=additional timecourse to be plotted on the same axis
%         Note: this must be the same length as the component timecourses.

if nargin<2|(isempty(times)|length(times)~=size(comps,2))
    times=1:size(comps,2);
end

if nargin>5
    plotColor=varargin{2};
    edgeColor=(varargin{2}+1)/2;
else
    edgeColor=[196/255 196/255 196/255];
    plotColor=[0 0 0];
end

compsMean=squeeze(nanmean(comps,1));
temp=squeeze(grpstats(comps,ones(size(comps)),'meanci'));

compsMax=temp(:,2)';%(compsMean+squeeze(stds);
compsMin=temp(:,1)';%compsMean-squeeze(stds);
scale=abs(nanmin(squeeze(compsMin)))+abs(nanmax(squeeze(compsMax)));
%Yoffset=-min(compsMin);
Yoffset=0;
% for t=1:size(comps,2)
% tempT=comps(:,t);
% compsMean(t)=mean(tempT(find(~isnan(tempT))));
% end;
compsMean=nanmean(comps);
%compsMean=compsMean+Yoffset;
compsMax=compsMax+Yoffset;
compsMin=compsMin+Yoffset;
if nargin>2 && ~isempty(u);
    hold on;
    if isempty(p) | p<=0
        p=0.05;
    end
    
    g=bar(times,4+(scale*(stoufferGroupProbability1D(comps,u)>1-p)),'r');
    set(g,'edgecolor','none');
end
hold on;
%plot(times,4+compsMax,'color',[196/255 187/255 173/255]);
%plot(times,4+compsMin,'color',[196/255 187/255 173/255]);
% jbfill(times,compsMax,compsMin,'none',edgeColor,0,0.5);
plot([times(:)' fliplr(times(:)') times(1)],[compsMax(:)' fliplr(compsMin(:)') compsMax(1)],'color',edgeColor);
hold on;

plot(times,transpose(squeeze(compsMean)),'color',plotColor,'linewidth',2);

% axis([nanmin(times) nanmax(times) nanmin(sq(compsMin)) nanmax(sq(compsMax))]);

if nargin>4 & ~isempty(varargin{1})
    plot(times,varargin{1}+Yoffset,'color',[146/255 139/255 129/255],'linewidth',1);
end
%set(gcf,'position',[0 100 1280 300]);
%set(gcf,'position',[0 100 640 150]);
%set(gca,'box','on')
%set(gca,'ytick',[])
%set(gca,'fontsize',20)

end

