function out = aaltoColors(idx)

    out=[254   203     0;
         237    41    57;
           0   101   189;
           0     0     0;
         146   139   129;
         105   190    40;
           0   155    58;
           0   168   180;
         102    57   183;
         177     5   157;
         255   121     0];

     if nargin>0 && ~isempty(idx)
         out=out(idx,:);
     end;
     %out=uint8(out);
     out=out/255;
end