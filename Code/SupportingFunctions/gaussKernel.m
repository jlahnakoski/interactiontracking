function out = gaussKernel(kernSize,sigma)
%Creates a 3D, 2D or 1D gaussian kernel assuming isotropic voxels
%If the kernel size along any direction is even that dimension is increased by 1
%to center the peak of the kernel in the output volume
kernSize=kernSize+(1-mod(kernSize,2));
if length(kernSize)>1
    out=zeros(kernSize);
else
    out=zeros(kernSize,1);
end;
center=ceil(kernSize/2);
 
if length(kernSize)==3
    for x=1:kernSize(1)
        for y=1:kernSize(2)
            for z=1:kernSize(3)
                
                out(x,y,z)= exp(-((x-center(1))^2+(y-center(2))^2+(z-center(3))^2)...
                    /(2*sigma^2))/sqrt(2*pi*sigma^2);
                
            end;
        end;
    end;
elseif length(kernSize)==2
    for x=1:kernSize(1)
        for y=1:kernSize(2)
            
            out(x,y)= exp(-((x-center(1))^2+(y-center(2))^2)...
                /(2*sigma^2))/sqrt(2*pi*sigma^2);
            
        end;
    end;
elseif length(kernSize)==1
    for x=1:kernSize(1)
                    
            out(x)= exp(-((x-center(1))^2)/(2*sigma^2))/sqrt(2*pi*sigma^2);
            
    end;
end;
out=out/sum(out(:));
end
