function [XCmap,maxVal,maxLoc,peakVal,peakLoc] = windowedCrossCorrelation(in1,in2,winLen,maxLag,winStep,visOn)
%Make a windowed cross-correlation timecourse
%Parameters:
%in1 & in2: Timecourses for calculation of cross-correlation. These should
%be of equal length and samples should correspond to data acquired at the
%same time from two sources. If timecourses are of different length, the
%longer will be cut from the end to match the length of the shorter.
%winLen: Window length for cross-correlation calculations in samples 
%(default=60)
%maxLag: Maximum lag between signals in cross correlation. For reliability,
%it is probably good to make this considerably smaller than the window.
%(Default=winLen/4).
%winStep: Step-size between adjacent windows (in samples). Default is equal
%to the window size (i.e. no overlap between windows)
%visOn: Visualize the crosscorrelation map and timecourse. Default: false.
%
%v 0.00001 alpha
%12.7.2018
%Juha Lahnakoski
%juha_lahnakoski@psych.mpg.de
if nargin<3 || isempty(winLen)
    winLen=60;
end
if nargin<4 || isempty(maxLag)
    maxLag=round(winLen/4);
end
if nargin<5 || isempty(winStep)
    winStep=winLen;
end
if maxLag>winLen %You can't have a bigger lag than the window
    maxLag=winLen;
end
if nargin<6 || isempty(visOn)
    visOn=false;
elseif any(visOn)
    visOn=true;
end
len=min(length(in1),length(in2));
in1=in1(1:len);
in2=in2(1:len);
in1=zscore(in1(:));%Standardize
in2=zscore(in2(:));

winNum=floor((len-winLen)/winStep); %Number of timewindows
XCmap=zeros(maxLag*2+1,winNum); %Preallocate for speed
peakLoc=zeros(winNum,1);
peakVal=zeros(winNum,1);
for t=1:winNum
    curInd=(t-1)*winStep+(1:winLen);
    XCmap(:,t)=xcorr(zscore(in1(curInd)),zscore(in2(curInd)),maxLag,'unbiased');
    [pks,loc]=findpeaks(XCmap(:,t),'MinPeakWidth',5,'minpeakprominence',.01);%get rid of tiny peaks and flat sections
    locs=find(abs(loc-maxLag)<=(min(abs(loc-maxLag))+5)); %If there are any higher peaks less than 10th of a second away, choose one of those instead
    if ~any(locs)
        %If there are no peaks, just take the middle sample
        idx=maxLag+1;
        peakLoc(t)=idx;
        peakVal(t)=XCmap(idx,t);
    else
        %If peaks were found, take the one that is closest to zero-shift
        %(take the highest peak if two are at the same distance)
        idx=locs(find(pks(locs)==max(pks(locs))));
        if length(idx)>1
            idx=idx(find(abs(loc(idx)-maxLag)==min(abs(loc(idx)-maxLag))));
            idx=idx(1); %If there are still more than one, just take the first one
        end
        peakLoc(t)=loc(idx);
        peakVal(t)=pks(idx);
        
    end
end
[maxVal,maxLoc]=max(XCmap);
if visOn
    imagesc(XCmap);
    axis tight
    hold on;
    [~,maxInd]=max(XCmap);
    plot(maxInd,'k');
    plot(peakLoc,'color',[.7 .7 .7]);
    
    lags=-maxLag:maxLag;
    N=maxLag+1;
    K=1:maxLag+1;
    D = K(rem(N,K)==0);
    tickStep=find(D>2,1);
    if isempty(tickStep)
        tickStep=maxLag;
    end
    ticks=1:tickStep:maxLag*2+1;
    set(gca,'ydir','reverse','ytick',ticks,'yticklabel',lags(ticks));
    axes;
    hold on;
    plot(winLen/2+(1:len),in1,'w');
    plot(winLen/2+(1:len),in2,'r');
    set(gca,'visible','off','xtick',[],'ytick',[]);
    axis tight
end
