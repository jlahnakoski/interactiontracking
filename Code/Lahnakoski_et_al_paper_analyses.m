 %% Load data and label one dataset to get started
clear
baseDir='C:\Users\jlahnakoski\Desktop\Manuscripts\Zitternix\R1verificationAnalyses\interactiontracking-master\interactiontracking-master';
addpath([baseDir '/Code'])
addpath([baseDir '/Code/SupportingFunctions'])
%If the converted data does not exist, run the conversion script
if ~exist([baseDir '/matDataKinectAndIMU/'],'Dir')
    zitternix_convert_kinect_IMU_to_Matlab;
end
%% Edit sensor-log-reading function so that it saves all columns
cd([baseDir '/matDataKinectAndIMU']);
d=dir('F*');
d2=dir('M*');
d=[d; d2];
d(~[d(:).isdir])=[]; %Remove files from the directory listin (if any)
trainSub=4; %Dyad number used for training the classifier that recognizes the individuals in a dyad based on location
subjectNo=trainSub;
%% Interaction ratings
rateMat=[5.5  5.6  4.6  4.5  5.8  6.4  5.8  5.9  4.8  5.6  5.4  6.9  5.1  4.6  5.1  5.0  4.5  3.4  5.4  4.6  6.0  5.5  5.3  4.9  6.8  5.0  4.8  5.6  4.4  3.6  2.5  3.4  5.1  5.9  5.6  6.1  6.3  5.3;
    6.7  6.9  4.7  5.0  6.7  6.6  6.9  5.3  5.6  6.7  6.4  7.0  5.4  6.0  6.4  5.7  6.3  4.7  6.1  5.7  6.6  5.6  5.9  4.9  7.0  6.3  4.6  6.6  5.7  4.0  3.0  4.4  5.6  6.1  6.1  6.6  6.6  5.4;
    3.6  4.4  2.0  4.8  4.8  6.4  3.6  4.8  4.2  4.8  4.8  4.8  5.8  5.6  3.2  2.8  4.8  2.2  5.6  2.2  3.8  3.0  4.6  3.2  5.6  5.2  4.4  4.6  4.6  1.8  2.4  4.8  5.2  6.6  4.2  4.4  4.0  4.4;
    2.4  2.0  2.6  2.0  2.2  1.0  3.2  1.4  1.0  3.2  3.0  1.0  1.6  1.4  1.6  3.4  2.4  2.4  2.2  1.2  2.2  2.0  2.8  1.8  3.0  3.6  2.8  3.2  2.8  1.2  3.0  2.2  2.4  1.0  2.4  2.2  1.8  2.6;
    4.0  4.5  3.8  4.5  2.3  5.5  4.0  4.3  2.8  5.0  4.0  4.3  6.0  3.5  5.5  4.3  5.8  6.8  5.3  6.0  5.3  5.3  4.0  5.5  2.5  3.8  4.8  6.5  5.0  4.5  4.5  4.8  3.3  6.0  6.3  5.3  5.5  5.0];
rateTitles={'Relatedness'
    'Interest/Enjoyment'
    'Perceived choice'
    'Pressure/tension'
    'Effort'};

rate1=rateMat(:,1:2:end);
rate2=rateMat(:,2:2:end);
namesReord=[1 11:14 2:9 15:19];
rate1=rate1(:,namesReord);
rate2=rate2(:,namesReord);

rateMean=(rate1+rate2)/2;
names={'F1' 'F2' 'F3' 'F4' 'F5' 'F6' 'F7' 'F8' 'F9' 'F10' 'F11' 'F12' 'F13' 'F14' 'M1' 'M2' 'M3' 'M4' 'M5'};
names=names(namesReord);

cols=aaltoColors([3 2 5 6]);

%% Manual relabeling of participants based on location distributions.
% %Necessary because Kinect gives random labels for each person, which may
% %change during the session especially if tracking is lost in between.
% %This is done separately for the two sensors ("Master" and "Slave")
% 
% %Setting this to false skips the manual labeling of participants, if it has
% %been done already.
% %The classification for the publication is already done, so this is only
% %for reference
% relabelParticipants=false;
% 
% if relabelParticipants
%     %% This prepares the training subject
%     load([baseDir '/matDataKinectAndIMU/' d(subjectNo).name '.mat']);
%     
%     uni=unique(dataKinSlave(1,:));
%     unimode=mode(dataKinSlave(1,:));
%     uni(uni==unimode)=[];
%     uni=[unimode; uni(:)];
%     unilen=uni*0;
%     dropData=false(size(dataKinSlave(1,:)));
%     
%     for k=1:length(uni)
%         unilen(k)=sum(dataKinSlave(1,:)==uni(k));
%         unispeed(k)=mean(sqrt(nanmean(diff(dataKinSlave(4:6,dataKinSlave(1,:)==uni(k)),[],2).^2)));
%         if unilen(k)<100
%             dropData(dataKinSlave(1,:)==uni(k))=true;
%         end
%         if unispeed(k)>.03 || isnan(unispeed(k))
%             dropData(dataKinSlave(1,:)==uni(k))=true;
%         end
%     end
%     dataKinSlave(:,dropData)=[];
%     uni=unique(dataKinSlave(1,:));
%     unimode=mode(dataKinSlave(1,:));
%     uni(uni==unimode)=[];
%     uni=[unimode; uni(:)];
%     %% Manually label the participants from one dataset and save the group
%     %  vector
%     
%     figure;
%     hp(1)=scatter3(dataKinSlave(4,dataKinSlave(1,:)==uni(1)),dataKinSlave(5,dataKinSlave(1,:)==uni(1)),dataKinSlave(6,dataKinSlave(1,:)==uni(1)),'markerfacecolor',cols(1,:),'markeredgecolor','none','sizedata',2,'markerfacealpha',.25);
%     hold on;
%     
%     % view(180,-60)
%     axis vis3d equal
%     set(gca,'position',[0 0 1 1]);
%     k=1;
%     idgrp(k)=1;
%     grpvec=zeros(size(dataKinSlave(1,:)));
%     grpvec(dataKinSlave(1,:)==uni(k))=idgrp(k);
%     for k=2:length(uni)
%         hp(k)=scatter3(dataKinSlave(4,dataKinSlave(1,:)==uni(k)),dataKinSlave(5,dataKinSlave(1,:)==uni(k)),dataKinSlave(6,dataKinSlave(1,:)==uni(k)),'markerfacecolor',cols(4,:),'markeredgecolor','none','sizedata',25,'markerfacealpha',1);
%         idgrp(k)=input('ID 1/2/3: 1=red,2=yellow,3=not of interest: ');
%         set(hp(k),'markerfacecolor',cols(idgrp(k),:),'markerfacealpha',.25,'sizedata',2);
%         grpvec(dataKinSlave(1,:)==uni(k))=idgrp(k);
%     end
%     save(sprintf('groupVectorSlave_dyad_%s',d(subjectNo).name),'grpvec');
%     %% Check results from the plot
%     figure;
%     hold on;
%     for k=1:3
%         plot(dataKinSlave(2,grpvec==k),dataKinSlave(4,grpvec==k),'color',cols(k,:));
%     end
%     % Train radial SVM classifier to recognize groups 1 and 2
%     fprintf('Training SVM ');
%     load(sprintf('groupVectorSlave_dyad_%s',d(subjectNo).name));
%     traindata=dataKinSlave(4:6,:);
%     trainlabel=grpvec;
%     
%     for k=1:2
%         fprintf('%i...',k);
%         SVMmodel{k}=fitcsvm(dataKinSlave(4:6,:)',double(grpvec==k),'KernelFunction','RBF');
%     end
%     
%     fprintf('done\n');
%     %% Label groups for new dyad
%     fprintf('Classifying...');
%     figure;
%     for subjectNo=1:length(d)
%         %%
%         fprintf('%s...',d(subjectNo).name);
%         load([baseDir '/matDataKinectAndIMU/' d(subjectNo).name '.mat']);
%         
%         % List labels and start with the most common one
%         uni=unique(dataKinSlave(1,:));
%         unimode=mode(dataKinSlave(1,:));
%         uni(uni==unimode)=[];
%         uni=[unimode; uni(:)];
%         unilen=uni*0;
%         dropData=false(size(dataKinSlave(1,:)));
%         
%         for k=1:length(uni)
%             unilen(k)=sum(dataKinSlave(1,:)==uni(k));
%             if unilen(k)<100
%                 dropData(dataKinSlave(1,:)==uni(k))=true;
%             end
%         end
%         dataKinSlave(:,dropData)=[];
%         uni=unique(dataKinSlave(1,:));
%         unimode=mode(dataKinSlave(1,:));
%         uni(uni==unimode)=[];
%         uni=[unimode; uni(:)];
%         grpvec=dataKinSlave(1,:);
%         
%         for k=1:2
%             labels{k}=SVMmodel{k}.predict(dataKinSlave(4:6,:)');
%         end
%         labelvec=ones(size(dataKinSlave,2),1)*3;
%         labelvec(and(labels{1},~labels{2}))=1;
%         labelvec(and(labels{2},~labels{1}))=2;
%         
%         grpNew=grpvec;
%         for k=1:length(uni)
%             if any(grpNew(grpvec==uni(k))==mode(labelvec(grpvec==uni(k)))) && ...
%                     sqrt(mean((mean(dataKinSlave(4:6,grpvec==uni(k)),2)-mean(dataKinSlave(4:6,grpvec==mode(labelvec(grpvec==uni(k)))),2)).^2))>.5
%                 fprintf('outlier~!!!111\n');
%                 grpNew(grpvec==uni(k))=max(labelvec);
%             else
%                 grpNew(grpvec==uni(k))=mode(labelvec(grpvec==uni(k)));
%             end
%         end
%         
%         subplot(1,2,1);
%         hold off;
%         for k=3:-1:1
%             scatter3(dataKinSlave(4,grpNew==k),dataKinSlave(5,grpNew==k),dataKinSlave(6,grpNew==k),'markerfacecolor',cols(k,:),'markeredgecolor','none','sizedata',5,'markerfacealpha',1/3);
%             hold on
%         end
%         drawnow
%         
%         subplot(1,2,2);
%         hold off;
%         for k=1:3
%             plot(dataKinSlave(2,grpNew==k),dataKinSlave(4,grpNew==k),'color',cols(k,:));
%             hold on
%         end
%         drawnow
%         
%         save(sprintf('%s/groupsSlave.mat',d(subjectNo).name),'grpNew');
%     end
%     fprintf('done\n');
%     %% Master
%     subjectNo=trainSub;
%     
%     load([baseDir '/matDataKinectAndIMU/' d(subjectNo).name '.mat']);
%     
%     
%     
%     uni=unique(dataKin(1,:));
%     unimode=mode(dataKin(1,:));
%     uni(uni==unimode)=[];
%     uni=[unimode; uni(:)];
%     unilen=uni*0;
%     dropData=false(size(dataKin(1,:)));
%     
%     for k=1:length(uni)
%         unilen(k)=sum(dataKin(1,:)==uni(k));
%         if unilen(k)<100
%             dropData(dataKin(1,:)==uni(k))=true;
%         end
%     end
%     dataKin(:,dropData)=[];
%     uni=unique(dataKin(1,:));
%     unimode=mode(dataKin(1,:));
%     uni(uni==unimode)=[];
%     uni=[unimode; uni(:)];
%     
%     cols=aaltoColors([3 2 5 6]);
%     
%     %% Manually label the participants from one dataset and save the group
%     % vector
%     
%     figure;
%     hp(1)=scatter3(dataKin(4,dataKin(1,:)==uni(1)),dataKin(5,dataKin(1,:)==uni(1)),dataKin(6,dataKin(1,:)==uni(1)),'markerfacecolor',cols(1,:),'markeredgecolor','none','sizedata',2,'markerfacealpha',.25);
%     hold on;
%     
%     % view(180,-60)
%     axis vis3d equal
%     set(gca,'position',[0 0 1 1]);
%     k=1;
%     idgrp(k)=1;
%     grpvec=zeros(size(dataKin(1,:)));
%     grpvec(dataKin(1,:)==uni(k))=idgrp(k);
%     for k=2:length(uni)
%         hp(k)=scatter3(dataKin(4,dataKin(1,:)==uni(k)),dataKin(5,dataKin(1,:)==uni(k)),dataKin(6,dataKin(1,:)==uni(k)),'markerfacecolor',cols(4,:),'markeredgecolor','none','sizedata',25,'markerfacealpha',1);
%         idgrp(k)=input('ID 1/2/3: 1=red,2=not of interest: ');
%         set(hp(k),'markerfacecolor',cols(idgrp(k),:),'markerfacealpha',.25,'sizedata',2);
%         grpvec(dataKin(1,:)==uni(k))=idgrp(k);
%     end
%     save(sprintf('groupVectorMaster_dyad_%s',d(subjectNo).name),'grpvec');
%     %% Check results from the plot
%     figure;
%     hold on;
%     for k=1:3
%         plot(dataKin(2,grpvec==k),dataKin(4,grpvec==k));
%     end
%     % Train radial SVM classifier to recognize groups 1 and 2
%     fprintf('Training SVM ');
%     load(sprintf('groupVectorMaster_dyad_%s',d(subjectNo).name));
%     traindata=dataKin(4:6,:);
%     trainlabel=grpvec;
%     
%     for k=1:1
%         fprintf('%i...',k);
%         SVMmodel{k}=fitcsvm(dataKin(4:6,:)',double(grpvec==k),'KernelFunction','RBF');
%     end
%     
%     fprintf('done\n');
%     %% Label groups for new dyad
%     fprintf('Classifying...');
%     figure;
%     for subjectNo=1:length(d)
%         %%
%         fprintf('%s...',d(subjectNo).name);
%         
%         load([baseDir '/matDataKinectAndIMU/' d(subjectNo).name '.mat']);
%         
%         % Drop IDs that
%         uni=unique(dataKin(1,:));
%         unimode=mode(dataKin(1,:));
%         uni(uni==unimode)=[];
%         uni=[unimode; uni(:)];
%         unilen=uni*0;
%         dropData=false(size(dataKin(1,:)));
%         
%         for k=1:length(uni)
%             unilen(k)=sum(dataKin(1,:)==uni(k));
%             if unilen(k)<100
%                 dropData(dataKin(1,:)==uni(k))=true;
%             end
%         end
%         dataKin(:,dropData)=[];
%         uni=unique(dataKin(1,:));
%         unimode=mode(dataKin(1,:));
%         uni(uni==unimode)=[];
%         uni=[unimode; uni(:)];
%         grpvec=dataKin(1,:);
%         
%         for k=1:1
%             labels{k}=SVMmodel{k}.predict(dataKin(4:6,:)');
%         end
%         labelvec=labels{k};
%         
%         grpNew=grpvec;
%         for k=1:length(uni)
%             grpNew(grpvec==uni(k))=mode(labelvec(grpvec==uni(k)));
%         end
%         
%         subplot(1,2,1);
%         hold off
%         for k=2:-1:1
%             scatter3(dataKin(4,grpNew==k),dataKin(5,grpNew==k),dataKin(6,grpNew==k),'markerfacecolor',cols(k,:),'markeredgecolor','none','sizedata',5,'markerfacealpha',1/3);
%             hold on;
%         end
%         drawnow
%         
%         subplot(1,2,2);
%         hold off;
%         for k=1:2
%             plot(dataKin(2,grpNew==k),dataKin(4,grpNew==k),'color',cols(k,:));
%             hold on
%         end
%         drawnow
%         
%         save(sprintf('%s/groupsMaster.mat',d(subjectNo).name),'grpNew');
%     end
%     fprintf('done\n');
%     
% end
%% IF LABELING HAS BEEN DONE AND SAVED ALREADY, CONTINUE FROM HERE!!
load([baseDir '/preprocessedData/roundTripTimes.mat'])
load([baseDir '/preprocessedData/timeWindowsWithManualAdjustment.mat']);
allN=[];
% hfig=figure;
% hfig2=figure;svm
% hfig3=figure;
fprintf('Registering...\n');
maxD=100;
delays=alltrip(:)*nan;
kinImuDelays=delays;
nrow=4;
slvID=1; %Id of the appropriate person in Slave Kinect
dsamp=100; %sampling interval in ms for registration
ord=[1 6:13 2:5 14:length(d)];
soi={1:2 4:6 7:9}; %Time-windows of interest
clear NallN Nall
clear Ntrials Nntrials
clear Nalltrials Nnalltrials
clear Ctrials Cntrials
for ordNum=1:length(d)
    
    subjectNo=ord(ordNum);
    if subjectNo==6
        domHand=7;
    else
        domHand=11;
    end
    fprintf('Subject #%i, %s:\n',ordNum,d(subjectNo).name);
    
    %These contain the participant IDs for each kinect datapoint
    load(sprintf('%s/groupsMaster.mat',d(subjectNo).name))
    grpMaster=grpNew;
    load(sprintf('%s/groupsSlave.mat',d(subjectNo).name))
    grpSlave=grpNew;
    
    %Load the Kinect and IMU data
    load([baseDir '/matDataKinectAndIMU/' d(subjectNo).name '.mat']);
    
    % This assumes most of the delay is caused by the slow starting of the
    % second Kinect, rather than delays in the network on the way back
    delays(subjectNo)=round(alltrip(subjectNo)/100);
    dataKinSlave(2:3,:)=dataKinSlave(2:3,:)+alltrip(subjectNo);
    %Unwrap the angles (converting to radians and back where necessary)
    for k=1:4
        dataSens{k}(:,2:4,4)=unwrap(dataSens{k}(:,2:4,4)/360*2*pi)/2/pi*360;
        dataSens{k}(:,2:4,5)=unwrap(dataSens{k}(:,2:4,5));
    end
    
    %Find the unique labels and move the most common label as the first one
    uni=unique(dataKinSlave(1,:));
    unimode=mode(dataKinSlave(1,:));
    uni(uni==unimode)=[];
    uni=[unimode; uni(:)];
    %Count the number of datapoints for each label and drop those that have
    %less than N samples; IDs with less than ~100 samples seem to
    %negatively impact the results quite severely, so they are dropped.
    N=100;
    unilen=uni*0;
    dropData=false(size(dataKinSlave(1,:)));
    for k=1:length(uni)
        unilen(k)=sum(dataKinSlave(1,:)==uni(k));
        if unilen(k)<N
            dropData(dataKinSlave(1,:)==uni(k))=true;
        end
    end
    dataKinSlave(:,dropData)=[];
    
    %Drop data as above, but for the other Kinect
    uni=unique(dataKin(1,:));
    unimode=mode(dataKin(1,:));
    uni(uni==unimode)=[];
    uni=[unimode; uni(:)];
    unilen=uni*0;
    dropData=false(size(dataKin(1,:)));
    
    for k=1:length(uni)
        unilen(k)=sum(dataKin(1,:)==uni(k));
        if unilen(k)<N
            dropData(dataKin(1,:)==uni(k))=true;
        end
    end
    dataKin(:,dropData)=[];
    
    %Make the gaze point for easier transformation
    vector=[0 0 -1]; %This is the reference direction (directly at Kinect, i.e. negative Z-direction)
    
    %Convert the Euler angles to rotation matrices (uses Robotics system
    %toolbox, to be replaced by manual calculations) with the default
    %rotation order (ZYX)
    rotm2=eul2rotm(dataKin([9 8 7],:)'/360*2*pi);
    for t=1:length(dataKin)
        dataKin(88:90,t)=dataKin(4:6,t)+rotm2(:,:,t)*vector';
    end
    
    rotm1=eul2rotm(dataKinSlave([9 8 7],:)'/360*2*pi);
    for t=1:length(dataKinSlave)
        dataKinSlave(88:90,t)=dataKinSlave(4:6,t)+rotm1(:,:,t)*vector';
    end
    
%-------------------------------------------------------------------------
%     OLD WAY OF CALCULATING: gives problems due to head roll, which is
%     omitted here
%-------------------------------------------------------------------------
%     dataKin(88,:)=dataKin(4,:)-sind(dataKin(8,:));
%     dataKin(89,:)=dataKin(5,:)+sind(dataKin(7,:));
%     dataKin(90,:)=dataKin(6,:)-cosd(dataKin(8,:));
%     dataKinSlave(88,:)=dataKinSlave(4,:)-sind(dataKinSlave(8,:));
%     dataKinSlave(89,:)=dataKinSlave(5,:)+sind(dataKinSlave(7,:));
%     dataKinSlave(90,:)=dataKinSlave(6,:)-cosd(dataKinSlave(8,:));
%-------------------------------------------------------------------------
    
    %Find the data points that correspond to the person that is visible in
    %both Kinects
    masterIdx=find(grpMaster==1);
    slaveIdx=find(grpSlave==slvID);
    
    %Drop data points where the timestamps are not monotonically
    %increasing (sometimes there are double samples with same timestamp)
    grpMaster(masterIdx(diff(dataKin(2,masterIdx))<=0))=[];
    grpSlave(slaveIdx(diff(dataKinSlave(2,slaveIdx))<=0))=[];
    dataKin(:,masterIdx(diff(dataKin(2,masterIdx))<=0))=[];
    dataKinSlave(:,slaveIdx(diff(dataKinSlave(2,slaveIdx))<=0))=[];
    masterIdx=find(grpMaster==1);
    
    %These are the indices of the "registration" participant in Slave sensor
    slaveIdx=find(grpSlave==slvID);
    %These are the indices of the participant the Slave sensor is oriented
    %to primarily track
    slaveTargetIdx=find(grpSlave==(3-slvID));
    %Drop points with no orientation data (zeros are saved when the
    %tracking fails)
    slaveTargetIdx(dataKinSlave(8,slaveTargetIdx)==0)=[];
    
    %Find samples that have the same timestamp, this happens sometimes when
    %the experimenter is misclassified. Drop the duplicate data that is
    %further away from the mean location of the subject of interest
    %(similar to above, but for "slave target" and with the extra distance
    %step)
    duplicates=find(diff(dataKinSlave(2,slaveTargetIdx))<=0);
    for ll=length(duplicates):-1:1
        cid=duplicates(ll)+(0:1);
        sampD=sqrt(mean((dataKinSlave(4:6,slaveTargetIdx(cid))-repmat(mean(dataKinSlave(4:6,slaveTargetIdx),2),[1 2])).^2));
        slaveTargetIdx(cid(sampD==max(sampD)))=[];
    end
    
    %The beginning and end time. Overlapping time between both Kinects
    tmax=min(max(dataKinSlave(2,:)),max(dataKin(2,:)));
    tmin=max(min(dataKinSlave(2,:)),min(dataKin(2,:)));
    %Vector of time stamps in the overlapping area for interpolation
    tWin=linspace(tmin,tmax,(tmax-tmin)/dsamp);
    
    %Find time windows that have actual data for the "registration subject"
    %from both kinects, making the registration possible
    [hbarSlave,hbinSlave]=hist(dataKinSlave(2,slaveIdx),tWin);
    [hbarMaster,hbinMaster]=hist(dataKin(2,grpMaster==1),tWin);
    tWin(or(hbarSlave==0,hbarMaster==0))=[];
    %Joints of interest (neck, head, right shoulder, spine at shoulder)
    joi=[2 3 8 20];
    %Interpolate the point clouds for the "registration participant" inside
    %the shared time window for registration
    %Reference point cloud for registration
    fixed=cat(3,interp1(dataKinSlave(2,slaveIdx),dataKinSlave(12+(joi(1)*3)+(1:3),slaveIdx)',tWin),...
        interp1(dataKinSlave(2,slaveIdx),dataKinSlave(12+(joi(2)*3)+(1:3),slaveIdx)',tWin),...
        interp1(dataKinSlave(2,slaveIdx),dataKinSlave(12+(joi(3)*3)+(1:3),slaveIdx)',tWin),...
        interp1(dataKinSlave(2,slaveIdx),dataKinSlave(12+(joi(4)*3)+(1:3),slaveIdx)',tWin));
    
    %Point cloud that is to be fitted to the reference
    moving=cat(3,interp1(dataKin(2,masterIdx),dataKin(12+(joi(1)*3)+(1:3),masterIdx)',tWin),...
        interp1(dataKin(2,masterIdx),dataKin(12+(joi(2)*3)+(1:3),masterIdx)',tWin),...
        interp1(dataKin(2,masterIdx),dataKin(12+(joi(3)*3)+(1:3),masterIdx)',tWin),...
        interp1(dataKin(2,masterIdx),dataKin(12+(joi(4)*3)+(1:3),masterIdx)',tWin));
    
    %Drop any NaNs in the data (usually missing data in the beginning/end)
    dropnan=or(max(max(isnan(moving),[],2),[],3),max(max(isnan(fixed),[],2),[],3));
    fixed(dropnan,:,:)=[];
    moving(dropnan,:,:)=[];
    tWin(dropnan)=[];
    
    %Reshape the data to concatenate the different joints
    fixed=reshape(permute(fixed,[1 3 2]),[],size(fixed,2));
    moving=reshape(permute(moving,[1 3 2]),[],size(moving,2));
    
    %Cross-correlation functions for re-aligning the two Kinects due to
    %lags in data synchronization during recording
    xcFun{subjectNo}=xcorr(sqrt(mean((moving-repmat(mean(moving),[size(moving,1) 1])).^2,2)),sqrt(mean((fixed-repmat(mean(fixed),[size(fixed,1) 1])).^2,2)),maxD);
%     delays(subjectNo)=find(xcFun{subjectNo}==max(xcFun{subjectNo}),1)-maxD-1;
    
    %Shift the data based on the maximum of the cross-correlation
%     moving=circshift(moving,-delays(subjectNo));
    %This drops the first and last samples, because the circular shift
    %moves samples from the end to the beginning or vice versa
    fixed(1:abs(delays(subjectNo)),:)=[];
    moving(1:abs(delays(subjectNo)),:)=[];
    fixed(end-abs(delays(subjectNo))+1:end,:)=[];
    moving(end-abs(delays(subjectNo))+1:end,:)=[];
    %Displays the delays for following what is happening
%     [delays(:) alltrip(:)]
    
    %Find the (root-mean squared error) optimized transformation between
    %the two point clouds
    [R,t,e,movingReg]=rigidBodyRegister(fixed',moving');
    movingReg=movingReg.'; %Transpose back
    
    %RMS error between the transformed data and the target sensor
    errVecs{subjectNo}=sqrt(mean((movingReg-fixed).^2,2));
    distFromMean{subjectNo}=[(movingReg-repmat(mean(movingReg),[size(movingReg,1) 1])) (fixed-repmat(mean(fixed),[size(fixed,1) 1]))];
    
    %Correlation of the location data betweeb sensors in X, Y and Z
    %direction and the distance from the mean location
    Rvals(:,subjectNo)=[diag(corr(movingReg,fixed)); corr(sqrt(mean(distFromMean{subjectNo}(:,1:3).^2,2)),sqrt(mean(distFromMean{subjectNo}(:,4:6).^2,2)))];
    
    %Data for the participant 1 (target for slave Kinect) from slave Kinect
    dataAll{subjectNo,1}=dataKinSlave([2 4 5 6 13:end],slaveTargetIdx);
    %Data for participant 2 from slave Kinect
    dataAll{subjectNo,2}=dataKinSlave([2 4 5 6 13:end],slaveIdx);
    %Data for participant 2 from master Kinect (target of master)
    dataAll{subjectNo,3}=[dataKin(2,masterIdx); R*dataKin(4:6,masterIdx)+repmat(t,[1 length(masterIdx)])];
    anglesAll{subjectNo,2}=dataKin(7:9,masterIdx);
    anglesAll{subjectNo,1}=dataKinSlave(7:9,slaveTargetIdx);
    
    %Transform all the joints
    for jnt=1:26 %25 joints plus the gaze point we calculated
        dataAll{subjectNo,3}(4+(jnt-1)*3+(1:3),:)=R*dataKin(12+(jnt-1)*3+(1:3),masterIdx)+repmat(t,[1 length(masterIdx)]);
    end
    %Timestamps according to host computer's clock (for finding the video
    %frames saved by the Kinect)
    timeImgPoints{subjectNo}=dataKinSlave(3,slaveTargetIdx);
    %Save the rotation matrices and translation vectors for future
    %reference
    Rall(:,:,subjectNo)=R;
    tAll(:,subjectNo)=t;
    
    % Timewindow with good data for both Kinects
    t0=max(dataAll{subjectNo,1}(1,1),dataAll{subjectNo,3}(1,1));
    tEnd=min(dataAll{subjectNo,1}(1,end),dataAll{subjectNo,3}(1,end));
    %Time vector in host computer time
    twinFull{subjectNo}=linspace(tmin,tmax,(tmax-tmin)/dsamp);
    %Time vector in Kinect time (with the offset t02 for master and t02s for slave)
    twinFullKin{subjectNo}=[twinFull{subjectNo}'+t02 twinFull{subjectNo}'+t02s];
    
    
    %% Make the time windows for each trial. This is quite a long block...
    %First, trial order for each participant (i.e. started with cooperative
    %or competitive)
    trialOrder=[1	1	0	0	1	1	0	0	1	1	0	0	1	1	1	1	0	0	1];
    trialOrder=trialOrder(namesReord);
    keyPressesAll{subjectNo}=keyPresses;
    keyPressesCut{subjectNo}=keyPressesAll{subjectNo}(1:7:end,:);
    
    switch subjectNo
        case 1
            dropdata=logical([0	0 1	0 0 1 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0]); %OK
            keyPressesCut{subjectNo}(dropdata,:)=[];
        case 6
            dropdata=logical([0 0 0 0 1 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0]); %OK
            keyPressesCut{subjectNo}(dropdata,:)=[];
        case 7
            dropdata=logical([0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0]); %OK
            keyPressesCut{subjectNo}(dropdata,:)=[];
            keyPressesCut{subjectNo}(11,2)={'4'};
        case 8
            %End marker of 6th Zitternix round is missing.
            %Original (commented out part) used the mean duration of the
            %setup time as an estimate for the break duration. This has
            %been replaced by manually annotated duration of the particular
            %trial (119300).
            keyPressesCut{subjectNo}(17:18,:)=keyPressesCut{subjectNo}(16:17,:);
            durs=diff(cell2mat(keyPressesCut{subjectNo}(:,1)));
            keyPressesCut{subjectNo}(16,1)={round(cell2mat(keyPressesCut{subjectNo}(15,1))+119300)};%{round(cell2mat(keyPressesCut{subjectNo}(17,1))-mean(durs(2:2:end-2)))};
            keyPressesCut{subjectNo}{16,2}='-';
        case 12
            keyPressesCut{subjectNo}(2,:)=[];
    end
    %%
    %After dropping unnecessary entries and adding missing ones, every
    %other entry should be the end point and denoted by a minus sign
    keyPressesCut{subjectNo}(2:2:end,2)={'-'};
    
    %Reorder so that first come the Conversations (in the order they
    %took place) then the practice trial, then three cooperative
    %games and finally three competitive games.
    if trialOrder(subjectNo)
        curOrd=[1 9 2 4 6 8 3 5 7];
        
    else
        curOrd=[1 9 2 3 5 7 4 6 8];
    end
    %%
    tWinAll{subjectNo}=[];
    %Cut the data into trials
    for trialNo=1:9
        if trialNo==1
            hold off;
        else
            hold on;
        end
        tPoints{subjectNo}=twinFull{subjectNo};
        tWin=and(tPoints{subjectNo}>cell2mat(keyPressesCut{subjectNo}((curOrd(trialNo)-1)*2+1)),tPoints{subjectNo}<cell2mat(keyPressesCut{subjectNo}((curOrd(trialNo)-1)*2+2)));
%         tWin((length(allYaw{subjectNo})+1):end)=[];
        tWinAll{subjectNo}(:,trialNo)=tWin;
        
    end
    tWinAllDrop=tWinAll;
    tWinAllDrop{subjectNo}(1:100,:)=0;
    tWinAllDrop{subjectNo}(end-99:end,:)=0;
%     tWinAll{subjectNo}=tWinAllNew{subjectNo};
    %%
    %Temporary timecourse variables for calculating distances etc.
    XYZ2=interp1(dataAll{subjectNo,3}(1,:),dataAll{subjectNo,3}([2:4 80:82],:)',...
        twinFull{subjectNo})';
    XYZ1=interp1(dataAll{subjectNo,1}(1,:),dataAll{subjectNo,1}([2:4 80:82],:)',...
        twinFull{subjectNo})';
    XYZn=interp1(dataAll{subjectNo,2}(1,:),dataAll{subjectNo,2}([2:4 80:82],:)',...
        twinFull{subjectNo})';
    anglesAllInt{subjectNo}(:,:,2)=interp1(dataAll{subjectNo,3}(1,:),anglesAll{subjectNo,2}',twinFull{subjectNo})';
    anglesAllInt{subjectNo}(:,:,1)=interp1(dataAll{subjectNo,1}(1,:),anglesAll{subjectNo,1}',twinFull{subjectNo})';
    %Little Gaussian filter with 0.3-second sigma
    for kj=1:6
        XYZ1(kj,:)=conv(XYZ1(kj,:),gaussKernel(27,3),'same');
        XYZ2(kj,:)=conv(XYZ2(kj,:),gaussKernel(27,3),'same');
        XYZn(kj,:)=conv(XYZn(kj,:),gaussKernel(27,3),'same');
    end
    
    %This is the approximate location of the target
    targetLoc{subjectNo}=[XYZ1(1,:); zeros(1,length(XYZ1)); XYZ2(3,:)];%[mean(dataAll{subjectNo,1}(2,:)) 0 XYZ2(3,:)];%[-.5 0 2.3];[mean(dataAll{subjectNo,1}(2,:)) 0 mean(dataAll{subjectNo,3}(4,:))];[mean(dataAll{subjectNo,1}(2,:)) 0 mean(dataAll{subjectNo,3}(4,:))];%[-.5 0 2.3];
    %Distances between participants at all timepoints based on both sensor
    %estimates of the location of participant 2
    distance{subjectNo}=zeros(length(XYZ1),2);
    distance{subjectNo}(:,1)=sqrt(mean((XYZ1(1:3,:)-XYZ2(1:3,:)).^2));
    distance{subjectNo}(:,2)=sqrt(mean((XYZ1(1:3,:)-XYZn(1:3,:)).^2));
    %
    %Angles for participant 1
    % 	ang12=atand((XYZ2(1,:)-XYZ1(1,:))./-(XYZ2(3,:)-XYZ1(3,:)));
    %     ang1=atand((XYZ1(4,:)-XYZ1(1,:))./-(XYZ1(6,:)-XYZ1(3,:)));
    %     ang1a=ang12-ang1;
    ang12wrap=atan2d(XYZ2(1,:)-XYZ1(1,:),-(XYZ2(3,:)-XYZ1(3,:)));
    ang12=ang12wrap+360*(ang12wrap<0);
    ang21=180+ang12;
    
    %Gaze angle of participant 1 in Kinect coordinates and in relation to
    %participant 2
    ang1wrap=atan2d(XYZ1(4,:)-XYZ1(1,:),(XYZ1(6,:)-XYZ1(3,:)));
    ang1=180-(ang1wrap+360*(ang1wrap<0));%+90;
    ang1a=ang12-ang1;%ang2+(ang2<0)*180-ang12;
    
    %Angles for participant 2, because of the orientation the calculation
    %differs somewhat from participant 1
    ang2wrap=atan2d(XYZ2(4,:)-XYZ2(1,:),-(XYZ2(6,:)-XYZ2(3,:)));
    ang2=ang2wrap+360*(ang2wrap<0);%+90;
    ang2a=ang2-ang21;%Reverse the order (sign) because the angles are mostly negative %ang2+(ang2<0)*180-ang12;
    
    %Angles from participants to target
    %     ang1t=atand((targetLoc{subjectNo}(1)-XYZ1(1,:))./-(targetLoc{subjectNo}(3)-XYZ1(3,:)));
    %     ang2t=atand((targetLoc{subjectNo}(1)-XYZ2(1,:))./-(targetLoc{subjectNo}(3)-XYZ2(3,:)));
    ang1twrap=atan2d(targetLoc{subjectNo}(1,:)-XYZ1(1,:),targetLoc{subjectNo}(3,:)-XYZ1(3,:)) ;
    ang1t=180-(ang1twrap+360*(ang1twrap<0));%+90;
    %     ang1at=ang12-ang1t;%ang2+(ang2<0)*180-ang12;
    
    ang2twrap=atan2d(targetLoc{subjectNo}(1,:)-XYZ2(1,:),-(targetLoc{subjectNo}(3,:)-XYZ2(3,:)));
    ang2t=ang2twrap+360*(ang2twrap<0);%+90;
    
    
    
    %Angle from participant gaze direction to target direction
    ang1at=ang1-ang1t;
    ang2at=ang2-ang2t;%+(ang2<0)*180-(180-ang2t);
    
    %Normalize the angle range between 0 (partner) and 1 (target)
    ang1T=abs(ang1t-ang12); %Total angle between partner and target
    ang2T=abs(ang2t-ang21);
    ang1N=ang1a./ang1T;
    ang2N=ang2a./ang2T;
    
    relYaw{subjectNo}=[ang1a(:) ang2a(:)];
    relYawNorm{subjectNo}=[ang1N(:) ang2N(:)];
    
    [N,Ca]=hist3([ang1a(:) ang2a(:)],{-30:5:90 -30:5:90});
    Nall(:,:,subjectNo)=N/sum(N(:));
    
    [Nn,Cn]=hist3([ang1N(:) ang2N(:)],{-1:.125:3 -1:.125:3});
    NallN(:,:,subjectNo)=Nn/sum(Nn(:));
    
    for trial=1:9
        ctrial=logical(tWinAllDrop{subjectNo}(:,trial));
        [Ntrials(:,:,trial),Ctrials(:,:,trial)]=hist3([ang1a(ctrial)' ang2a(ctrial)'],{-30:5:90 -30:5:90});
        Nalltrials(:,:,trial,subjectNo)=Ntrials(:,:,trial)/sum(sum(Ntrials(:,:,trial)));
        
        [Nntrials(:,:,trial),Cntrials(:,:,trial)]=hist3([ang1N(ctrial)' ang2N(ctrial)'],{-1:.125:3 -1:.125:3});
        Nnalltrials(:,:,trial,subjectNo)=Nntrials(:,:,trial)/sum(sum(Nntrials(:,:,trial)));
    end
    %     pause(1)
    
    
    %     hs3=scatter3(dataKinReor(1,masterIdx),dataKinReor(2,masterIdx),dataKinReor(3,masterIdx),'markerfacecolor',cols(4,:),'markeredgecolor','none','sizedata',5,'markerfacealpha',1/3);
    %% Make another tWin since we are done with the registration and want to compare uniformly sampled (interpolated) data
    tWin=linspace(tmin,tmax,(tmax-tmin)/dsamp);
    %Make a structure containing the time windows (as logical vectors) of
    %each of the different experimental conditions.
    %     condWins(
    eyeCon=[0 -15 60 210];
    dataSens{1}(diff(dataSens{1}(:,1,5))<=0,:,:)=[];
    dataSens{2}(diff(dataSens{2}(:,1,5))<=0,:,:)=[];
    dataSens{3}(diff(dataSens{3}(:,1,5))<=0,:,:)=[];
    dataSens{4}(diff(dataSens{4}(:,1,5))<=0,:,:)=[];
    yaw{1}=interp1(dataKin(2,masterIdx),dataKin(8,masterIdx)-eyeCon(1),tWin);
    dropstuf=dataKinSlave(2,slaveTargetIdx);
    yaw{2}=interp1(dataKinSlave(2,slaveTargetIdx),dataKinSlave(8,slaveTargetIdx)-eyeCon(2),tWin);
    
    %     yaw{3}=interp1(dataSens{4}(:,1,5),((dataSens{4}(:,2,5))-mean(dataSens{4}(:,2,5)))/2/pi*360,tWin);
    %     yaw{4}=interp1(dataSens{3}(:,1,5),((dataSens{3}(:,2,5))-mean(dataSens{3}(:,2,5)))/2/pi*360,tWin);
    yaw{3}=interp1(dataSens{4}(:,1,4),(dataSens{4}(:,2,4)-eyeCon(3)),tWin);
    yaw{4}=interp1(dataSens{3}(:,1,4),(dataSens{3}(:,2,4)-eyeCon(4)),tWin);
    
    pitch{1}=interp1(dataKin(2,masterIdx),dataKin(7,masterIdx),tWin);
    pitch{2}=interp1(dataKinSlave(2,slaveTargetIdx),dataKinSlave(7,slaveTargetIdx),tWin);
    
    %     yaw{3}=interp1(dataSens{4}(:,1,5),((dataSens{4}(:,2,5))-mean(dataSens{4}(:,2,5)))/2/pi*360,tWin);
    %     yaw{4}=interp1(dataSens{3}(:,1,5),((dataSens{3}(:,2,5))-mean(dataSens{3}(:,2,5)))/2/pi*360,tWin);
    pitch{3}=-120-interp1(dataSens{4}(:,1,4),(dataSens{4}(:,4,4)),tWin);
    pitch{4}=-120-interp1(dataSens{3}(:,1,4),(dataSens{3}(:,4,4)),tWin);
    
    roll{1}=interp1(dataKin(2,masterIdx),dataKin(9,masterIdx),tWin);
    roll{2}=interp1(dataKinSlave(2,slaveTargetIdx),dataKinSlave(9,slaveTargetIdx),tWin);
    
    %     yaw{3}=interp1(dataSens{4}(:,1,5),((dataSens{4}(:,2,5))-mean(dataSens{4}(:,2,5)))/2/pi*360,tWin);
    %     yaw{4}=interp1(dataSens{3}(:,1,5),((dataSens{3}(:,2,5))-mean(dataSens{3}(:,2,5)))/2/pi*360,tWin);
    roll{3}=interp1(dataSens{4}(:,1,4),(dataSens{4}(:,3,4)),tWin);
    roll{4}=interp1(dataSens{3}(:,1,4),(dataSens{3}(:,3,4)),tWin);
    %     yaw{3}=interp1(dataSens{4}(:,1,4),dataSens{4}(:,2,4),tWin);
    %     yaw{4}=interp1(dataSens{3}(:,1,4),dataSens{3}(:,2,4),tWin);
    %     length(masterIdx)==length(dataKin)
    temp=interp1(dataKin(2,masterIdx),dataKin(4:6,masterIdx)',tWin);
    temp1=sqrt(mean((temp-repmat(nanmean(temp),[size(temp,1) 1])).^2,2));
    temp=interp1(dataKinSlave(2,slaveTargetIdx),dataKinSlave(4:6,slaveTargetIdx)',tWin);
    temp2=sqrt(mean((temp-repmat(nanmean(temp),[size(temp,1) 1])).^2,2));
    displacement{subjectNo}=[tWin(:) temp1(:) temp2(:)];
    dropDisp=find(max(isnan(displacement{subjectNo}),[],2));
    keepDisp=find(~max(isnan(displacement{subjectNo}),[],2));
    displacement{subjectNo}(dropDisp,:)=[];
    tcInterp=[];
    %Interpolate data before convolution
    tcInterp(:,:,1)=interp1(dataKin(2,masterIdx),dataKin(4:6,masterIdx)',tWin);
    tcInterp(:,:,2)=interp1(dataKinSlave(2,slaveTargetIdx),dataKinSlave(4:6,slaveTargetIdx)',tWin);
    %Mean of the timecourses to be subtracted before and added back after
    %the convolution to reduce edge effects
    meanTC=repmat(nanmean(tcInterp),[size(tcInterp,1) 1 1]);
    accel{1}=sqrt(nanmean(diff((conv2(tcInterp(:,:,1)-meanTC(:,:,1),gaussKernel(27,2),'same')+meanTC(:,:,1)),2).^2,2))'./(diff(tWin(2:end))/1000);
    accel{2}=sqrt(nanmean(diff((conv2(tcInterp(:,:,2)-meanTC(:,:,2),gaussKernel(27,2),'same')+meanTC(:,:,2)),2).^2,2))'./(diff(tWin(2:end))/1000);
    accel{3}=conv(interp1(dataSens{4}(:,1,6),sqrt(sum(dataSens{4}(:,2:4,6).^2,2)),tWin(2:end-1)),gaussKernel(27,5),'same');%,gaussKernel(27,5),'same');
    accel{4}=conv(interp1(dataSens{3}(:,1,6),sqrt(sum(dataSens{3}(:,2:4,6).^2,2)),tWin(2:end-1)),gaussKernel(27,5),'same');%,gaussKernel(27,5),'same');
    accel{5}=interp1(dataSens{4}(:,1,1),abs(sqrt(sum(dataSens{4}(:,2:4,1).^2,2))-9.81),tWin(2:end-1));
    accel{6}=interp1(dataSens{3}(:,1,1),abs(sqrt(sum(dataSens{3}(:,2:4,1).^2,2))-9.81),tWin(2:end-1));
    sensAccel{subjectNo}=[];
    for kk=1:4
        sensAccel{subjectNo}(:,kk)=interp1(dataSens{kk}(:,1,6),sqrt(sum(dataSens{kk}(:,2:4,6).^2,2)),tWin(2:end));
    end
    %     accel{3}(1)=[];
    %     accel{4}(1)=[];
    %     accel{5}(1)=[];
    %     accel{6}(1)=[];
    headAccel{subjectNo}=[accel{1}(:) accel{2}(:) accel{3}(:) accel{4}(:)];
    
    %Because of delays (at least for one dyad) in starting data recording
    %on the Slave Kinect host causing loss of synchrony (apparent in the
    %cross-correlation function), this temporally realigns the data with a
    %constant shift (Note. each dyad has a very clear peak in the
    %cross-correlation function, so this seems justifiable).
    %This delay was only present in the "Slave" Kinect, so we do not
    %realign the other data sources.
    accNonNan=cat(2,headAccel{subjectNo}(:,2),headAccel{subjectNo}(:,4));
    for l=1:2
        accNonNan(isnan(accNonNan(:,l)),l)=nanmean(accNonNan(:,l));
    end
    xc=xcorr(zscore(accNonNan(:,1)),zscore(accNonNan(:,2)),50,'unbiased');
    xc=51-find(xc==max(xc));
    kinImuDelays(subjectNo)=xc;
%     headAccel{subjectNo}(:,2)=circshift(headAccel{subjectNo}(:,2),xc);
    
    %The IMUs sometimes fail at removing the acceleration due to gravity,
    %so we will exclude those samples from the reliability calculation.
    %This procedure is based on the scale of the current data. The time
    %windowing is then done to differentiate real peaks of acceleration
    %from the shifted baseline that usually lasts for several seconds.
    gravityOutliers=headAccel{subjectNo}(:,3:4)>1.5;
    for twinlen=1:30
        gravityOutliers=and([ones(1,2); diff(gravityOutliers)==0],gravityOutliers==1);
    end
    gravityOutliers=max(and([ones(1,2); diff(gravityOutliers)==0],gravityOutliers==1),[],2);
%     gravityOutliers(1:5000)=1;
%     gravityOutliers(end-4999:end)=1;
    %Drop also data before the first part of the experiment starts and
    %after the last part ends, because there may be inconsistent data (e.g.
    %Kinect tracking has not found the participant yet, or the participant
    %is removing the wearable sensor before data recording ends.)
    gravityOutliers(1:find(max(tWinAll{subjectNo},[],2),1,'first'))=1;
    gravityOutliers(find(max(tWinAll{subjectNo},[],2),1,'last'):end)=1;
    %This only focuses on the actual trials, where the correspondence
    %should be best
    gravityOutliers(max(tWinAll{subjectNo},[],2)==0)=1;
    
	allOutliers{subjectNo}=gravityOutliers;
    
    fprintf('Outliers %.2f %%\n',sum(gravityOutliers)/length(gravityOutliers)*100);
    cormats2(:,:,subjectNo)=corr(headAccel{subjectNo}(~gravityOutliers,:),'rows','pairwise');%corr(headAccel{subjectNo},'rows','pairwise');
    %Make the hand acceleration things
    %Interpolate data before convolution
    tcInterp(:,:,1)=interp1(dataKin(2,masterIdx),dataKin(4+(domHand-1)*3+(1:3),masterIdx)',tWin);
    tcInterp(:,:,2)=interp1(dataKinSlave(2,slaveTargetIdx),dataKinSlave(4+(domHand-1)*3+(1:3),slaveTargetIdx)',tWin);
    %Mean of the timecourses to be subtracted before and added back after
    %the convolution to reduce edge effects
    meanTC=repmat(nanmean(tcInterp),[size(tcInterp,1) 1 1]);
    accel{1}=sqrt(nanmean(diff((conv2(tcInterp(:,:,1)-meanTC(:,:,1),gaussKernel(27,5),'same')+meanTC(:,:,1)),2).^2,2))'./(diff(tWin(2:end))/1000);
    accel{2}=sqrt(nanmean(diff((conv2(tcInterp(:,:,2)-meanTC(:,:,2),gaussKernel(27,5),'same')+meanTC(:,:,2)),2).^2,2))'./(diff(tWin(2:end))/1000);
    accel{3}=interp1(dataSens{2}(:,1,6),sqrt(sum(dataSens{2}(:,2:4,6).^2,2)),tWin(2:end-1));%,gaussKernel(27,5),'same');
    accel{4}=interp1(dataSens{1}(:,1,6),sqrt(sum(dataSens{1}(:,2:4,6).^2,2)),tWin(2:end-1));%,gaussKernel(27,5),'same');
    accel{5}=interp1(dataSens{2}(:,1,1),abs(sqrt(sum(dataSens{2}(:,2:4,1).^2,2))-9.81),tWin(2:end-1));
    accel{6}=interp1(dataSens{1}(:,1,1),abs(sqrt(sum(dataSens{1}(:,2:4,1).^2,2))-9.81),tWin(2:end-1));
    sensAccel{subjectNo}=[];
    for kk=1:4
        sensAccel{subjectNo}(:,kk)=interp1(dataSens{kk}(:,1,6),sqrt(sum(dataSens{kk}(:,2:4,6).^2,2)),tWin(2:end));
    end
    %     accel{3}(1)=[];
    %     accel{4}(1)=[];
    %     accel{5}(1)=[];
    %     accel{6}(1)=[];
    handAccel{subjectNo}=[accel{1}(:) accel{2}(:) accel{3}(:) accel{4}(:)];
    
    for k=1:4
        accel{k}(isnan(accel{k}))=0;
%         accel{k}=conv(accel{k},gaussKernel(27,27/5),'same');
        accel{k}=filtfilt(gaussKernel(27,27/5),1,accel{k});
    end
    allAccel{subjectNo}=accel;
    accelCorrs(:,:,subjectNo)=corr(headAccel{subjectNo}(~max(isnan(headAccel{subjectNo}),[],2),:));
    accelCorrsHand(:,:,subjectNo)=corr(handAccel{subjectNo}(~max(isnan(handAccel{subjectNo}),[],2),:));
%     cormats2(:,:,subjectNo)=accelCorrs(:,:,subjectNo);
    
    %Speeds
    %Interpolate data before convolution
    tcInterp(:,:,1)=interp1(dataKin(2,masterIdx),dataKin(4:6,masterIdx)',tWin);
    tcInterp(:,:,2)=interp1(dataKinSlave(2,slaveTargetIdx),dataKinSlave(4:6,slaveTargetIdx)',tWin);
    meanTC=repmat(nanmean(tcInterp),[size(tcInterp,1) 1 1]);
    %
    speed{1}=sqrt(nanmean(diff((conv2(tcInterp(:,:,1)-meanTC(:,:,1),gaussKernel(27,2),'same')+meanTC(:,:,1))).^2,2))'./(diff(tWin(1:end))/1000);
    speed{2}=sqrt(nanmean(diff((conv2(tcInterp(:,:,2)-meanTC(:,:,2),gaussKernel(27,2),'same')+meanTC(:,:,2))).^2,2))'./(diff(tWin(1:end))/1000);
    headSpeed{subjectNo}=[speed{1}(:) speed{2}(:)];
    
    %Make the hand acceleration things
    %Interpolate data before convolution
    tcInterp(:,:,1)=interp1(dataKin(2,masterIdx),dataKin(4+(domHand-1)*3+(1:3),masterIdx)',tWin);
    tcInterp(:,:,2)=interp1(dataKinSlave(2,slaveTargetIdx),dataKinSlave(4+(domHand-1)*3+(1:3),slaveTargetIdx)',tWin);
    meanTC=repmat(nanmean(tcInterp),[size(tcInterp,1) 1 1]);
    %Mean of the timecourses to be subtracted before and added back after
    %the convolution to reduce edge effects
    speed{1}=sqrt(nanmean(diff((conv2(tcInterp(:,:,1)-meanTC(:,:,1),gaussKernel(27,2),'same')+meanTC(:,:,1))).^2,2))'./(diff(tWin(1:end))/1000);
    speed{2}=sqrt(nanmean(diff((conv2(tcInterp(:,:,2)-meanTC(:,:,2),gaussKernel(27,2),'same')+meanTC(:,:,2))).^2,2))'./(diff(tWin(1:end))/1000);
    handSpeed{subjectNo}=[speed{1}(:) speed{2}(:)];
    
    
    %Drop any NaNs as well as values that are exactly zero, since the
    %face angle tracking at those points did not work
    dropnan=logical(isnan(yaw{1})+isnan(yaw{2})+isnan(yaw{3})+isnan(yaw{4})+...
        (yaw{1}==0)+(yaw{2}==0)+(yaw{3}==0)+(yaw{4}==0));
    nancut=false(size(tWin));
    
    for k=1:4
        %         yaw{k}(dropnan)=[];
        %         pitch{k}(dropnan)=[];
        %         roll{k}(dropnan)=[];
        yaw{k}=interp1(tWin(~dropnan),yaw{k}(~dropnan),tWin);
        pitch{k}=interp1(tWin(~dropnan),pitch{k}(~dropnan),tWin);
        roll{k}=interp1(tWin(~dropnan),roll{k}(~dropnan),tWin);
        
        %This should only have nans in the beginning or the end
        nancut=or(isnan(yaw{k}),nancut);
        
        if k>2 %Supposedly, the angles work in different directions in IMUs vs. Kinect
            yaw{k}=-yaw{k};
        end
    end
    nancut(:)=false;
    for k=1:4
        yaw{k}(nancut)=[];
        %         yaw{k}=(yaw{k})/360*2*pi;
        pitch{k}(nancut)=[];
        roll{k}(nancut)=[];
    end
    dropnan(nancut)=[];
    tWin(nancut)=[];
    tWinAll{subjectNo}(nancut,:)=[];
    displacement{subjectNo}(nancut,:)=[];
    for l=1:4
        roll{l}=conv(roll{l},gaussKernel(27,27/5),'same');
        yaw{l}=conv(yaw{l},gaussKernel(27,27/5),'same');
        pitch{l}=conv(pitch{l},gaussKernel(27,27/5),'same');
    end
    allroll{subjectNo}=cell2mat(roll')';
    %%
    %     hfigHmap=figure;
    %     set(hfigHmap,'position',[300 300 700 350])
    
    for k=1:2
        %         ha=axes;
        %         axis equal tight
        tempTC=[conv(yaw{(k-1)*2+1},gaussKernel(27*2,27*2/5),'same')' conv(yaw{(k-1)*2+2},gaussKernel(27,27/5),'same')'];
        [N,C]=hist3(tempTC,{(-45:5:45) (-45:5:45)});%{linspace(-pi/4,pi/4,41) linspace(-pi/2,pi/2,41)});
        %         imagesc(N);
        %         grid
        allN(:,:,subjectNo,k)=N;
        stepn=1/diff(C{1}(1:2))*2.5;
        %         set(ha,'position',[0.075+(k-1)*.5 0.12 .35 .8],'xtick',1:stepn:length(C{1}),'ytick',1:stepn:length(C{2}),'xticklabel',C{1}(1:stepn:length(C{1})),'yticklabel',C{2}(1:stepn:length(C{2})))
        %         xlabel('Direction S1 (degrees)')
        %         ylabel('Direction S2 (degrees)')
        %         if k==2
        %             title(sprintf('Dyad %s, Kinect',d(subjectNo).name));
        %         else
        %             title(sprintf('Dyad %s, IMU',d(subjectNo).name));
        %         end
        
    end
    %     drawnow
    %%

    for l=1:4
        yaw{l}=conv(yaw{l},gaussKernel(27,27/5),'same');
    end
    
%     yaw{2}=circshift(yaw{2},xc);
    allyaw{subjectNo}=cell2mat(yaw')';
    tempyaw=cell2mat(yaw');
    
    cormats(:,:,subjectNo)=corr(tempyaw','rows','pairwise');%nancorr(tempyaw');%(:,logical(max(tWinAll{subjectNo}')))');
    %     figure(hfig2);
    %     subplot(nrow,ceil(length(d)/nrow),ordNum);%subjectNo);
    %     imagesc(cormats(:,:,subjectNo));
    %     title(d(subjectNo).name);
    %     set(gca,'clim',[-1 1]);
    %     drawnow;
    %     %%
    %     hfigHmapP=figure;
    %     set(hfigHmapP,'position',[300 300 700 350])
    
    for k=1:2
        %         ha=axes;
        %         axis equal tight
        
        [N,C]=hist3([conv(pitch{(k-1)*2+1},gaussKernel(27*2,27*2/5),'same')' conv(pitch{(k-1)*2+2},gaussKernel(27,27/5),'same')'],{(-90:5:15) (-90:5:15)});
        %         imagesc(N);
        %         grid
        allNP(:,:,subjectNo,k)=N;
        stepn=1/diff(C{1}(1:2))*30;
        %         set(ha,'position',[0.075+(k-1)*.5 0.12 .35 .8],'xtick',1:stepn:length(C{1}),'ytick',1:stepn:length(C{2}),'xticklabel',C{1}(1:stepn:length(C{1})),'yticklabel',C{2}(1:stepn:length(C{2})))
        %         xlabel('Direction S1 (degrees)')
        %         ylabel('Direction S2 (degrees)')
        %         if k==2
        %             title(sprintf('Dyad %s, Kinect',d(subjectNo).name));
        %         else
        %             title(sprintf('Dyad %s, IMU',d(subjectNo).name));
        %         end
        
    end
    %     drawnow
    %%
    
    for l=1:4
        pitch{l}=conv(pitch{l},gaussKernel(27,27/5),'same');
    end
    allpitch{subjectNo}=cell2mat(pitch')';
    cormatsP(:,:,subjectNo)=corr(cell2mat(pitch')');
    %     figure(hfig3);
    %     subplot(nrow,ceil(length(d)/nrow),ordNum);%subjectNo);
    %     imagesc(cormatsP(:,:,subjectNo));
    %     title(d(subjectNo).name);
    %     set(gca,'clim',[-1 1]);
    %     drawnow;
    save([baseDir '/matDataKinectAndIMU/' d(subjectNo).name '_orientations.mat'],'yaw','pitch','roll','tWin','dropnan');
end
fprintf('\ndone\n');

%% Correlation plots between Kinects
Labels={'Base of the spine' %0
    'Middle of the spine'   %1)
    'Neck'                  %2  x
    'Head'                  %3  x
    'Left shoulder'         %4
    'Left elbow'            %5
    'Left wrist'            %6
    'Left hand'             %7
    'Right shoulder'        %8  x
    'Right elbow'           %9
    'Right wrist'           %10
    'Right hand'            %11
    'Left hip'              %12
    'Left knee'             %13
    'Left ankle'            %14
    'Left foot'             %15
    'Right hip'             %16
    'Right knee'            %17
    'Right ankle'           %18
    'Right foot'            %19
    'Spine at the shoulder' %20 x
    'Tip of the left hand'  %21
    'Left thumb'            %22
    'Tip of the right hand' %23
    'Right thumb'};         %24
visible=[0 .5 1 1 1 0 0 0 1 1 1 1 0 0 0 0 0 0 0 0 1 0 0 1 1]*2+1;
for subjectNo=1:18
    tmax=min(max(dataAll{subjectNo,2}(1,:)),max(dataAll{subjectNo,3}(1,:)));
    tmin=max(min(dataAll{subjectNo,2}(1,:)),min(dataAll{subjectNo,3}(1,:)));
    tWin=linspace(tmin,tmax,(tmax-tmin)/dsamp);
    %find time windows that have actual data
    [hbarSlave,hbinSlave]=hist(dataAll{subjectNo,2}(1,:),tWin);
    [hbarMaster,hbinMaster]=hist(dataAll{subjectNo,3}(1,:),tWin);
    tWin(or(hbarSlave==0,hbarMaster==0))=[];
    for jj=1:length(Labels)
        j=4+((jj-1)*3)+1;
        for cc=0:2
            crArr(subjectNo,jj,cc+1)=corr(interp1(dataAll{subjectNo,3}(1,:),dataAll{subjectNo,3}(j+cc,:),tWin(51:end-49))',interp1(dataAll{subjectNo,2}(1,:),dataAll{subjectNo,2}(j+cc,:),tWin(51:end-49))');
            rsub=1:length(d);
            rsub(subjectNo)=[];
            rsub=rsub(randperm(length(rsub)));
            rsub=rsub(1);
            crNull(subjectNo,jj,cc+1)=corr(interp1(dataAll{subjectNo,3}(1,:),dataAll{subjectNo,3}(j+cc,:),tWin(51:end-49))',interp1(dataAll{rsub,2}(1,:),dataAll{rsub,2}(j+cc,:),tWin(51:end-49))');
        end
    end
end

figure;
apos=[.095 .325 .88 .65];
cmap=.7+repmat([0; .5; 1],[1 3])*.3;
ha1=axes;
hold on;
image(permute(repmat(cmap([3; reshape(repmat(visible(:),[1 2])',[],1); 3],:),[1 1 10]),[3 1 2]))
axis tight;
set(ha1,'visible','off','position',apos)
ha2=axes;
BML_violinPlot(nanmean(crArr,3),[],[1 1 1],1,1,0,6);%aaltoColors(3))
set(ha2,'XTick',1:length(Labels),'XTickLabel',Labels,'XTickLabelRotation',90,'Color','none')
set(ha2,'position',apos)
ylabel('Kinect to Kinect correlation');
plot([0 size(crArr,2)+1],max(nanmean(nanmean(crNull,3)))*[1 1],'k')
plot([0 size(crArr,2)+1],max(nanmax(nanmean(crNull,3)))*[1 1],'k--')
plot([0 size(crArr,2)+1],max(nanmin(nanmean(crNull,3)))*[1 1],'k--')

%% IMU-Kinect correlation plots
figure;

set(gcf,'position',[500 100 500 1000]);
subplot(2,1,1);
BML_violinPlot([squeeze(cormats(1,3,:)) squeeze(cormats(2,4,:))],[],[],1,0,1);
set(gca,'xtick',[1 2],'xticklabel',{'Kinect 1','Kinect 2'});
title(sprintf('Yaw angle, r=%.2f±%.2f',...
    mean([squeeze(cormats(1,3,:));squeeze(cormats(2,4,:))]),...
    std([squeeze(cormats(1,3,:));squeeze(cormats(2,4,:))])));
xlabel('Number of participants');
ylabel('Correlation');
subplot(2,1,2);
BML_violinPlot([squeeze(cormats2(1,3,:)) squeeze(cormats2(2,4,:))],[],[],1,0,1);
set(gca,'xtick',[1 2],'xticklabel',{'Kinect 1','Kinect 2'},'ylim',[0 1]);
title(sprintf('Accelerations, r=%.2f±%.2f',...
    mean([squeeze(cormats2(1,3,:));squeeze(cormats2(2,4,:))]),...
    std([squeeze(cormats2(1,3,:));squeeze(cormats2(2,4,:))])));
xlabel('Number of participants');
ylabel('Correlation');
%% Registration error distribution
figure;
hold on;
clear errHists;
for k=1:18
    [hbar,hbin]=hist(errVecs{k}*100,0:.25:15);
    errHists(:,k)=hbar/sum(hbar);
end
plotComponentErrors2(errHists',hbin);
xlabel('RMS error (cm)')
ylabel('Fraction of timepoints');
%% Speed similarity and synchrony (hand)
trialNames={'Conv1','Conv2','Practice','Coop1','Coop2','Coop3','Comp1','Comp2','Comp3'};
% for l=1:2
%     for k=1:18
%         [hbard,hbind]=hist(abs(diff(displacement{k}(:,l+1)))./diff(displacement{k}(:,1)/1000),0:.01:10);
%         dispBars(:,k+(l-1)*18)=hbard/nansum(hbard);
%     end
% end


%     'Base of the spine'     %0
%     'Middle of the spine'   %1)
%     'Neck'                  %2  x
%     'Head'                  %3  x
%     'Left shoulder'         %4
%     'Left elbow'            %5
%     'Left wrist'            %6
%     'Left hand'             %7
%     'Right shoulder'        %8  x
%     'Right elbow'           %9
%     'Right wrist'           %10
%     'Right hand'            %11
%     'Left hip'              %12
%     'Left knee'             %13
%     'Left ankle'            %14
%     'Left foot'             %15
%     'Right hip'             %16
%     'Right knee'            %17
%     'Right ankle'           %18
%     'Right foot'            %19
%     'Spine at the shoulder' %20 x
%     'Tip of the left hand'  %21
%     'Left thumb'            %22
%     'Tip of the right hand' %23
%     'Right thumb'           %24
head=3;
shoulderspine=20;
neck=2;
rightshoulder=8;

for k=1:18
    %     speeds{k}=abs(diff(displacement{k}(:,2:3)))./repmat(diff(displacement{k}(:,1)/1000),[1 2]);
    subjectNo=k; %Lazy fix for different loop variable
    if subjectNo==6
        domHand=7;
    else
        domHand=11;
    end
    jj=domHand;%head
    j=4+((jj+1)*3)+(1:3);
    
    speeds{subjectNo}=[sqrt(sum(diff(conv2(interp1(dataAll{subjectNo,1}(1,:)',dataAll{subjectNo,1}(j,:)',twinFull{subjectNo}),gaussKernel(27,3))).^2,2))...
                       sqrt(sum(diff(conv2(interp1(dataAll{subjectNo,2}(1,:)',dataAll{subjectNo,2}(j,:)',twinFull{subjectNo}),gaussKernel(27,3))).^2,2))...
                       sqrt(sum(diff(conv2(interp1(dataAll{subjectNo,3}(1,:)',dataAll{subjectNo,3}(j,:)',twinFull{subjectNo}),gaussKernel(27,3))).^2,2))];
	dispHand{subjectNo}=[sqrt(sum((conv2(interp1(dataAll{subjectNo,1}(1,:)',dataAll{subjectNo,1}(j,:)',twinFull{subjectNo}),gaussKernel(27,3))).^2,2))...
                       sqrt(sum((conv2(interp1(dataAll{subjectNo,2}(1,:)',dataAll{subjectNo,2}(j,:)',twinFull{subjectNo}),gaussKernel(27,3))).^2,2))...
                       sqrt(sum((conv2(interp1(dataAll{subjectNo,3}(1,:)',dataAll{subjectNo,3}(j,:)',twinFull{subjectNo}),gaussKernel(27,3))).^2,2))];
	idxInc{subjectNo}=100:length(speeds{subjectNo})-99;
    for l=1:3
        speeds{subjectNo}(isnan(speeds{subjectNo}(:,l)),l)=nanmean(speeds{subjectNo}(:,l));%Replace nans by the mean
    end
    speedCorr(k)=corr(speeds{k}(:,1),speeds{k}(:,2));
    for trial=1:9
        speedsTrial{k,trial}=abs(speeds{k}(logical(tWinAllDrop{k}(2:end,trial)),[1 3]));
        speedCorrTrials(k,trial)=corr(speedsTrial{k,trial}(:,1),speedsTrial{k,trial}(:,2));
        trialLens(k,trial)=size(speedsTrial{k,trial},1);
    end
end

%Full behavioral data/demographic data
ord=[1 6:13 2:5 14:length(d)];
rateMat=[5.5  5.6  4.6  4.5  5.8  6.4  5.8  5.9  4.8  5.6  5.4  6.9  5.1  4.6  5.1  5.0  4.5  3.4  5.4  4.6  6.0  5.5  5.3  4.9  6.8  5.0  4.8  5.6  4.4  3.6  2.5  3.4  5.1  5.9  5.6  6.1  6.3  5.3;
         6.7  6.9  4.7  5.0  6.7  6.6  6.9  5.3  5.6  6.7  6.4  7.0  5.4  6.0  6.4  5.7  6.3  4.7  6.1  5.7  6.6  5.6  5.9  4.9  7.0  6.3  4.6  6.6  5.7  4.0  3.0  4.4  5.6  6.1  6.1  6.6  6.6  5.4;
         3.6  4.4  2.0  4.8  4.8  6.4  3.6  4.8  4.2  4.8  4.8  4.8  5.8  5.6  3.2  2.8  4.8  2.2  5.6  2.2  3.8  3.0  4.6  3.2  5.6  5.2  4.4  4.6  4.6  1.8  2.4  4.8  5.2  6.6  4.2  4.4  4.0  4.4;
         2.4  2.0  2.6  2.0  2.2  1.0  3.2  1.4  1.0  3.2  3.0  1.0  1.6  1.4  1.6  3.4  2.4  2.4  2.2  1.2  2.2  2.0  2.8  1.8  3.0  3.6  2.8  3.2  2.8  1.2  3.0  2.2  2.4  1.0  2.4  2.2  1.8  2.6;
         4.0  4.5  3.8  4.5  2.3  5.5  4.0  4.3  2.8  5.0  4.0  4.3  6.0  3.5  5.5  4.3  5.8  6.8  5.3  6.0  5.3  5.3  4.0  5.5  2.5  3.8  4.8  6.5  5.0  4.5  4.5  4.8  3.3  6.0  6.3  5.3  5.5  5.0];
subNames={'F1Y' 'F1R' 'F2Y' 'F2R' 'F3Y' 'F3R' 'F4Y' 'F4R' 'F5Y' 'F5R' 'F6Y' 'F6R' 'F7Y' 'F7R' 'F8Y' 'F8R' 'F9Y' 'F9R' 'F10Y' 'F10R' 'F11Y' 'F11R' 'F12Y' 'F12R' 'F13Y' 'F13R' 'F14Y' 'F14R' 'M1Y' 'M1R' 'M2Y' 'M2R' 'M3Y' 'M3R' 'M4Y' 'M4R' 'M5Y' 'M5R'};
dropSubs=false(length(subNames),1);
% dropSubs(19:20)=true; %F10 dyad had technical issues (no Kinect data)
% rateMat(:,dropSubs)=[];
% subNames(dropSubs)=[];
qdata=[];

fId=fopen([baseDir '/QuestionnaireData/ACIPS_AQ_SNQ_AGE_sorted_corrected.csv']);
line=fgetl(fId);
coms=strfind(line,',');
qTitles={line(coms(1)+1:coms(2)-1) line(coms(2)+1:coms(3)-1) line(coms(3)+1:coms(4)-1) line(coms(4)+1:coms(4)+3)};
for k=1:length(subNames)
    line=fgetl(fId);
    coms=strfind(line,',');
    subNamesFile{k}=line(1:coms(1)-1);
    qdata(:,k)=[str2num(line(coms(1)+1:coms(2)-1)) str2num(line(coms(2)+1:coms(3)-1)) str2num(line(coms(3)+1:coms(4)-1)) str2num(line(coms(4)+1:end))];
end
for k=1:size(qdata,1)
    qdata(k,isnan(qdata(k,:)))=nanmean(qdata(k,:)); %Because some participants' data is missing, let's replace it with the mean
end
%SNQ seems to be misunderstood by some participants so badly, that it
%cannot be used for them. To be safe, we omit the whole scale for now.
qdata(3,:)=[];
qTitles(3)=[];

pairNames=subNames(1:2:end);
subOrd=[1:2*9 2*10+1:length(rateMat)];
for k=1:length(pairNames) 
    pairNames{k}(end)=[];
end
rateTitles2={'Relatedness'
            'Enjoyment'
            'Choice'
            'Pressure'%/tension'
            'Effort'};
rateTitles2={rateTitles2{:} qTitles{:}}
rateMat=[rateMat; qdata]
rate1=rateMat(:,1:2:end); %Rating for participant 1 (Yellow)
rate2=rateMat(:,2:2:end); %Rating for participant 2 (Red)
pairNames={'F1' 'F2' 'F3' 'F4' 'F5' 'F6' 'F7' 'F8' 'F9' 'F10' 'F11' 'F12' 'F13' 'F14' 'M1' 'M2' 'M3' 'M4' 'M5'}
namesReord=[1 11:14 2:9 15:19]; 
rate1=rate1(:,namesReord);
rate2=rate2(:,namesReord);

%% Speed similarity and synchrony (head)
trialNames={'Conv1','Conv2','Practice','Coop1','Coop2','Coop3','Comp1','Comp2','Comp3'};
% for l=1:2
%     for k=1:18
%         [hbard,hbind]=hist(abs(diff(displacement{k}(:,l+1)))./diff(displacement{k}(:,1)/1000),0:.01:10);
%         dispBars(:,k+(l-1)*18)=hbard/nansum(hbard);
%     end
% end


%     'Base of the spine'     %0
%     'Middle of the spine'   %1)
%     'Neck'                  %2  x
%     'Head'                  %3  x
%     'Left shoulder'         %4
%     'Left elbow'            %5
%     'Left wrist'            %6
%     'Left hand'             %7
%     'Right shoulder'        %8  x
%     'Right elbow'           %9
%     'Right wrist'           %10
%     'Right hand'            %11
%     'Left hip'              %12
%     'Left knee'             %13
%     'Left ankle'            %14
%     'Left foot'             %15
%     'Right hip'             %16
%     'Right knee'            %17
%     'Right ankle'           %18
%     'Right foot'            %19
%     'Spine at the shoulder' %20 x
%     'Tip of the left hand'  %21
%     'Left thumb'            %22
%     'Tip of the right hand' %23
%     'Right thumb'           %24
head=3;
shoulderspine=20;
neck=2;
rightshoulder=8;

for k=1:18
    %     speeds{k}=abs(diff(displacement{k}(:,2:3)))./repmat(diff(displacement{k}(:,1)/1000),[1 2]);
    subjectNo=k; %Lazy fix for different loop variable
    if subjectNo==6
        domHand=7;
    else
        domHand=11;
    end
    jj=head;
    j=4+((jj+1)*3)+(1:3);
    
    speedsHead{subjectNo}=[sqrt(sum(diff(conv2(interp1(dataAll{subjectNo,1}(1,:)',dataAll{subjectNo,1}(j,:)',twinFull{subjectNo}),gaussKernel(27,3))).^2,2))...
                       sqrt(sum(diff(conv2(interp1(dataAll{subjectNo,2}(1,:)',dataAll{subjectNo,2}(j,:)',twinFull{subjectNo}),gaussKernel(27,3))).^2,2))...
                       sqrt(sum(diff(conv2(interp1(dataAll{subjectNo,3}(1,:)',dataAll{subjectNo,3}(j,:)',twinFull{subjectNo}),gaussKernel(27,3))).^2,2))];
    dispHead{subjectNo}=[sqrt(sum((conv2(interp1(dataAll{subjectNo,1}(1,:)',dataAll{subjectNo,1}(j,:)',twinFull{subjectNo}),gaussKernel(27,3))).^2,2))...
                       sqrt(sum((conv2(interp1(dataAll{subjectNo,2}(1,:)',dataAll{subjectNo,2}(j,:)',twinFull{subjectNo}),gaussKernel(27,3))).^2,2))...
                       sqrt(sum((conv2(interp1(dataAll{subjectNo,3}(1,:)',dataAll{subjectNo,3}(j,:)',twinFull{subjectNo}),gaussKernel(27,3))).^2,2))];
	idxInc{subjectNo}=100:length(speeds{subjectNo})-99;
    for l=1:3
        speedsHead{subjectNo}(isnan(speedsHead{subjectNo}(:,l)),l)=nanmean(speedsHead{subjectNo}(:,l));%Replace nans by the mean
    end
    speedCorrHead(k)=corr(speedsHead{k}(:,1),speedsHead{k}(:,2));
    for trial=1:9
        speedsTrialHead{k,trial}=abs(speedsHead{k}(logical(tWinAllDrop{k}(2:end,trial)),[1 3]));
        speedCorrTrialsHead(k,trial)=corr(speedsTrialHead{k,trial}(:,1),speedsTrialHead{k,trial}(:,2));
        trialLensHead(k,trial)=size(speedsTrialHead{k,trial},1);
    end
end

%Full behavioral data/demographic data
ord=[1 6:13 2:5 14:length(d)];
rateMat=[5.5  5.6  4.6  4.5  5.8  6.4  5.8  5.9  4.8  5.6  5.4  6.9  5.1  4.6  5.1  5.0  4.5  3.4  5.4  4.6  6.0  5.5  5.3  4.9  6.8  5.0  4.8  5.6  4.4  3.6  2.5  3.4  5.1  5.9  5.6  6.1  6.3  5.3;
         6.7  6.9  4.7  5.0  6.7  6.6  6.9  5.3  5.6  6.7  6.4  7.0  5.4  6.0  6.4  5.7  6.3  4.7  6.1  5.7  6.6  5.6  5.9  4.9  7.0  6.3  4.6  6.6  5.7  4.0  3.0  4.4  5.6  6.1  6.1  6.6  6.6  5.4;
         3.6  4.4  2.0  4.8  4.8  6.4  3.6  4.8  4.2  4.8  4.8  4.8  5.8  5.6  3.2  2.8  4.8  2.2  5.6  2.2  3.8  3.0  4.6  3.2  5.6  5.2  4.4  4.6  4.6  1.8  2.4  4.8  5.2  6.6  4.2  4.4  4.0  4.4;
         2.4  2.0  2.6  2.0  2.2  1.0  3.2  1.4  1.0  3.2  3.0  1.0  1.6  1.4  1.6  3.4  2.4  2.4  2.2  1.2  2.2  2.0  2.8  1.8  3.0  3.6  2.8  3.2  2.8  1.2  3.0  2.2  2.4  1.0  2.4  2.2  1.8  2.6;
         4.0  4.5  3.8  4.5  2.3  5.5  4.0  4.3  2.8  5.0  4.0  4.3  6.0  3.5  5.5  4.3  5.8  6.8  5.3  6.0  5.3  5.3  4.0  5.5  2.5  3.8  4.8  6.5  5.0  4.5  4.5  4.8  3.3  6.0  6.3  5.3  5.5  5.0];
subNames={'F1Y' 'F1R' 'F2Y' 'F2R' 'F3Y' 'F3R' 'F4Y' 'F4R' 'F5Y' 'F5R' 'F6Y' 'F6R' 'F7Y' 'F7R' 'F8Y' 'F8R' 'F9Y' 'F9R' 'F10Y' 'F10R' 'F11Y' 'F11R' 'F12Y' 'F12R' 'F13Y' 'F13R' 'F14Y' 'F14R' 'M1Y' 'M1R' 'M2Y' 'M2R' 'M3Y' 'M3R' 'M4Y' 'M4R' 'M5Y' 'M5R'};
dropSubs=false(length(subNames),1);
% dropSubs(19:20)=true; %F10 dyad had technical issues (no Kinect data)
% rateMat(:,dropSubs)=[];
% subNames(dropSubs)=[];
qdata=[];

fId=fopen([baseDir '/QuestionnaireData/ACIPS_AQ_SNQ_AGE_sorted_corrected.csv']);
line=fgetl(fId);
coms=strfind(line,',');
qTitles={line(coms(1)+1:coms(2)-1) line(coms(2)+1:coms(3)-1) line(coms(3)+1:coms(4)-1) line(coms(4)+1:coms(4)+3)};
for k=1:length(subNames)
    line=fgetl(fId);
    coms=strfind(line,',');
    subNamesFile{k}=line(1:coms(1)-1);
    qdata(:,k)=[str2num(line(coms(1)+1:coms(2)-1)) str2num(line(coms(2)+1:coms(3)-1)) str2num(line(coms(3)+1:coms(4)-1)) str2num(line(coms(4)+1:coms(5)))];
end
for k=1:size(qdata,1)
    qdata(k,isnan(qdata(k,:)))=nanmean(qdata(k,:)); %Because some participants' data is missing, let's replace it with the mean
end

%SNQ seems to be misunderstood by some participants so badly, that it
%cannot be used for them. To be safe, we omit the whole scale for now.
qdata(3,:)=[];
qTitles(3)=[];

pairNames=subNames(1:2:end);
subOrd=[1:2*9 2*10+1:length(rateMat)];
for k=1:length(pairNames) 
    pairNames{k}(end)=[];
end
rateTitles2={'Relatedness'
            'Enjoyment'
            'Choice'
            'Pressure'%/tension'
            'Effort'};
rateTitles2={rateTitles2{:} qTitles{:}}
rateMat=[rateMat; qdata]
rate1=rateMat(:,1:2:end); %Rating for participant 1 (Yellow)
rate2=rateMat(:,2:2:end); %Rating for participant 2 (Red)
pairNames={'F1' 'F2' 'F3' 'F4' 'F5' 'F6' 'F7' 'F8' 'F9' 'F10' 'F11' 'F12' 'F13' 'F14' 'M1' 'M2' 'M3' 'M4' 'M5'}
namesReord=[1 11:14 2:9 15:19]; 
rate1=rate1(:,namesReord);
rate2=rate2(:,namesReord);
%% Calculate windowed cross-correlations for real data
fprintf('Windowed cross-correlations:\n');
winSize=300;
maxLag=150; %50
hop=10;
trialLens=floor((cell2mat(cellfun(@length,speedsTrial,'uniformoutput',false))-winSize)/hop);
minlen=min(trialLens(trialLens>0)); %Shortest length for a valid trial (shorter ones will not be (completely) processed, longer ones will be cropped to this length)
% hfig=figure;

XCmeanHandCut=[];
XCmeanHeadCut=[];
XCfullwinHeadCut=[];
XCfullwinHandCut=[];
for sub=1:18
    fprintf('%i ',sub);
    
    for trial=1:9
        
        fprintf('.');
        
        
        %Cross-correlations for real data for HEAD
        %smooth the speed timecourses a bit, since they are quite noisy
        [winXCHead,maxValHead{sub,trial},maxLocHead{sub,trial},peakValHead{sub,trial},peakLocHead{sub,trial}]=windowedCrossCorrelation(conv(speedsTrialHead{sub,trial}(:,1),gaussKernel(27,3)),conv(speedsTrialHead{sub,trial}(:,2),gaussKernel(27,3)),winSize,maxLag,hop);
        XCtrialsHead{sub,trial}=winXCHead;
        meanXCsHead(sub,trial)=mean(winXCHead(:));
        XCmaxMatHead(sub,trial)=mean(maxValHead{sub,trial}(1:min(length(maxValHead{sub,trial}),minlen)));
        XCpeakMatHead(sub,trial)=mean(peakValHead{sub,trial}(1:min(length(maxValHead{sub,trial}),minlen)));
        XCmidMatHead(sub,trial)=mean(winXCHead(maxLag+1,1:min(length(maxValHead{sub,trial}),minlen)));
        XCmaxLocMatHead(sub,trial)=mean(maxLocHead{sub,trial});
        XCpeakLocMatHead(sub,trial)=mean(peakLocHead{sub,trial});
        XCmeanHead(:,trial,sub)=nanmean(winXCHead,2);
        if size(winXCHead,2)>minlen
            XCmeanHeadCut(:,trial,sub)=nanmean(winXCHead(:,1:minlen),2);
            XCfullwinHeadCut(:,trial,sub)=xcorr(conv(speedsTrialHead{sub,trial}(:,1),gaussKernel(27,3)),conv(speedsTrialHead{sub,trial}(:,2),gaussKernel(27,3)),maxLag);
        else
            XCmeanHeadCut(:,trial,sub)=nan;
            XCfullwinHeadCut(:,trial,sub)=nan;
        end
        
        
        %Cross-correlations for real data for HAND
        %smooth the speed timecourses a bit, since they are quite noisy
        [winXCHand,maxValHand{sub,trial},maxLocHand{sub,trial},peakValHand{sub,trial},peakLocHand{sub,trial}]=windowedCrossCorrelation(conv(speedsTrial{sub,trial}(:,1),gaussKernel(27,3)),conv(speedsTrial{sub,trial}(:,2),gaussKernel(27,3)),winSize,maxLag,hop);
        XCtrialsHand{sub,trial}=winXCHand;
        meanXCsHand(sub,trial)=mean(winXCHand(:));
        XCmaxMatHand(sub,trial)=mean(maxValHand{sub,trial}(1:min(length(maxValHead{sub,trial}),minlen)));
        XCpeakMatHand(sub,trial)=mean(peakValHand{sub,trial}(1:min(length(maxValHead{sub,trial}),minlen)));
        XCmidMatHand(sub,trial)=mean(winXCHand(maxLag+1,1:min(length(maxValHead{sub,trial}),minlen)));
        XCmaxLocMatHand(sub,trial)=mean(maxLocHand{sub,trial});
        XCpeakLocMatHand(sub,trial)=mean(peakLocHand{sub,trial});
        XCmeanHand(:,trial,sub)=nanmean(winXCHand,2);
        if size(winXCHand,2)>minlen
            XCmeanHandCut(:,trial,sub)=nanmean(winXCHand(:,1:minlen),2);
            XCfullwinHandCut(:,trial,sub)=xcorr(conv(speedsTrial{sub,trial}(:,1),gaussKernel(27,3)),conv(speedsTrial{sub,trial}(:,2),gaussKernel(27,3)),maxLag);
        else
            XCmeanHandCut(:,trial,sub)=nan;
            XCfullwinHandCut(:,trial,sub)=nan;
        end
        
    end
end
fprintf('\ndone\n');
%% Null distribution for windowed cross-correlations (Takes an hour or two to calculate)
for sub=1:18
    fprintf('-------------------------------------------------\n');
    fprintf('Dyad %i\n',sub);
    fprintf('-------------------------------------------------\n');
    fprintf('\nPermutations\n');
    for cperm=1:10%0
        %For null data, mix dyads
        sub2=1:18;
        sub2(sub)=[];
        sub2=sub2(randperm(length(sub2)));
        sub2=sub2(1);
        fprintf('%i',cperm);
        for trial=1:9
            %For null data, mix trials
            trial2=1:9;
            trial2(trial)=[];
            trial2=trial2(randperm(length(trial2)));
            trial2=trial2(1);
            
            fprintf('.');
            %Cross-correlations for null data
            [winXC,maxVal{sub,trial,cperm},maxLoc{sub,trial},peakVal{sub,trial},peakLoc{sub,trial}]=windowedCrossCorrelation(conv(speedsTrial{sub,trial}(:,1),gaussKernel(27,3)),conv(circshift(speedsTrial{sub2,trial2}(:,2),round(rand*500)),gaussKernel(27,3)),winSize,maxLag,1);
            XCtrialsNull{sub,trial,cperm}=winXC;
            meanXCsNull(sub,trial,cperm)=mean(winXC(:));
            XCmaxMatNull(sub,trial,cperm)=mean(maxVal{sub,trial});
            XCpeakMatNull(sub,trial,cperm)=mean(peakVal{sub,trial});
            XCmidMatNull(sub,trial,cperm)=mean(winXC(maxLag+1,:));
            XCmaxLocMatNull(sub,trial,cperm)=mean(maxLoc{sub,trial});
            XCpeakLocMatNull(sub,trial,cperm)=mean(peakLoc{sub,trial});
        end
    end
    fprintf('\n');
end

for trial=1:9
    meanXCsHead(isnan(meanXCsHead(:,trial)),trial)=nanmean(meanXCsHead(:,trial));
    meanXCsHand(isnan(meanXCsHand(:,trial)),trial)=nanmean(meanXCsHand(:,trial));
    meanXCsNull(isnan(meanXCsNull(:,trial)),trial)=nanmean(meanXCsNull(:,trial));
end

%% Create cross-correlation plots (zero-lag, peak-picking and maximum)
%Not included in the final paper because none of these values reflect
%well the full cross-correlation structure with the side peaks etc.

nulldata{1}=reshape(mean(XCmidMatNull),[],1);
nulldata{2}=reshape(mean(XCpeakMatNull),[],1);
nulldata{3}=reshape(mean(XCmaxMatNull),[],1);
hfig=figure;
set(hfig,'position',[400 300 1400 600])

subplot(2,4,1);
title('No lag');
semipFit=paretotails(nulldata{1}(~isnan(nulldata{1})),.1,.9);
hold on;
plotComponentErrors2(XCmidMatHead,[],[],[],[],aaltoColors(4));
% BML_violinPlot(XCmidMat,[],aaltoColors(3)/3+2/3);
plot([.5 9.5],repmat(icdf(semipFit,[.05 .95]),[2 1]),'-.','color',aaltoColors(4));
plot([.5 9.5],repmat(icdf(semipFit,.5),[2 2])','color',aaltoColors(4));
ylabel('Cross-correlation (r)')
set(gca,'xtick',1:9,'xticklabel',trialNames,'xticklabelrotation',45)
axis([.5 9.5 0 .7])

subplot(2,4,2);title('Peak-picking');
semipFit=paretotails(nulldata{2}(~isnan(nulldata{2})),.1,.9);
hold on;
plotComponentErrors2(XCpeakMatHead,[],[],[],[],aaltoColors(4));
% BML_violinPlot(XCpeakMat,[],aaltoColors(2)/3+2/3);
plot([.5 9.5],repmat(icdf(semipFit,[.05 .95]),[2 1]),'-.','color',aaltoColors(4));
plot([.5 9.5],repmat(icdf(semipFit,.5),[2 2])','color',aaltoColors(4));
set(gca,'xtick',1:9,'xticklabel',trialNames,'xticklabelrotation',45)
axis([.5 9.5 0 .7])

subplot(2,4,3);
title('Maximum');
semipFit=paretotails(nulldata{3}(~isnan(nulldata{3})),.1,.9);
hold on;
% BML_violinPlot(XCmaxMat,9,aaltoColors(6)/3+2/3);
plotComponentErrors2(XCmaxMatHead,[],[],[],[],aaltoColors(4));
plot([.5 9.5],repmat(icdf(semipFit,[.05 .95]),[2 1]),'-.','color',aaltoColors(4));
plot([.5 9.5],repmat(icdf(semipFit,.5),[2 2])','color',aaltoColors(4));
set(gca,'xtick',1:9,'xticklabel',trialNames,'xticklabelrotation',45)
axis([.5 9.5 0 .7])

subplot(2,4,4);
hold on
bins=1:5:maxLag*2+1;
[hbar,hbin]=hist(cell2mat(reshape(peakLocHead(:,1:2),[],1)),bins);
plot(hbin,hbar/sum(hbar),'color',aaltoColors(4));
[hbar,hbin]=hist(cell2mat(reshape(peakLocHead(:,3),[],1)),bins);
plot(hbin,hbar/sum(hbar),'color',aaltoColors(5));
[hbar,hbin]=hist(cell2mat(reshape(peakLocHead(:,4:6),[],1)),bins);
plot(hbin,hbar/sum(hbar),'color',aaltoColors(3));
[hbar,hbin]=hist(cell2mat(reshape(peakLocHead(:,7:9),[],1)),bins);
plot(hbin,hbar/sum(hbar),'color',aaltoColors(2));
legend('Conversation','Practice','Cooperative','Competitive')
axis tight
set(gca,'xtick',1:(maxLag/2):maxLag*2+1,'xticklabel',(-maxLag:(maxLag/2):maxLag)/10);
xlabel('Lag (s)')
ylabel('Probability density')

%Hand

subplot(2,4,5);
title('No lag');
semipFit=paretotails(nulldata{1}(~isnan(nulldata{1})),.1,.9);
hold on;
plotComponentErrors2(XCmidMatHand,[],[],[],[],aaltoColors(4));
% BML_violinPlot(XCmidMat,[],aaltoColors(3)/3+2/3);
plot([.5 9.5],repmat(icdf(semipFit,[.05 .95]),[2 1]),'-.','color',aaltoColors(4));
plot([.5 9.5],repmat(icdf(semipFit,.5),[2 2])','color',aaltoColors(4));
ylabel('Cross-correlation (r)')
set(gca,'xtick',1:9,'xticklabel',trialNames,'xticklabelrotation',45)
axis([.5 9.5 0 .7])

subplot(2,4,6);title('Peak-picking');
semipFit=paretotails(nulldata{2}(~isnan(nulldata{2})),.1,.9);
hold on;
plotComponentErrors2(XCpeakMatHand,[],[],[],[],aaltoColors(4));
% BML_violinPlot(XCpeakMat,[],aaltoColors(2)/3+2/3);
plot([.5 9.5],repmat(icdf(semipFit,[.05 .95]),[2 1]),'-.','color',aaltoColors(4));
plot([.5 9.5],repmat(icdf(semipFit,.5),[2 2])','color',aaltoColors(4));
set(gca,'xtick',1:9,'xticklabel',trialNames,'xticklabelrotation',45)
axis([.5 9.5 0 .7])

subplot(2,4,7);
title('Maximum');
semipFit=paretotails(nulldata{3}(~isnan(nulldata{3})),.1,.9);
hold on;
% BML_violinPlot(XCmaxMat,9,aaltoColors(6)/3+2/3);
plotComponentErrors2(XCmaxMatHand,[],[],[],[],aaltoColors(4));
plot([.5 9.5],repmat(icdf(semipFit,[.05 .95]),[2 1]),'-.','color',aaltoColors(4));
plot([.5 9.5],repmat(icdf(semipFit,.5),[2 2])','color',aaltoColors(4));
set(gca,'xtick',1:9,'xticklabel',trialNames,'xticklabelrotation',45)
axis([.5 9.5 0 .7])

subplot(2,4,8);
hold on
bins=1:5:maxLag*2+1;
[hbar,hbin]=hist(cell2mat(reshape(peakLocHand(:,1:2),[],1)),bins);
plot(hbin,hbar/sum(hbar),'color',aaltoColors(4));
[hbar,hbin]=hist(cell2mat(reshape(peakLocHand(:,3),[],1)),bins);
plot(hbin,hbar/sum(hbar),'color',aaltoColors(5));
[hbar,hbin]=hist(cell2mat(reshape(peakLocHand(:,4:6),[],1)),bins);
plot(hbin,hbar/sum(hbar),'color',aaltoColors(3));
[hbar,hbin]=hist(cell2mat(reshape(peakLocHand(:,7:9),[],1)),bins);
plot(hbin,hbar/sum(hbar),'color',aaltoColors(2));
legend('Conversation','Practice','Cooperative','Competitive')
axis tight
set(gca,'xtick',1:(maxLag/2):maxLag*2+1,'xticklabel',(-maxLag:(maxLag/2):maxLag)/10);
xlabel('Lag (s)')
ylabel('Probability density')


%% Differences between mean within-condition windowed cross-correlations of the HEADS, FULL LENGTH

%null stuff
XCfullNull=[];
for trial=1:9
    for cperm=1:10
        temppp=[];
        for sub=1:18
            temppp(:,sub)=nanmean(XCtrialsNull{sub,trial,cperm},2);
        end
        XCfullNull=cat(2,XCfullNull,nanmean(temppp,2));
    end
end
qnts=quantile(XCfullNull(:),[.05 .5 .95]);
colOrd=aaltoColors([1 5 3 2]);
% colOrd(1,:)=colOrd(1,:)*3/4;
soiCur={1:2 3 4:6 7:9};
condNames={'Conversation','Practice','Cooperative','Competitive'};
nn=length(soiCur);
figure('position',[0 300 1920 1920/7*2],'color',[1 1 1]);
tempmaxlag=15;
tbins=-tempmaxlag:.1:tempmaxlag;
ii=0;
ratecors=[];
ratepartcors=[];

condType=[];
for k=1:4
    condType(soiCur{k})=k;
end
subtable=permute(repmat((1:18)',[1 size(XCmeanHead,2)]),[2 1]);
condtable=repmat([1 1 2 3 3 3 4 4 4]',[1 size(XCmeanHead,3)]);
XCmeanHeadTemp=XCmeanHead;
XCmeanHeadTemp(isnan(XCmeanHead))=nanmean(XCmeanHead(:));
xcAnova=rm_anova2multi(reshape(XCmeanHeadTemp,size(XCmeanHead,1),[])',subtable(:),condtable(:),condtable(:)*0+1,{'Condition' 'Constant'});
%Proportion of timepoints showing a significant effect of condition
sum(xcAnova.p(1,:)<.05)/size(xcAnova.p,2)
for tpoint=1:301
t = table((1:18)',...
          squeeze(mean(XCmeanHead(tpoint,1:2,:),2)),...
          squeeze(XCmeanHead(tpoint,3,:)),...
          squeeze(mean(XCmeanHead(tpoint,4:6,:),2)),...
          squeeze(mean(XCmeanHead(tpoint,7:9,:),2)),...
          'VariableNames',{'Sub' condNames{:}});
Meas = table([1 2 3 4]','VariableNames',{'Conditions'});
rm = fitrm(t,sprintf('%s,%s,%s,%s ~ Sub',condNames{:}),'WithinDesign',Meas);
ranovatbl = ranova(rm);
pRMvec(tpoint)=ranovatbl{2,5};
FRMvec(tpoint)=ranovatbl{2,4};
end
for k=1:nn
    for l=k+1:nn
        ii=ii+1;
        axes('position',[.1/2.5+(ii-1)/6 .57 1/6-.05 .78/2]);%subplot(1,nn*(nn-1)/2,ii);
        if ii==1
            ylabel('Cross-correlation (r)');
            xlabel('Lag (s)');
        end
        title([condNames{k} ' âˆ’ ' condNames{l}]);%trialNames{k});
        %Condition-specific mean cross-correlation
        temp1=squeeze(nanmean(XCmeanHead(abs(tbins)<=tempmaxlag,soiCur{k},:),2))';
        temp2=squeeze(nanmean(XCmeanHead(abs(tbins)<=tempmaxlag,soiCur{l},:),2))';
%         temp1=(temp1+fliplr(temp1))/2;
%         temp2=(temp2+fliplr(temp2))/2;
        for sub=1:size(temp1,1)
            [pks,locs] = findpeaks(temp1(sub,:));
            locs(pks<0)=[];
            pks(pks<0)=[];
            locdist=abs(locs-ceil(length(temp1(sub,:))/2));
            mainpeakloc(sub,1)=locs(find(locdist==min(locdist),1));
            mainpeakheight(sub,1)=pks(find(locdist==min(locdist),1));
            
            
            [pks,locs] = findpeaks(temp2(sub,:));
            locs(pks<0)=[];
            pks(pks<0)=[];
            locdist=abs(locs-ceil(length(temp1(sub,:))/2));
            mainpeakloc(sub,2)=locs(find(locdist==min(locdist),1));
            mainpeakheight(sub,2)=pks(find(locdist==min(locdist),1));
            
        end
        
        [~,ptemp]=ttest(temp1,temp2);
        hold on;
        hptch=patch([tbins(abs(tbins)<=tempmaxlag) tempmaxlag -tempmaxlag],.45*[ptemp<(.05/(6)) 0 0]-.15,aaltoColors(5)/3+2/3);
                plot(tbins(abs(tbins)<=tempmaxlag),.45*(xcAnova.p(1,:)<.05)-.15,'k')
        set(hptch,'edgecolor','none');
        
        
        plotComponentErrors2(temp1,tbins,[],[],[],colOrd(k,:));
        plotComponentErrors2(temp2,tbins,[],[],[],colOrd(l,:));
        plot(tbins(mainpeakloc(:,1)),mainpeakheight(:,1),'x','color',colOrd(k,:));%.27*ones(size(mainpeakloc(:,1))),'x','color',colOrd(k,:));
        plot(tbins(mainpeakloc(:,2)),mainpeakheight(:,2),'x','color',colOrd(l,:));%.28*ones(size(mainpeakloc(:,2))),'x','color',colOrd(l,:));
        plot(tbins,qnts(1)*ones(size(tbins)),'k-.');%,color',[.7 .7 .7]);
        plot(tbins,qnts(2)*ones(size(tbins)),'k--');
        plot(tbins,qnts(3)*ones(size(tbins)),'k-.');%,color',[.7 .7 .7]);
%         plotComponentErrors2(temp1-temp2,tbins,[],[],[],[0 0 0]);
        axis([-15 15 -.15 .3])
        grid on
        drawnow;
    end
    
    temp1=squeeze(nanmean(XCmeanHead(abs(tbins)<=tempmaxlag,soiCur{k},:),2))';
    for sub=1:size(temp1,1)
        [pks,locs] = findpeaks(temp1(sub,:));
        locs(pks<0)=[];
        pks(pks<0)=[];
        locdist=abs(locs-ceil(length(temp1(sub,:))/2));
        peakheights(sub,k)=pks(find(locdist==min(locdist),1));
    end
    axes('position',[.1+(k-1)/5 .07 1/5-.05 .78/2]);%subplot(1,nn*(nn-1)/2,ii);
    
   
    %Partial correlation after removing outliers
    rateMeanFull=(rate1+rate2)'/2;
%     rateMeanFull=abs(rate1'-rate2');%rateMeanFull(:,8)=rate1(8,:)-rate2(8,:));
    
    rateMean=rateMeanFull;
    outlierMat=isoutlier(rateMeanFull,'mean');%abs((rateMeanFull-repmat(mean(rateMeanFull),[size(rateMeanFull,1) 1]))./repmat(std(rateMeanFull),[size(rateMeanFull,1) 1]))>3;
    rateMean(outlierMat)=nan;
    
    ratecors(:,1)=corr(peakheights(:,k),rateMeanFull);
    ratecors(:,2)=corr(temp1(:,(size(temp1,2)-1)/2+1),rateMeanFull);
    for curRat=1:size(rateMeanFull,2)
        ratepartcors(curRat,1)=partialcorr(peakheights(:,k),rateMeanFull(:,curRat),double(outlierMat(:,curRat)));
        ratepartcors(curRat,2)=partialcorr(temp1(:,(size(temp1,2)-1)/2+1),rateMeanFull(:,curRat),double(outlierMat(:,curRat)));
    end
    
%     ratecors(:,3)=corr(abs(mainpeakloc(:,1)),rateMean');
    hold on
    hba=barh(ratecors((end:-1:1),:));
    set(hba(1),'facecolor',[1 1 1])
    set(hba(2),'facecolor',colOrd(k,:))
    hba2=barh(ratepartcors((end:-1:1),:));%hba2=barh([(1:8)'-.2 (1:8)'+.2],ratepartcors((end:-1:1),:));
    set(hba2(1),'facecolor',[.8 .8 .8],'facealpha',.5)
    set(hba2(2),'facecolor',(colOrd(k,:)+1)/2,'facealpha',.5)
%     set(hba(3),'facecolor',[0 0 0])
    hold on;
    plot(probabilityToCorrelation(18,.05)*[1 1],[.5 length(rateTitles2)+.5],'k--')
    plot(probabilityToCorrelation(18,.05)*-[1 1],[.5 length(rateTitles2)+.5],'k--')
    title(condNames{k});
    axis([-.8 .8 .5 length(rateTitles2)+.5])
    colormap gray;
    if k==1
        set(gca,'ytick',1:length(rateTitles2),'yticklabel',rateTitles2(end:-1:1));
        xlabel('Correlation (r)');
    else
        set(gca,'ytick',1:length(rateTitles2),'yticklabel',[]);
    end
    if k==4
        hleg=legend('Peak height','Zero-lag','Partial correlation');%,'Peak lag');
        set(hleg,'position',[.1/2.5+(ii-1)/6 .35 .075 .46-.35])
    end
end
%% Differences between mean within-condition windowed cross-correlations of the HANDS, FULL LENGTH
colOrd=aaltoColors([1 5 3 2]);
% colOrd(1,:)=colOrd(1,:)*3/4;
soiCur={1:2 3 4:6 7:9};
condNames={'Conversation','Practice','Cooperative','Competitive'};
nn=length(soiCur);
figure('position',[0 300 1920 1920/7*2],'color',[1 1 1]);
tempmaxlag=15;
tbins=-tempmaxlag:.1:tempmaxlag;

XCmeanHandTemp=XCmeanHand;
XCmeanHandTemp(isnan(XCmeanHand))=nanmean(XCmeanHand(:));
xcAnovaHand=rm_anova2multi(reshape(XCmeanHandTemp,size(XCmeanHand,1),[])',subtable(:),condtable(:),condtable(:)*0+1,{'Condition' 'Constant'});
%Proportion of timepoints showing a significant effect of condition
sum(xcAnovaHand.p(1,:)<.05)/size(xcAnovaHand.p,2)

ii=0;
ratecors=[];
ratepartcors=[];
for k=1:nn
    for l=k+1:nn
        ii=ii+1;
        axes('position',[.1/2.5+(ii-1)/6 .57 1/6-.05 .78/2]);%subplot(1,nn*(nn-1)/2,ii);
        if ii==1
            ylabel('Cross-correlation (r)');
            xlabel('Lag (s)');
        end
        title([condNames{k} ' âˆ’ ' condNames{l}]);%trialNames{k});
        %Condition-specific mean cross-correlation
        temp1=squeeze(nanmean(XCmeanHand(abs(tbins)<=tempmaxlag,soiCur{k},:),2))';
        temp2=squeeze(nanmean(XCmeanHand(abs(tbins)<=tempmaxlag,soiCur{l},:),2))';
%         temp1=(temp1+fliplr(temp1))/2;
%         temp2=(temp2+fliplr(temp2))/2;
        for sub=1:size(temp1,1)
            [pks,locs] = findpeaks(temp1(sub,:));
            locs(pks<0)=[];
            pks(pks<0)=[];
            locdist=abs(locs-ceil(length(temp1(sub,:))/2));
            mainpeakloc(sub,1)=locs(find(locdist==min(locdist),1));
            mainpeakheight(sub,1)=pks(find(locdist==min(locdist),1));
            
            
            [pks,locs] = findpeaks(temp2(sub,:));
            locs(pks<0)=[];
            pks(pks<0)=[];
            locdist=abs(locs-ceil(length(temp1(sub,:))/2));
            mainpeakloc(sub,2)=locs(find(locdist==min(locdist),1));
            mainpeakheight(sub,2)=pks(find(locdist==min(locdist),1));
            
        end
        
        [~,ptemp]=ttest(temp1,temp2);
        hold on;
        hptch=patch([tbins(abs(tbins)<=tempmaxlag) tempmaxlag -tempmaxlag],.45*[ptemp<(.05/(6)) 0 0]-.15,aaltoColors(5)/3+2/3);
        plot(tbins(abs(tbins)<=tempmaxlag),.45*(xcAnovaHand.p(1,:)<.05)-.15,'k')
        set(hptch,'edgecolor','none');
        
        plotComponentErrors2(temp1,tbins,[],[],[],colOrd(k,:));
        plotComponentErrors2(temp2,tbins,[],[],[],colOrd(l,:));
        plot(tbins(mainpeakloc(:,1)),mainpeakheight(:,1),'x','color',colOrd(k,:));%.27*ones(size(mainpeakloc(:,1))),'x','color',colOrd(k,:));
        plot(tbins(mainpeakloc(:,2)),mainpeakheight(:,2),'x','color',colOrd(l,:));%.28*ones(size(mainpeakloc(:,2))),'x','color',colOrd(l,:));
%         plotComponentErrors2(temp1-temp2,tbins,[],[],[],[0 0 0]);
        axis([-15 15 -.15 .3])
        grid on
        drawnow;
    end
    
    temp1=squeeze(nanmean(XCmeanHand(abs(tbins)<=tempmaxlag,soiCur{k},:),2))';
    for sub=1:size(temp1,1)
        [pks,locs] = findpeaks(temp1(sub,:));
        locs(pks<0)=[];
        pks(pks<0)=[];
        locdist=abs(locs-ceil(length(temp1(sub,:))/2));
        peakheights(sub,k)=pks(find(locdist==min(locdist),1));
    end
    axes('position',[.1+(k-1)/5 .07 1/5-.05 .78/2]);%subplot(1,nn*(nn-1)/2,ii);
    
    ratecors(:,1)=corr(peakheights(:,k),rate1'+rate2');
    ratecors(:,2)=corr(temp1(:,(size(temp1,2)-1)/2+1),rate1'+rate2');
    %Partial correlation after removing outliers
    rateMeanFull=(rate1+rate2)'/2;
    %Difference of scores
%     rateMeanFull=abs(rate1'-rate2');
    rateMean=rateMeanFull;
    outlierMat=isoutlier(rateMeanFull,'mean');%abs((rateMeanFull-repmat(mean(rateMeanFull),[size(rateMeanFull,1) 1]))./repmat(std(rateMeanFull),[size(rateMeanFull,1) 1]))>3;
    rateMean(outlierMat)=nan;
    ratecors(:,1)=corr(peakheights(:,k),rateMeanFull);
    ratecors(:,2)=corr(temp1(:,(size(temp1,2)-1)/2+1),rateMeanFull);
    for curRat=1:size(rateMeanFull,2)
        ratepartcors(curRat,1)=partialcorr(peakheights(:,k),rateMeanFull(:,curRat),double(outlierMat(:,curRat)));
        ratepartcors(curRat,2)=partialcorr(temp1(:,(size(temp1,2)-1)/2+1),rateMeanFull(:,curRat),double(outlierMat(:,curRat)));
    end
    
%     ratecors(:,3)=corr(abs(mainpeakloc(:,1)),rateMean');
    hold on
    hba=barh(ratecors((end:-1:1),:));
    set(hba(1),'facecolor',[1 1 1])
    set(hba(2),'facecolor',colOrd(k,:))
    hba2=barh(ratepartcors((end:-1:1),:));%hba2=barh([(1:8)'-.2 (1:8)'+.2],ratepartcors((end:-1:1),:));
    set(hba2(1),'facecolor',[.8 .8 .8],'facealpha',.5)
    set(hba2(2),'facecolor',(colOrd(k,:)+1)/2,'facealpha',.5)
%     set(hba(3),'facecolor',[0 0 0])
    hold on;
    plot(probabilityToCorrelation(18,.05)*[1 1],[.5 length(rateTitles2)+.5],'k--')
    plot(probabilityToCorrelation(18,.05)*-[1 1],[.5 length(rateTitles2)+.5],'k--')
    title(condNames{k});
    axis([-.8 .8 .5 length(rateTitles2)+.5])
    colormap gray;
    if k==1
        set(gca,'ytick',1:length(rateTitles2),'yticklabel',rateTitles2(end:-1:1));
        xlabel('Correlation (r)');
    else
        set(gca,'ytick',1:length(rateTitles2),'yticklabel',[]);
    end
    if k==4
        hleg=legend('Peak height','Zero-lag','Partial correlation');%,'Peak lag');
        set(hleg,'position',[.1/2.5+(ii-1)/6 .35 .075 .46-.35])
    end
end

%% Gaze classification part: Prepare variables
load([baseDir '/PreprocessedData/gazedata_with_correct_time_windows_repro.mat'])
load([baseDir '/PreprocessedData/gazedata_with_correct_time_windows_reliability_third.mat'],'gazedata','gazedatarel','gazedataorig');
addpath([baseDir '/Code'])
addpath([baseDir '/Code/SupportingFunctions'])
gzreli=[];
fullLabels1=[];
fullLabels2=[];
fullLabels3=[];
fullLabelsCons=[];

for k=1:18
    gazedatacons{k}=mode(cat(3,gazedataorig{k},gazedatarel{k},gazedata{k}),3);
end
gazedata=gazedatacons;
% addpath('/home/juha/matlab-codes/')
% addpath('/home/juha/matlab-codes/aalto/')
% addpath('/home/juha/matlab-codes/nanconv/')

%Calculate pause lengths for reference
tPause=[];
tBegin=[];
for sub=1:18
    temp=tWinAll{sub};
    for k=1:9
        tempord(k)=find(temp(:,k),1);
    end
    [~,tempidx]=sort(tempord,'ascend');
    temp=temp(:,tempidx);
    tBegin(sub)=find(temp(:,1),1,'first')/10;
    for k=2:9 
        pstart=find(temp(:,k-1),1,'last');
        pend=find(temp(:,k),1,'first');
        tPause(sub,k-1)=(pend-pstart+1)/10;
    end
end
% Interpolate subject data
clear subData
classLabels={'Eye contact','Joint attention','Other-oriented','Other'};
for subjectNo=1:18
    trialWins=logical(max(tWinAll{subjectNo},[],2));
    tWinAll{subjectNo}=logical(tWinAll{subjectNo});
    %Interpolate the data again... please remember to save the
    %interpolated data at some point so we can eventually skip these
    %lines in the future
    XYZ2=interp1(dataAll{subjectNo,3}(1,:),dataAll{subjectNo,3}([2:4 80:82],:)',twinFull{subjectNo})';
    XYZ1=interp1(dataAll{subjectNo,1}(1,:),dataAll{subjectNo,1}([2:4 80:82],:)',twinFull{subjectNo})';
    XYZn=interp1(dataAll{subjectNo,2}(1,:),dataAll{subjectNo,2}([2:4 80:82],:)',twinFull{subjectNo})';
    
    %Little Gaussian filter with 0.3-second sigma
    for kj=1:6
        XYZ1(kj,:)=conv(XYZ1(kj,:),gaussKernel(27,3),'same');
        XYZ2(kj,:)=conv(XYZ2(kj,:),gaussKernel(27,3),'same');
        XYZn(kj,:)=conv(XYZn(kj,:),gaussKernel(27,3),'same');
    end
    ptch1=atand((XYZ1(5,:)-XYZ1(2,:))./sqrt((XYZ1(6,:)-XYZ1(3,:)).^2+(XYZ1(4,:)-XYZ1(1,:)).^2));
    ptch2=atand((XYZ2(5,:)-XYZ2(2,:))./sqrt((XYZ2(6,:)-XYZ2(3,:)).^2+(XYZ2(4,:)-XYZ2(1,:)).^2));
    
    pitchSimple{subjectNo}=[ptch1(:) ptch2(:)];
    subData{subjectNo}=[relYaw{subjectNo}(trialWins,1) relYaw{subjectNo}(trialWins,2) pitchSimple{subjectNo}(trialWins,:) XYZ2(:,trialWins)' XYZ1(:,trialWins)'];
    subData{subjectNo}(:,17)=mean(subData{subjectNo}(:,3:4),2);
    subData{subjectNo}(:,18:19)=sqrt(subData{subjectNo}(:,1:2).^2+subData{subjectNo}(:,3:4).^2);
    %Trial data
    for trial=1:9
        XYZ2=interp1(dataAll{subjectNo,3}(1,:),dataAll{subjectNo,3}([2:4 80:82],:)',twinFull{subjectNo}(tWinAll{subjectNo}(:,trial)))';
        XYZ1=interp1(dataAll{subjectNo,1}(1,:),dataAll{subjectNo,1}([2:4 80:82],:)',twinFull{subjectNo}(tWinAll{subjectNo}(:,trial)))';
        XYZn=interp1(dataAll{subjectNo,2}(1,:),dataAll{subjectNo,2}([2:4 80:82],:)',twinFull{subjectNo}(tWinAll{subjectNo}(:,trial)))';
        
        %Little Gaussian filter with 0.3-second sigma
        for kj=1:6
            XYZ1(kj,:)=conv(XYZ1(kj,:),gaussKernel(27,3),'same');
            XYZ2(kj,:)=conv(XYZ2(kj,:),gaussKernel(27,3),'same');
            XYZn(kj,:)=conv(XYZn(kj,:),gaussKernel(27,3),'same');
        end
        ptch1=atand((XYZ1(5,:)-XYZ1(2,:))./sqrt((XYZ1(6,:)-XYZ1(3,:)).^2+(XYZ1(4,:)-XYZ1(1,:)).^2));
        ptch2=atand((XYZ2(5,:)-XYZ2(2,:))./sqrt((XYZ2(6,:)-XYZ2(3,:)).^2+(XYZ2(4,:)-XYZ2(1,:)).^2));
        
        pitchSimpleTrial{subjectNo,trial}=[ptch1(:) ptch2(:)];
        
        subDataTrial{subjectNo,trial}=[relYaw{subjectNo}(tWinAll{subjectNo}(:,trial),1) relYaw{subjectNo}(tWinAll{subjectNo}(:,trial),2) pitchSimple{subjectNo}(tWinAll{subjectNo}(:,trial),:) XYZ2' XYZ1'];
        subDataTrial{subjectNo,trial}(:,17)=mean(subDataTrial{subjectNo,trial}(:,3:4),2);
        subDataTrial{subjectNo,trial}(:,18:19)=sqrt(subDataTrial{subjectNo,trial}(:,1:2).^2+subDataTrial{subjectNo,trial}(:,3:4).^2);
    end
    
    
end

% Make probability field for Naive Bayes (used to be kNN) classifier for visualization
gzAngOog=[];
colOrd=[2 3 1 5];
foi={1:2 1:4 [1:7 11:13] 1:16};%18:19 1:16};
% foi={1:2 1:4 1:10 1:16};
figure;
set(gcf,'position',[0 200 1920 1920*2/3.9])
featTitles={'Yaw' 'Yaw+Pitch','Yaw+Pitch+Location','Yaw+Pitch+Location+Gaze location'};
clear nAllGaze
confMatLOSO=nan(4,4,18);
confMatLOSOnull=confMatLOSO;
allConfMat=[];
allConfMatNull=[];
accLOSO=nan(18,1);
nIter=50;
for cfoi=1:length(foi)
    for k=1:4
        gzAng{k}=zeros(0,16);
    end
    numConds=zeros(1,4);
    for k=1:4
        for subjectNo=1:18
            if k<4
                ec{subjectNo,k}=and(and(gazedata{subjectNo}(3,:),gazedata{subjectNo}(4,:)),gazedata{subjectNo}(22,:)==k);
                sa{subjectNo,k}=and(and(gazedata{subjectNo}(5,:),gazedata{subjectNo}(6,:)),gazedata{subjectNo}(22,:)==k);
                oog{subjectNo,k}=or(and(and(gazedata{subjectNo}(3,:),gazedata{subjectNo}(6,:)),gazedata{subjectNo}(22,:)==k),...
                    and(and(gazedata{subjectNo}(4,:),gazedata{subjectNo}(5,:)),gazedata{subjectNo}(22,:)==k));
                other{subjectNo,k}=and(~or(ec{subjectNo,k},or(sa{subjectNo,k},oog{subjectNo,k})),gazedata{subjectNo}(22,:)==k);
            else
                ec{subjectNo,k}=and(gazedata{subjectNo}(3,:),gazedata{subjectNo}(4,:));
                sa{subjectNo,k}=and(gazedata{subjectNo}(5,:),gazedata{subjectNo}(6,:));
                oog{subjectNo,k}=or(and(gazedata{subjectNo}(3,:),gazedata{subjectNo}(6,:)),...
                    and(gazedata{subjectNo}(4,:),gazedata{subjectNo}(5,:)));
                other{subjectNo,k}=~or(ec{subjectNo,k},or(sa{subjectNo,k},oog{subjectNo,k}));
                numConds=numConds+[sum(ec{subjectNo,k}) sum(sa{subjectNo,k}) sum(oog{subjectNo,k}) sum(other{subjectNo,k})];
                %Make a list of all gaze angles in each condition
                nEC(subjectNo)=sum(ec{subjectNo,k})/length(ec{subjectNo,k});
                nSA(subjectNo)=sum(sa{subjectNo,k})/length(ec{subjectNo,k});
                nOOG(subjectNo)=sum(oog{subjectNo,k})/length(ec{subjectNo,k});
                nOther(subjectNo)=sum(other{subjectNo,k})/length(ec{subjectNo,k});
                nAllGaze(subjectNo,:)=[nEC(subjectNo) nSA(subjectNo) nOOG(subjectNo) nOther(subjectNo)];
                % FOR EXTENDING WITH PITCH DATA add the mean pitch for example
                % (3D) or both pitches (4D)
                gzAng{1}=[gzAng{1};[gazedata{subjectNo}(1,ec{subjectNo,k})' gazedata{subjectNo}(2,ec{subjectNo,k})' gazedata{subjectNo}(25,ec{subjectNo,k})' gazedata{subjectNo}(26,ec{subjectNo,k})' gazedataFull{subjectNo,1}([2:4 80:82],ec{subjectNo,k})' gazedataFull{subjectNo,2}([2:4 80:82],ec{subjectNo,k})']];
                gzAng{2}=[gzAng{2};[gazedata{subjectNo}(1,sa{subjectNo,k})' gazedata{subjectNo}(2,sa{subjectNo,k})' gazedata{subjectNo}(25,sa{subjectNo,k})' gazedata{subjectNo}(26,sa{subjectNo,k})' gazedataFull{subjectNo,1}([2:4 80:82],sa{subjectNo,k})' gazedataFull{subjectNo,2}([2:4 80:82],sa{subjectNo,k})']];
                gzAng{3}=[gzAng{3};[gazedata{subjectNo}(1,oog{subjectNo,k})' gazedata{subjectNo}(2,oog{subjectNo,k})' gazedata{subjectNo}(25,oog{subjectNo,k})' gazedata{subjectNo}(26,oog{subjectNo,k})' gazedataFull{subjectNo,1}([2:4 80:82],oog{subjectNo,k})' gazedataFull{subjectNo,2}([2:4 80:82],oog{subjectNo,k})']];
                gzAngOog=[gzAngOog;gazedata{subjectNo}(2,and(gazedata{subjectNo}(3,:)==1,gazedata{subjectNo}(6,:)==1))';gazedata{subjectNo}(1,and(gazedata{subjectNo}(4,:)==1,gazedata{subjectNo}(5,:)==1))';gazedata{subjectNo}(26,and(gazedata{subjectNo}(3,:)==1,gazedata{subjectNo}(6,:)==1))';gazedata{subjectNo}(25,and(gazedata{subjectNo}(4,:)==1,gazedata{subjectNo}(5,:)==1))']; %#ok<AGROW>
                gzAng{4}=[gzAng{4};[gazedata{subjectNo}(1,other{subjectNo,k})' gazedata{subjectNo}(2,other{subjectNo,k})' gazedata{subjectNo}(25,other{subjectNo,k})' gazedata{subjectNo}(26,other{subjectNo,k})' gazedataFull{subjectNo,1}([2:4 80:82],other{subjectNo,k})' gazedataFull{subjectNo,2}([2:4 80:82],other{subjectNo,k})']];
            end
   
        end
    end
    %Add mean pitch data
    for ll=1:4
        gzAng{ll}(:,17)=mean(gzAng{ll}(:,3:4),2);
        gzAng{ll}(:,18:19)=sqrt(gzAng{ll}(:,1:2).^2+gzAng{ll}(:,3:4).^2);
    end
    %Add the group labels to the end of the data for easier shuffling...
    allGazeAng=[gzAng{1} ones(length(gzAng{1}),1);...
        gzAng{2} ones(length(gzAng{2}),1)*2;...
        gzAng{3} ones(length(gzAng{3}),1)*3;...
        gzAng{4} ones(length(gzAng{4}),1)*4];
    
    
    % allGazeAng(allGazeAng==0)=nan;
    
    %     gridInd=-10:1:90;
    %     gridIndZ=-90:1:10;
    % % [XX,YY,ZZ]=ndgrid(gridInd,gridInd,-gridInd);
    %     [XX,YY,ZZ,TT]=ndgrid(gridInd,gridInd,gridIndZ,gridIndZ);
    % probPlane=repmat(XX,[1 1 4])*0;
    % probScale=probPlane;
    % numConds=[1 1 1 1];
    % kNNk=100;
    %mdl = fitcnb(X,Y);SVMmodelECvsJA=fitcsvm(gazeVec(idx,:),double(ec(idx))+1);labelsPredECvsJA=SVMmodelECvsJA.predict(gazeVec(idx,:));
    
    % for x=1:length(XX)
    %     for y=1:length(YY)
    %
    %         [~,idx]=sort(sqrt((allGazeAng(:,1)-YY(y,x)).^2+(allGazeAng(:,2)-XX(y,x)).^2),'ascend');
    %         probPlane(y,x,1)=sum(allGazeAng(idx(1:kNNk),3)==1)/kNNk;
    %         probPlane(y,x,3)=sum(allGazeAng(idx(1:kNNk),3)==2)/kNNk;
    %         probPlane(y,x,2)=sum(allGazeAng(idx(1:kNNk),3)==3)/kNNk;
    %         probPlane(y,x,4)=sum(allGazeAng(idx(1:kNNk),3)==4)/kNNk;
    %         probIm(y,x,:)=aaltoColors(2)*probPlane(y,x,1)+aaltoColors(3)*probPlane(y,x,3)+aaltoColors(1)*probPlane(y,x,2);
    %
    %         probScale(y,x,:)=squeeze(probPlane(y,x,:)).*(numConds(:)./sum(numConds));
    %         probScale(y,x,:)=squeeze(probScale(y,x,:)/sum(probScale(y,x,:),3));
    %         probImScale(y,x,:)=aaltoColors(2)*probScale(y,x,1)+aaltoColors(3)*probScale(y,x,3)+aaltoColors(1)*probScale(y,x,2);
    %         [~,temp]=max(probPlane(y,x,:));
    %         maxProb(y,x)=temp(1);
    %     end
    % end
    
    allGazeAng=allGazeAng(:,[foi{cfoi} end]);
    %Run a naive Bayes classifier multiple times with balanced group sizes to
    %get semi-stable estimates of the posterior probabilities
    
    %     idxDrop=idx(round(min(numConds)*.5)+1:end);
    nTrain=floor(min(numConds)*.5);
    fprintf('Classifying');
    confMat=zeros(4,4,nIter);
    confMatNull=zeros(4,4,nIter);
    
    for iter=1:nIter
        fprintf('.');
        allGazePrune=[];
        allGazeTest=[];
        for k=1:4
            idx=find(allGazeAng(:,end)==k);%idx=find(allGazeAng(:,5)==k); FOR EXTENDING WITH PITCH DATA
            idx=idx(randperm(length(idx)));
            idxTest=idx((nTrain+1):(2*nTrain));
            idx=idx(1:nTrain);
            allGazePrune=[allGazePrune;allGazeAng(idx,:)]; %#ok<AGROW>
            allGazeTest=[allGazeTest;allGazeAng(idxTest,:)]; %#ok<AGROW>
        end
        %     allGazePrune(:,3)=0+rand(length(allGazePrune),1)*.0001;
        %     allGazeTest(:,3)=0+rand(length(allGazePrune),1)*.0001;
        mdl = fitcnb(allGazePrune(:,1:end-1),allGazePrune(:,end));%mdl = fitcnb(allGazePrune(:,1:4),allGazePrune(:,5)); FOR EXTENDING WITH PITCH DATA
        %     [predictedspecies,Posterior(:,:,:,iter),~] = predict(mdl,[YY(:) XX(:) ZZ(:) TT(:)]);
        predictedspecies2 = predict(mdl,allGazeTest(:,1:end-1));
        
        for subjectNo=1:18
            predictedspecies3 = predict(mdl,subData{subjectNo}(:,foi{cfoi}));
            dataLabels{subjectNo}=predictedspecies3;
            for ll=1:4
                ecPerc(subjectNo,iter,cfoi,ll)=sum(predictedspecies3==ll)/length(predictedspecies3);
            end
            for trial=1:9
                predictedspecies4 = predict(mdl,subDataTrial{subjectNo,trial}(:,foi{cfoi}));
                for ll=1:4
                    ecPercTrial(subjectNo,iter,cfoi,ll,trial)=sum(predictedspecies4==ll)/length(predictedspecies4);
                end
            end
        end
        toi={1:2 4:6 7:9 3};
        for trtype=1:length(toi)%4
            ecPercType(:,:,trtype,cfoi)=squeeze(mean(mean(ecPercTrial(:,:,end,:,toi{trtype}),2),5));
        end
        
        percCorr(iter)=nansum(allGazeTest(:,end)==predictedspecies2)/length(predictedspecies2);
        percCorrNull(iter)=nansum(allGazeTest(randperm(size(allGazeTest,1)),end)==predictedspecies2)/length(predictedspecies2);
        for xx=1:4
            for yy=1:4
                confMat(xx,yy,iter)=sum(predictedspecies2(allGazeTest(:,end)==xx)==yy)/sum(allGazeTest(:,end)==xx);
                confMatNull(xx,yy,iter)=sum(predictedspecies2(allGazeTest(randperm(size(allGazeTest,1)),end)==xx)==yy)/sum(allGazeTest(:,end)==xx);
            end
        end
        
    end
    %% Leave-one-subject-out cross-validation to check generalizability
    
    for subjectNoTest=1:18
        trainSubs=1:18;
        trainSubs(subjectNoTest)=[];
        
        gzAngTrain=cell(4,1);
        for subjectNo=1:length(trainSubs)
            gzAngTrain{1}=[gzAngTrain{1};[gazedata{subjectNo}(1,ec{subjectNo,k})' gazedata{subjectNo}(2,ec{subjectNo,k})' gazedata{subjectNo}(25,ec{subjectNo,k})' gazedata{subjectNo}(26,ec{subjectNo,k})' gazedataFull{subjectNo,1}([2:4 80:82],ec{subjectNo,k})' gazedataFull{subjectNo,2}([2:4 80:82],ec{subjectNo,k})']];
            gzAngTrain{2}=[gzAngTrain{2};[gazedata{subjectNo}(1,sa{subjectNo,k})' gazedata{subjectNo}(2,sa{subjectNo,k})' gazedata{subjectNo}(25,sa{subjectNo,k})' gazedata{subjectNo}(26,sa{subjectNo,k})' gazedataFull{subjectNo,1}([2:4 80:82],sa{subjectNo,k})' gazedataFull{subjectNo,2}([2:4 80:82],sa{subjectNo,k})']];
            gzAngTrain{3}=[gzAngTrain{3};[gazedata{subjectNo}(1,oog{subjectNo,k})' gazedata{subjectNo}(2,oog{subjectNo,k})' gazedata{subjectNo}(25,oog{subjectNo,k})' gazedata{subjectNo}(26,oog{subjectNo,k})' gazedataFull{subjectNo,1}([2:4 80:82],oog{subjectNo,k})' gazedataFull{subjectNo,2}([2:4 80:82],oog{subjectNo,k})']];
            gzAngTrain{4}=[gzAngTrain{4};[gazedata{subjectNo}(1,other{subjectNo,k})' gazedata{subjectNo}(2,other{subjectNo,k})' gazedata{subjectNo}(25,other{subjectNo,k})' gazedata{subjectNo}(26,other{subjectNo,k})' gazedataFull{subjectNo,1}([2:4 80:82],other{subjectNo,k})' gazedataFull{subjectNo,2}([2:4 80:82],other{subjectNo,k})']];
        end
        
        %Add mean pitch data
        for ll=1:4
            gzAngTrain{ll}(:,17)=mean(gzAngTrain{ll}(:,3:4),2);
            gzAngTrain{ll}(:,18:19)=sqrt(gzAngTrain{ll}(:,1:2).^2+gzAngTrain{ll}(:,3:4).^2);
        end
        %Add the group labels to the end of the data for easier shuffling...
        trainData=[gzAngTrain{1} ones(length(gzAngTrain{1}),1);...
                  gzAngTrain{2} ones(length(gzAngTrain{2}),1)*2;...
                  gzAngTrain{3} ones(length(gzAngTrain{3}),1)*3;...
                  gzAngTrain{4} ones(length(gzAngTrain{4}),1)*4];
        
        trainData=trainData(:,[foi{cfoi} end]);
        trainData(isnan(trainData(:,2)),2)=nanmean(trainData(:,2));
        subjectNo=subjectNoTest;
        gzAngTest{1}=[gazedata{subjectNo}(1,ec{subjectNo,k})' gazedata{subjectNo}(2,ec{subjectNo,k})' gazedata{subjectNo}(25,ec{subjectNo,k})' gazedata{subjectNo}(26,ec{subjectNo,k})' gazedataFull{subjectNo,1}([2:4 80:82],ec{subjectNo,k})' gazedataFull{subjectNo,2}([2:4 80:82],ec{subjectNo,k})'];
        gzAngTest{2}=[gazedata{subjectNo}(1,sa{subjectNo,k})' gazedata{subjectNo}(2,sa{subjectNo,k})' gazedata{subjectNo}(25,sa{subjectNo,k})' gazedata{subjectNo}(26,sa{subjectNo,k})' gazedataFull{subjectNo,1}([2:4 80:82],sa{subjectNo,k})' gazedataFull{subjectNo,2}([2:4 80:82],sa{subjectNo,k})'];
        gzAngTest{3}=[gazedata{subjectNo}(1,oog{subjectNo,k})' gazedata{subjectNo}(2,oog{subjectNo,k})' gazedata{subjectNo}(25,oog{subjectNo,k})' gazedata{subjectNo}(26,oog{subjectNo,k})' gazedataFull{subjectNo,1}([2:4 80:82],oog{subjectNo,k})' gazedataFull{subjectNo,2}([2:4 80:82],oog{subjectNo,k})'];
        gzAngTest{4}=[gazedata{subjectNo}(1,other{subjectNo,k})' gazedata{subjectNo}(2,other{subjectNo,k})' gazedata{subjectNo}(25,other{subjectNo,k})' gazedata{subjectNo}(26,other{subjectNo,k})' gazedataFull{subjectNo,1}([2:4 80:82],other{subjectNo,k})' gazedataFull{subjectNo,2}([2:4 80:82],other{subjectNo,k})'];
        
        
        %Add mean pitch data
        for ll=1:4
            gzAngTest{ll}(:,17)=mean(gzAngTest{ll}(:,3:4),2);
            gzAngTest{ll}(:,18:19)=sqrt(gzAngTest{ll}(:,1:2).^2+gzAngTest{ll}(:,3:4).^2);
        end
        %Add the group labels to the end of the data for easier shuffling...
        testData=[gzAngTest{1} ones(size(gzAngTest{1},1),1);...
                  gzAngTest{2} ones(size(gzAngTest{2},1),1)*2;...
                  gzAngTest{3} ones(size(gzAngTest{3},1),1)*3;...
                  gzAngTest{4} ones(size(gzAngTest{4},1),1)*4];
        
        testData=testData(:,[foi{cfoi} end]);
        testData(isnan(testData(:,2)),2)=nanmean(testData(:,2));
        mdl = fitcnb(trainData(:,1:end-1),trainData(:,end));
        predictedspeciesLOSO = predict(mdl,testData(:,1:end-1));
        accLOSO(subjectNoTest)=nanmean(predictedspeciesLOSO==testData(:,end));
        accLOSOnull(subjectNoTest)=nanmean(predictedspeciesLOSO==testData(randperm(size(testData,1)),end));
        %Some participants don't have any events of particular labels, so
        %we have to fill in the relevant rows/columns of the confusion
        %matrix
        uniLabels=unique([testData(:,end) predictedspeciesLOSO]);
        confMatLOSO(uniLabels,uniLabels,subjectNoTest,cfoi)=confusionmat(testData(:,end),predictedspeciesLOSO);
        confMatLOSO(:,:,subjectNoTest,cfoi)=confMatLOSO(:,:,subjectNoTest,cfoi)./repmat(sum(confMatLOSO(:,:,subjectNoTest,cfoi),2),[1 size(confMatLOSO,2)]);
        for iter=1:nIter
            confMatLOSOnull(uniLabels,uniLabels,subjectNoTest,cfoi,iter)=confusionmat(testData(randperm(size(testData,1)),end),predictedspeciesLOSO);
            confMatLOSOnull(:,:,subjectNoTest,cfoi,iter)=confMatLOSOnull(:,:,subjectNoTest,cfoi,iter)./repmat(sum(confMatLOSOnull(:,:,subjectNoTest,cfoi,iter),2),[1 size(confMatLOSO,2)]);
        end
%         dataLabels{subjectNo}=predictedspecies3;
%         for ll=1:4
%             ecPerc(subjectNo,iter,cfoi,ll)=sum(predictedspecies3==ll)/length(predictedspecies3);
%         end
%         for trial=1:9
%             predictedspecies4 = predict(mdl,subDataTrial{subjectNo,trial}(:,foi{cfoi}));
%             for ll=1:4
%                 ecPercTrial(subjectNo,iter,cfoi,ll,trial)=sum(predictedspecies4==ll)/length(predictedspecies4);
%             end
%         end
    end
    %%
    allConfMat(:,:,:,cfoi)=confMat;
    allConfMatNull(:,:,:,cfoi)=confMatNull;
    confMatMean(:,cfoi)=reshape(nanmean(confMat,3),[],1);
    confMatMeanLOSO(:,cfoi)=reshape(nanmean(confMatLOSO(:,:,:,cfoi),3),[],1);
    fprintf('done\n');
    
    
    subplot(2,length(foi),cfoi);
    imagesc(nanmean(confMat,3));
    for x=1:4
        for y=1:4
            text(x,y,sprintf('%.1f',mean(confMat(y,x,:),3)*100),'HorizontalAlignment','center','color',[0 0 0],'fontweight','bold');
        end
    end
    axis image;
    set(gca,'xtick',1:4,'xticklabel',classLabels,'xticklabelrotation',45,'ytick',1:4,'yticklabel',classLabels)
    title(featTitles{cfoi},'FontWeight','normal');
    if cfoi==1
        ylabel('True class')
        xlabel('Predicted class')
    end
    
    colors=aaltoColors([2 3 1 5]);
    for ll=1:4
        subplot(4,length(foi)*2,length(foi)*4+ll+(ll>2)*length(foi)*2-(ll>2)*2+(cfoi-1)*2);
        nECraw=squeeze(mean(ecPerc(:,:,cfoi,ll),2));
        y=nECraw(:);
        x=nAllGaze(:,ll);
        X=linspace(min(x),max(x),100);
        
        %sort the data so the plotting works nicely
        [~,xidx]=sort(x,'ascend');
        x=x(xidx);
        y=y(xidx);
        
        [p,s]=polyfit(x,y,1);
        [yfit,dy]=polyconf(p,X,s,'predopt','curve');
        
        plot(x,y,'o','markerfacecolor',colors(ll,:),'markeredgecolor',[0 0 0]);
        hold on;
        plot(X,yfit,'linewidth',2,'color',colors(ll,:));
        plot(X,yfit-dy,'color',(colors(ll,:)+1)/2);
        plot(X,yfit+dy,'color',(colors(ll,:)+1)/2);
        %         text(.17,.2,sprintf('r=%.2f',corr(nEC(:),nECraw(:))));
        %         text(max(x)-.1,max(x)-.05,sprintf('r=%.2f',corr(x,y)),'color',colors(ll,:)/2);
        if cfoi==1
            text(.5,1.05,sprintf('%s, r=%.2f',classLabels{ll},corr(x,y)),'units','normalized','horizontalalignment','center');%'color',colors(ll,:)/2,
        else
            text(.5,1.05,sprintf('r=%.2f',corr(x,y)),'units','normalized','horizontalalignment','center');%'color',colors(ll,:)/2,
        end
        plot(x,x,'-.','color',(colors(ll,:)+1)/2)
        if ll==3 && cfoi==1
            %             subplot(2,length(foi),length(foi)+1);
            %             axis([0 1 0 1])
            %             set(gca,'visible','off')
            xlabel('% eye contact, manually annotated');
            ylabel('% eye contact, full data');
        end
        %         minAx=min([min(x) min(yfit+dy)]);
        %         maxAx=max([max(x) max(yfit+dy)]);%max(reshape(mean(ecPerc(:,:,cfoi,:),2),[],1)),max(nAllGaze(:)));
        %         axis([minAx maxAx minAx maxAx]);%max(x) 0 max(yfit+dy)]);
        axis tight
    end
    
    drawnow
end
%% Compare the real accuracies to null distribution
figure;
subplot(1,2,1);
hold on
allConfVec=reshape(allConfMat,[],size(allConfMat,3),size(allConfMat,4));
[hbar,hbin]=hist(squeeze(nanmean(allConfVec(logical(eye(size(allConfMat,1))),:,:))));
plot(hbin,hbar/50,'linewidth',2)
allConfVecNull=reshape(allConfMatNull,[],size(allConfMatNull,3),size(allConfMatNull,4));
[hbar,hbin]=hist(squeeze(nanmean(allConfVecNull(logical(eye(size(allConfMat,1))),:,4))));
plot(hbin,hbar/50,'color',aaltoColors(5),'linewidth',2);
legend('yaw','yaw+pitch','yaw+pitch+location','yaw+pitch+location+gaze','null');
fprintf('Split-half: Null max = %.1f%%\n',max(mean(allConfVecNull(logical(eye(4)),:)))*100);
title('Split half');

subplot(1,2,2);
hold on
allConfVec=reshape(nanmean(confMatLOSO(:,:,:,4),3),[],1);
plot(nanmean(allConfVec(logical(eye(size(confMatLOSO,1))),:))*[1 1],[0 4],'linewidth',2)
allConfVecNull=reshape(nanmean(confMatLOSOnull(:,:,:,4,:),3),[],size(confMatLOSOnull,5));
[hbar,hbin]=hist(squeeze(nanmean(allConfVecNull(logical(eye(size(confMatLOSOnull,1))),:))));
plot(hbin,hbar/50,'color',aaltoColors(5),'linewidth',2);
legend('yaw+pitch+location+gaze','null');
title('Leave one dyad out');
fprintf('Leave-one-dyad-out: Null max = %.1f%%\n',max(mean(allConfVecNull(logical(eye(4)),:)))*100);
% %%
% probPlane=reshape(mean(Posterior(:,[1 3 2 4],:),3),size(repmat(XX,[1 1 4])));
% sz=size(probPlane);
% probIm=repmat(permute(aaltoColors(2),[1 3 2]),[sz(1:2) 1]).*probPlane(:,:,1)+repmat(permute(aaltoColors(3),[1 3 2]),[sz(1:2) 1]).*probPlane(:,:,3)+repmat(permute(aaltoColors(1),[1 3 2]),[sz(1:2) 1]).*probPlane(:,:,2);
% probScale=probPlane;
% probImScale=probIm;
% %%
% figure;
% for cfoi=1:4
%     gazeBehav(:,:,cfoi)=corr(squeeze(mean(ecPerc(:,:,cfoi,:),2)),rate1'+rate2');
%     
% end
% imagesc(gazeBehav(:,:,cfoi));
% hold on;
% % contour(correlationToProbability(18,abs(gazeBehav(:,:,cfoi)))<.05,1,'linewidth',2,'color',[0 0 0]);
% %%
% figure;
% for trial=1:9
%     subplot(3,3,trial);
%     title(trialNames{trial});
%     for cfoi=1:4
%         gazeBehavTrials(:,:,cfoi,trial)=corr(squeeze(mean(ecPercTrial(:,:,cfoi,:,trial),2)),rate1'+rate2');
%     end
%     imagesc(gazeBehavTrials(:,:,cfoi,trial));
%     hold on;
%     %     if any(reshape(correlationToProbability(18,abs(gazeBehavTrials(:,:,cfoi,trial)))<.05,[],1))
%     contour(correlationToProbability(18,abs(gazeBehavTrials(:,:,cfoi,trial)))<.05,1,'linewidth',2,'color',[0 0 0]);
%     %     end
%     set(gca,'clim',[-1 1])
%     
% end
% %%
% figure;
% for subjectNo=1:18
%     subplot(6,3,subjectNo)
%     hold on;
%     for ll=1:4
%         plot3(subData{subjectNo}(dataLabels{subjectNo}==ll,1),subData{subjectNo}(dataLabels{subjectNo}==ll,2),mean(subData{subjectNo}(dataLabels{subjectNo}==ll,3:4),2),'o','markerfacecolor',colors(ll,:),'markeredgecolor','none');
%     end
%     drawnow
% end
%% Differences in gaze behavior durations
figure;
set(gcf,'position',[200 400 1200 300]*1.2);

%Overall effects of gaze behavior, experimental condition and dyad on
%the overall time spent doing each behavior (ANOVA)
gazeLabels=ones(size(ecPercType(:,:,:,1)));
conditionLabels=ones(size(ecPercType(:,:,:,1)));
subjectLabels=ones(size(ecPercType(:,:,:,1)));
for k=1:4
    gazeLabels(:,k,:,:)=k;
    conditionLabels(:,:,k,:)=k;
end
for subL=1:18
    subjectLabels(subL,:,:)=subL;
end
%Run the ANOVA and display statistics table (4 in the last dimension refers
%to the results using all features in the classifier)
[p,tbl,stats] = anovan(reshape(ecPercType(:,:,:,4),[],1),{gazeLabels(:) conditionLabels(:) subjectLabels(:)},'varnames',{'Gaze category' 'Condition' 'Dyad'},'model','interaction','random',[3],'display','off');
fprintf('ANOVA results for gaze behaviors between conditions:\n')
tbl
for k=1:4
    subplot(1,4,k);
    if k==1
        ylabel('Time (%)')
    end
    set(gca,'xticklabel',{'Conversation','Cooperative','Competitive','Practice'},'xticklabelrotation',45,'ytick',0:.2:1,'yticklabel',(0:.2:1)*100);
    title(classLabels{k});
    %Here the 4 refers to the best-performing combination of features
    [~,p,~,stats]=ttest(repmat(squeeze(ecPercType(:,k,:,4)),[1 1 4]),permute(repmat(squeeze(ecPercType(:,k,:,4)),[1 1 4]),[1 3 2]));
    
%     imagesc(squeeze(stats.tstat).*(squeeze(p)<(.05/(4*6))));
    BML_violinPlot(squeeze(ecPercType(:,k,:,4)),10,[1 1 1],1,1,1);
    p=squeeze(p);
    p(triu(true(size(p)),1))=nan;
    [x,y]=find(p<(.05/(4*6)));
    [x2,y2]=find(p<(.001/(4*6)));
    [~,idx]=sort(x-y);
    x=x(idx);
    y=y(idx);

    for l=1:length(x)
        plot([x(l) x(l) y(l) y(l)],[0 .01 .01 0]+1+l*.04,'k');
        text((x(l)+y(l))/2,1+l*.04+.015,'*','horizontalalignment','center');
        if p(x(l),y(l))<(.001/(4*6))
            text((x(l)+y(l))/2,1+l*.04+.015,'* *','horizontalalignment','center');
        end
    end
    
    axis([0 5 -.1 1.25])
    
    hold on;
%     set(gca,'clim',[-6 6]);
%     axis image
    fprintf('%s\n',classLabels{k});
    squeeze(p)
end

%% Calculate body orientations
cd([baseDir '/matDataKinectAndIMU'])
clear angRel
d=dir('F*');
d2=dir('M*');
d=[d; d2];
d(~[d(:).isdir])=[];
jointIdx3=4+([8 20 4]*3)+1;
jointIdx=4+([8 4]*3)+1;

for ordNum=1:18
    subjectNo=ord(ordNum);
    fprintf('---------------------------------------------------------\n');
    fprintf('Dyad %i, %s\n',ordNum,d(subjectNo).name);
    fprintf('---------------------------------------------------------\n');
    %List saved video frame timestamps for Slave Kinect
%     if ~exist([baseDir '/matDataKinectAndIMU/'],'Dir')
%     zitternix_convert_kinect_IMU_to_Matlab;
% end
% %Edit sensor-log-reading function so that it saves all columns
% cd([baseDir '/OriginalData']);
    dtemp=dir(sprintf('%s/OriginalData/%s/Slave/%s*',baseDir,d(subjectNo).name,d(subjectNo).name));
    dtemp(~[dtemp(:).isdir])=[];
   
%     [hdr{subjectNo},t0slav{subjectNo},t02slav{subjectNo}]=readADOSheader(sprintf('%s/OriginalData/%s/Slave/%s.txt',baseDir,d(subjectNo).name,dtemp.name));
    tSample=twinFull{subjectNo};
    
    XYZ2=interp1(dataAll{subjectNo,3}(1,:),dataAll{subjectNo,3}',twinFull{subjectNo})';
    XYZ1=interp1(dataAll{subjectNo,1}(1,:),dataAll{subjectNo,1}',twinFull{subjectNo})';
    XYZn=interp1(dataAll{subjectNo,2}(1,:),dataAll{subjectNo,2}',twinFull{subjectNo})';
    
    dataCur{1}=XYZ1;
    dataCur{2}=XYZ2;
   
%     midpoints=[mean(dataCur{1}(jointIdx,t)) mean(dataCur{2}(jointIdx,t));...
%         mean(dataCur{1}(jointIdx+1,t)) mean(dataCur{2}(jointIdx+1,t));...
%         mean(dataCur{1}(jointIdx+2,t)) mean(dataCur{2}(jointIdx+2,t))];

    conAng=[];
    curAng=[];
    fprintf('Calculating body angles...\n');
    for t=1:length(twinFull{subjectNo})
        
        tMs=twinFull{subjectNo}(t);
        
%         distTemp2=abs(nfrm2-(tSample(t)+t02slav{subjectNo}));
%         t2=find(distTemp2==min(distTemp2),1);
        
        midpoints=[mean(dataCur{1}(jointIdx,t)) mean(dataCur{2}(jointIdx,t));...
            mean(dataCur{1}(jointIdx+1,t)) mean(dataCur{2}(jointIdx+1,t));...
            mean(dataCur{1}(jointIdx+2,t)) mean(dataCur{2}(jointIdx+2,t))];
        
        conAng(t,2)=angle(diff(fliplr(midpoints(1,:)))+1i*diff(fliplr(midpoints(3,:))));
        conAng(t,1)=pi+conAng(t,2);
        
        
        for l=1:2
            
            curAng(t,l)=angle(-diff(dataCur{l}(jointIdx+2,t))+1i*diff(dataCur{l}(jointIdx,t)));
            curAng(t,l)=mod(2*pi+curAng(t,l),2*pi);
            angRel{subjectNo}(t,l)=(conAng(t,l)-curAng(t,l))*360/2/pi*(-2*l+3);
            
        end
       
    end
    
    fprintf('done\n');
end

%% Visualize body and face angles (Supplementary figure)
binC=-10:5:90;%-10:2.5:90;
nn=length(binC);
tickInd=find(mod(binC,15)==0);
mrgB=.035;
mrgS=.015;

h=1000;
nC=5;
nR=4;
w=h*nC/nR;
hfig2=figure;
set(hfig2,'position',[(1920-w)/2 (1200-h)/2 w h]);
axSz=[1/nC 1/nR];
cmap=aaltoGradient(256);
allNbf=[];
Nb=[]; %2D body histograms
Nbtrial=[];
Nf=[]; %2D face histograms
Nftrial=[];
gkrn=gaussKernel([5 5],.5);%zeros(5);gkrn(3,3)=1;
for ordNum=1:18
    subjectNo=ord(ordNum);
    ax(ordNum)=axes;
    set(ax(ordNum),'position',[mod(ordNum-1,nC)*axSz(1)+mrgB+mrgS 1-(floor((ordNum-1)/nC)+1)*axSz(2)+mrgB axSz-mrgB-mrgS]);
    hold on;
    N=zeros(nn,nn,3);
    for l=1:2
        [tempN,~]=hist3([relYaw{subjectNo}(:,l) angRel{subjectNo}(:,l)],{binC binC});
        N(:,:,l)=nanconv(tempN,gkrn,'nanout');
        N(:,:,l)=N(:,:,l)/max(max(N(:,:,l)));
    end
    [tempN,~]=hist3([angRel{subjectNo}(:,1) angRel{subjectNo}(:,2)],{binC binC});
    Nb(subjectNo,:,:)=nanconv(tempN,gkrn,'nanout');
    Nb(subjectNo,:,:)=Nb(subjectNo,:,:)/sum(sum(Nb(subjectNo,:,:),3),2);
    [tempN,~]=hist3([relYaw{subjectNo}(:,1) relYaw{subjectNo}(:,2)],{binC binC});
    Nf(subjectNo,:,:)=nanconv(tempN,gkrn,'nanout');
    Nf(subjectNo,:,:)=Nf(subjectNo,:,:)/sum(sum(Nf(subjectNo,:,:),3),2);
    for curTrial=1:9
        [tempN,~]=hist3([angRel{subjectNo}(logical(tWinAll{subjectNo}(:,curTrial)),1) angRel{subjectNo}(logical(tWinAll{subjectNo}(:,curTrial)),2)],{binC binC});
        Nbtrial(subjectNo,:,:,curTrial)=nanconv(tempN,gkrn,'nanout');
        Nbtrial(subjectNo,:,:,curTrial)=Nbtrial(subjectNo,:,:,curTrial)/sum(sum(Nbtrial(subjectNo,:,:,curTrial),3),2);
        [tempN,~]=hist3([relYaw{subjectNo}(logical(tWinAll{subjectNo}(:,curTrial)),1) relYaw{subjectNo}(logical(tWinAll{subjectNo}(:,curTrial)),2)],{binC binC});
        Nftrial(subjectNo,:,:,curTrial)=nanconv(tempN,gkrn,'nanout');
        Nftrial(subjectNo,:,:,curTrial)=Nftrial(subjectNo,:,:,curTrial)/sum(sum(Nftrial(subjectNo,:,:,curTrial),3),2);
    end
    meanFace(subjectNo,:)=nanmean(relYaw{subjectNo});
    meanBody(subjectNo,:)=nanmean(angRel{subjectNo});
    allNbf(subjectNo,:,:)=N(:,:,1)/sum(sum(N(:,:,1)));
    allNbf(subjectNo+18,:,:)=N(:,:,2)/sum(sum(N(:,:,2)));
    N1=reshape(cmap(round((N(:,:,1).^(1/2))*127)+127,:),size(N));
    N2=reshape(cmap(128-round((N(:,:,2).^(1/2))*127),:),size(N));
    %     N=(N1+N2)/2;
    
    image(N.^(1/2));
    
    set(gca,'xtick',tickInd,'xticklabel',binC(tickInd),'ytick',tickInd,'yticklabel',binC(tickInd),'ydir','normal');
    hold on;
    for k=1:length(tickInd)
        patchline([0 length(binC)]+.5,tickInd(k)*[1 1],'edgecolor',aaltoColors(5),'edgealpha',.25);
        patchline(tickInd(k)*[1 1],[0 length(binC)]+.5,'edgecolor',aaltoColors(5),'edgealpha',.25);
    end
    patchline([0 length(binC)]+.5,[0 length(binC)]+.5,'edgecolor',[1 1 1]*(mean(N(:))<.5),'edgealpha',.25);
    
    %     if ordNum==1
    ylabel('Face yaw (°)');
    xlabel('Body yaw (°)');
    %     end
    axis image
    title(d(subjectNo).name);
end
maxR=.75;
%% Rating and distance calculations
ord=[1 6:13 2:5 14:length(d)];
rateMat=[5.5  5.6  4.6  4.5  5.8  6.4  5.8  5.9  4.8  5.6  5.4  6.9  5.1  4.6  5.1  5.0  4.5  3.4  5.4  4.6  6.0  5.5  5.3  4.9  6.8  5.0  4.8  5.6  4.4  3.6  2.5  3.4  5.1  5.9  5.6  6.1  6.3  5.3;
    6.7  6.9  4.7  5.0  6.7  6.6  6.9  5.3  5.6  6.7  6.4  7.0  5.4  6.0  6.4  5.7  6.3  4.7  6.1  5.7  6.6  5.6  5.9  4.9  7.0  6.3  4.6  6.6  5.7  4.0  3.0  4.4  5.6  6.1  6.1  6.6  6.6  5.4;
    3.6  4.4  2.0  4.8  4.8  6.4  3.6  4.8  4.2  4.8  4.8  4.8  5.8  5.6  3.2  2.8  4.8  2.2  5.6  2.2  3.8  3.0  4.6  3.2  5.6  5.2  4.4  4.6  4.6  1.8  2.4  4.8  5.2  6.6  4.2  4.4  4.0  4.4;
    2.4  2.0  2.6  2.0  2.2  1.0  3.2  1.4  1.0  3.2  3.0  1.0  1.6  1.4  1.6  3.4  2.4  2.4  2.2  1.2  2.2  2.0  2.8  1.8  3.0  3.6  2.8  3.2  2.8  1.2  3.0  2.2  2.4  1.0  2.4  2.2  1.8  2.6;
    4.0  4.5  3.8  4.5  2.3  5.5  4.0  4.3  2.8  5.0  4.0  4.3  6.0  3.5  5.5  4.3  5.8  6.8  5.3  6.0  5.3  5.3  4.0  5.5  2.5  3.8  4.8  6.5  5.0  4.5  4.5  4.8  3.3  6.0  6.3  5.3  5.5  5.0];
subNames={'F1Y' 'F1R' 'F2Y' 'F2R' 'F3Y' 'F3R' 'F4Y' 'F4R' 'F5Y' 'F5R' 'F6Y' 'F6R' 'F7Y' 'F7R' 'F8Y' 'F8R' 'F9Y' 'F9R' 'F10Y' 'F10R' 'F11Y' 'F11R' 'F12Y' 'F12R' 'F13Y' 'F13R' 'F14Y' 'F14R' 'M1Y' 'M1R' 'M2Y' 'M2R' 'M3Y' 'M3R' 'M4Y' 'M4R' 'M5Y' 'M5R'};
dropSubs=false(length(subNames),1);
% dropSubs(19:20)=true; %F10 dyad had technical issues (no Kinect data)
% rateMat(:,dropSubs)=[];
% subNames(dropSubs)=[];
qdata=[];
fId=fopen([baseDir '/QuestionnaireData/ACIPS_AQ_SNQ_AGE_sorted_corrected.csv']);
line=fgetl(fId);
coms=strfind(line,',');
qTitles={line(coms(1)+1:coms(2)-1) line(coms(2)+1:coms(3)-1) line(coms(3)+1:coms(4)-1) line(coms(4)+1:coms(4)+3)};
for k=1:length(subNames)
    line=fgetl(fId);
    coms=strfind(line,',');
    subNamesFile{k}=line(1:coms(1)-1);
    qdata(:,k)=[str2num(line(coms(1)+1:coms(2)-1)) str2num(line(coms(2)+1:coms(3)-1)) str2num(line(coms(3)+1:coms(4)-1)) str2num(line(coms(4)+1:coms(5)-1))];
end
for k=1:size(qdata,1)
    qdata(k,isnan(qdata(k,:)))=nanmean(qdata(k,:)); %Because some participants' data is missing, let's replace it with the mean
end
%SNQ seems to be misunderstood by some participants so badly, that it
%cannot be used for them. To be safe, we omit the whole scale for now.
qdata(3,:)=[];
qTitles(3)=[];

pairNames=subNames(1:2:end);
subOrd=[1:2*9 2*10+1:length(rateMat)];
for k=1:length(pairNames)
    pairNames{k}(end)=[];
end
rateTitles={'Relatedness'
    'Interest/Enjoyment'
    'Perceived choice'
    'Pressure'%/tension'
    'Effort'};
rateTitles={rateTitles{:} qTitles{:}}
rateMat=[rateMat; qdata]
rate1=rateMat(:,1:2:end); %Rating for participant 1 (Yellow)
rate2=rateMat(:,2:2:end); %Rating for participant 2 (Red)
pairNames={'F1' 'F2' 'F3' 'F4' 'F5' 'F6' 'F7' 'F8' 'F9' 'F10' 'F11' 'F12' 'F13' 'F14' 'M1' 'M2' 'M3' 'M4' 'M5'}
namesReord=[1 11:14 2:9 15:19];
rate1=rate1(:,namesReord);
rate2=rate2(:,namesReord);
hbin=0:.05:3;
apprAvoidBin=-1.5:.05:1.5;
apprHist=zeros(length(apprAvoidBin),36);
distBars=zeros(length(hbin),length(d));

trialOrder=[1	1	0	0	1	1	0	0	1	1	0	0	1	1	1	1	0	0	1];
trialOrder=trialOrder(namesReord);

fixMaps=[];
% figure
for ordNum=1:18
    t0=tWin(1);
    subjectNo=ord(ordNum);
    
    cd([baseDir '/matDataKinectAndIMU'])
    d=dir('F*');
    d2=dir('M*');
    d(~[d(:).isdir])=[];
    d2(~[d2(:).isdir])=[];
    d=[d; d2];
    load([baseDir '/matDataKinectAndIMU/' d(subjectNo).name '_orientations.mat']);
    load([baseDir '/matDataKinectAndIMU/' d(subjectNo).name '.mat'],'t02s');
    %     t0=tWin(1);
    %     tEnd=tWin(end);
    tVec=twinFull{subjectNo};%linspace(t0,tEnd,(tEnd-t0)/1000*10);
    XYZ2=interp1(dataAll{subjectNo,3}(1,:),dataAll{subjectNo,3}([2:4 end-2:end],:)',tVec)';
    XYZ1=interp1(dataAll{subjectNo,1}(1,:),dataAll{subjectNo,1}([2:4 end-2:end],:)',tVec)';
    keyPressesCut{subjectNo}=keyPressesAll{subjectNo}(1:7:end,:); %One version of the IMU data reading script reads the keypresses multiple times!
    switch subjectNo
        case 1
            dropdata=logical([0	0	1	0	0	1	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0]);
            keyPressesCut{subjectNo}(dropdata,:)=[];
        case 6
            dropdata=logical([0	0	0	0	1	0	0	0	0	1	0	0	0	0	0	0	0	0	0	0]);
            keyPressesCut{subjectNo}(dropdata,:)=[];
        case 7
            dropdata=logical([0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0]);
            keyPressesCut{subjectNo}(dropdata,:)=[];
            keyPressesCut{subjectNo}(11,2)={'4'};
        case 8
            %End marker of 6th Zitternix round is missing, let's use
            %the mean duration of the setup time as an estimate for the
            %break duration
            keyPressesCut{subjectNo}(17:18,:)=keyPressesCut{subjectNo}(16:17,:);
            durs=diff(cell2mat(keyPressesCut{subjectNo}(:,1)));
            keyPressesCut{subjectNo}(16,1)={round(cell2mat(keyPressesCut{subjectNo}(17,1))-mean(durs(2:2:end-2)))};
        case 12
            keyPressesCut{subjectNo}(12,:)=[];
    end
    
    %After dropping unnecessary entries and adding missing ones, every
    %other entry should be the end point and denoted by a minus sign
    keyPressesCut{subjectNo}(2:2:end,2)={'-'};
    
    %Reorder so that first come the discussions (in the order they
    %took place) then the practice trial, then three cooperative
    %games and finally three competitive games.
    if trialOrder(subjectNo)
        curOrd=[1 9 2 4 6 8 3 5 7];
        
    else
        curOrd=[1 9 2 3 5 7 4 6 8];
    end
    
    %Cut the data into trials
    for trialNo=1:9
        
        tPoints{subjectNo}=twinFull{subjectNo};
        tWin=and(tPoints{subjectNo}>cell2mat(keyPressesCut{subjectNo}((curOrd(trialNo)-1)*2+1)),tPoints{subjectNo}<cell2mat(keyPressesCut{subjectNo}((curOrd(trialNo)-1)*2+2)));
        %         tWin((length(allYaw{subjectNo})+1):end)=[];
        tWinsAll{subjectNo}(:,trialNo)=tWin;
        distWin=sqrt(nansum((XYZ1(1:3,tWin)-XYZ2(1:3,tWin)).^2));
        meanDistWin(subjectNo,trialNo)=nanmean(distWin(:));
        
    end
    %Between trials
    tWin=nanmax(tWinsAll{subjectNo}')==0;
    distWin=sqrt(nansum((XYZ1(1:3,tWin)-XYZ2(1:3,tWin)).^2));
    meanDistWin(subjectNo,10)=nanmean(distWin(:));
    
    %Full session
    tWin=nanmax(tWinsAll{subjectNo}')>-inf;%==0;
    distWin=sqrt(nansum((XYZ1(1:3,tWin)-XYZ2(1:3,tWin)).^2));
    meanDistWin(subjectNo,11)=nanmean(distWin(:));
    [hbarWin,~]=hist(distWin(:,11),hbin);
    distBarsWin(:,subjectNo,11)=hbarWin/sum(hbarWin);
    
    
end
%% Correlation plots (distance and joint orienting plots; these are reorganized in Figure 4)
%Body-face plot correlations
h=1000;
nC=8;
nR=3;%2;
mrgB=.035;
mrgL=.04;
mrgT=.01*2;
mrgS=.015;
w=1900;
h=w*nR/nC;
if h>1100
    w=w*1100/h;
    h=1100;
end
if h>1900
    h=h*1900/w;
end
axSz=[1/nC 1/nR]*.95;

hfig3=figure;
set(hfig3,'position',[(1920-w)/2 (1200-h)/2 w h]);

cmapT=parula(128);
ordNum=0;

%Remove the outliers
rateMeanFull=(rate1+rate2)'/2;
rateMean=rateMeanFull;
outlierMat=isoutlier(rateMeanFull,'mean');%abs((rateMeanFull-repmat(mean(rateMeanFull),[size(rateMeanFull,1) 1]))./repmat(std(rateMeanFull),[size(rateMeanFull,1) 1]))>=3;

% rateMean(outlierMat)=nan;

soi={11 1:2 3 4:6 7:9 10};
plotTitles={'Full experiment' 'Conversation','Practice','Cooperative','Competitive','Between trials'};%'Between trials','All'}
for trialNo=1:length(plotTitles)
    %     subplot(1,5,trialNo);
    ordNum=ordNum+1;
    ax2(ordNum)=axes;
    set(ax2(ordNum),'position',[mod(ordNum-1,nC)*axSz(1)+mrgB+mrgL 1-(floor((ordNum-1)/nC)+1)*axSz(2)+mrgB-mrgT axSz-mrgB-mrgS]);
    %     hb=barh([mean(corr(meanDistWin(:,soi{trialNo}),zscore(rate1'+rate2'))',2) mean(corr(apprMeanWin(:,soi{trialNo}),rateMat(:,subOrd)')',2)]);
    
    hb=barh(mean(corr(meanDistWin(:,soi{trialNo}),zscore(rate1'+rate2'))',2));
    hold on
    %Outlierless correlation
    for ctrial=1:size(meanDistWin,2)
        for crating=1:size(rateMean,2)
            corvals(crating,ctrial)=partialcorr(meanDistWin(:,ctrial),rateMean(:,crating),double(outlierMat(:,crating)));
        end
    end
    
    hb2=barh(mean(corvals(:,soi{trialNo}),2));
    
    set(hb(1),'faceColor',aaltoColors(2));
    set(hb2(1),'faceColor',aaltoColors(5),'facealpha',.5);
    %     set(hb(2),'faceColor',aaltoColors(3));
    hold on;
    title(plotTitles{trialNo});
    % plot(,'color',aaltoColors(3));
    %Thresholds
    plot(-probabilityToCorrelation(18,.05)*[1 1],[.5 length(rateTitles)+.5],'--','color',aaltoColors(2)/1.3);
    plot(-.75*[1 1],[.5 length(rateTitles)+.5],'-','color',[1 1 1]);%aaltoColors(2)/1.7);
    plot(-probabilityToCorrelation(18,.05/8)*[1 1],[.5 length(rateTitles)+.5],'--','color',aaltoColors(5)*1.3);%aaltoColors(2)/1.7);
    plot(probabilityToCorrelation(18,.05)*[1 1],[.5 length(rateTitles)+.5],'--','color',aaltoColors(2)/1.3);
    plot(.75*[1 1],[.5 length(rateTitles)+.5],'-','color',[1 1 1]);%aaltoColors(2)/1.7);
    plot(probabilityToCorrelation(18,.05/8)*[1 1],[.5 length(rateTitles)+.5],'--','color',aaltoColors(5)*1.3);%aaltoColors(2)/1.7);
    axis tight
    if trialNo==1
        set(gca,'ytick',1:length(rateTitles),'yticklabel',rateTitles);
        xlabel('Distanceâ€“rating correlation (r)');
    else
        set(gca,'ytick',1:length(rateTitles),'yticklabel',[]);
    end
end


%Body-body correlations
cmapT=parula(128);
ordNum=nC;

if nR==3
    for k=1:8
        ordNum=ordNum+1;
        ax2(ordNum)=axes;
        set(ax2(ordNum),'position',[mod(ordNum-1,nC)*axSz(1)+mrgB+mrgL 1-(floor((ordNum-1)/nC)+1)*axSz(2)+mrgB-mrgT axSz-mrgB-mrgS]);
        
        tempmap=(reshape(squeeze(corr(reshape(Nb,size(Nb,1),[]),[rate1(k,:)+rate2(k,:)]')),size(Nb,2),[]));
    
        %Outlierless correlation
        for curRat=1:size(rateMeanFull,2)
            tempmap=(reshape(squeeze(partialcorr(reshape(Nb,size(Nb,1),[]),rateMean(:,k),double(outlierMat(:,k)))),size(Nb,2),[]));
        end
        tempmapc=min(length(cmapT),max(1,65+round(tempmap/maxR*63)));
        tempmapc(isnan(tempmapc))=1;
        statIm=reshape(cmapT(tempmapc,:),size(tempmapc,1),size(tempmapc,2),[]);
        tempmat=repmat(tempmap,[1 1 3]);
        image(statIm.*(.5*(abs(tempmat)<probabilityToCorrelation(18,.05/8)))+statIm.*(abs(tempmat)>probabilityToCorrelation(18,.05/8)));
        set(gca,'xtick',tickInd,'xticklabel',binC(tickInd),'ytick',tickInd,'yticklabel',binC(tickInd));%,'clim',[-.5 .5]);
        hold on;
        if any(any(abs(tempmat(:))>probabilityToCorrelation(18,.05/8)))
            contour(abs(tempmat(:,:,1))>probabilityToCorrelation(18,.05/8),1,'color',[0 0 0],'linewidth',2);
            contour(abs(tempmat(:,:,1))>probabilityToCorrelation(18,.05/8),1,'color',[1 1 1],'linewidth',1);
        end
        
        for kk=1:length(tickInd)
            patchline([0 length(binC)]+.5,tickInd(kk)*[1 1],'edgecolor',aaltoColors(5),'edgealpha',.25);
            patchline(tickInd(kk)*[1 1],[0 length(binC)]+.5,'edgecolor',aaltoColors(5),'edgealpha',.25);
        end
        patchline([0 length(binC)]+.5,[0 length(binC)]+.5,'edgecolor',[1 1 1]*(mean(N(:))<.5),'edgealpha',.25);
        
        %     if ordNum==1
        ylabel('Body, participant 1 (°)');
        xlabel('Body, participant 2 (°)');
        %     end
        axis image
        set(gca,'ydir','normal')
        title(rateTitles{k});
    end
end

%Face-face correlations
cmapT=parula(128);

for k=1:8
    ordNum=ordNum+1;
    ax2(ordNum)=axes;
    set(ax2(ordNum),'position',[mod(ordNum-1,nC)*axSz(1)+mrgB+mrgL 1-(floor((ordNum-1)/nC)+1)*axSz(2)+mrgB-mrgT axSz-mrgB-mrgS]);
    
    tempmap=(reshape(squeeze(corr(reshape(Nf,size(Nf,1),[]),[rate1(k,:)+rate2(k,:)]')),size(Nf,2),[]));
    
    %Outlierless correlation
%     tempmap=(reshape(squeeze(nancorr(reshape(Nf,size(Nf,1),[]),rateMean(:,k))),size(Nf,2),[]));
        for curRat=1:size(rateMeanFull,2)
            tempmap=(reshape(squeeze(partialcorr(reshape(Nf,size(Nf,1),[]),rateMean(:,k),double(outlierMat(:,k)))),size(Nf,2),[]));
        end
    tempmapc=min(length(cmapT),max(1,65+round(tempmap/maxR*63)));
    tempmapc(isnan(tempmapc))=1;
    statIm=reshape(cmapT(tempmapc,:),size(tempmapc,1),size(tempmapc,2),[]);
    tempmat=repmat(tempmap,[1 1 3]);
    image(statIm.*(.5*(abs(tempmat)<probabilityToCorrelation(18,.05/8)))+statIm.*(abs(tempmat)>probabilityToCorrelation(18,.05/8)));
    set(gca,'xtick',tickInd,'xticklabel',binC(tickInd),'ytick',tickInd,'yticklabel',binC(tickInd));%,'clim',[-.5 .5]);
    hold on;
    if any(any(abs(tempmat(:))>probabilityToCorrelation(18,.05/8)))
        contour(abs(tempmat(:,:,1))>probabilityToCorrelation(18,.05/8),1,'color',[0 0 0],'linewidth',2);
        contour(abs(tempmat(:,:,1))>probabilityToCorrelation(18,.05/8),1,'color',[1 1 1],'linewidth',1);
    end
    
    for kk=1:length(tickInd)
        patchline([0 length(binC)]+.5,tickInd(kk)*[1 1],'edgecolor',aaltoColors(5),'edgealpha',.25);
        patchline(tickInd(kk)*[1 1],[0 length(binC)]+.5,'edgecolor',aaltoColors(5),'edgealpha',.25);
    end
    patchline([0 length(binC)]+.5,[0 length(binC)]+.5,'edgecolor',[1 1 1]*(mean(N(:))<.5),'edgealpha',.25);
    
    if ordNum==7
        ylabel('Face, participant 1 (°)');
        xlabel('Face, participant 2 (°)');
    end
    axis image
    set(gca,'ydir','normal')
    title(rateTitles{k});
end

ordNum=ordNum-2;
ax2(ordNum)=axes;
set(ax2(ordNum),'position',[mod(ordNum-1,nC)*axSz(1)+mrgB+mrgL  1-(floor((ordNum-1)/nC)+1)*axSz(2)+mrgB-mrgT-.01  axSz(1)-mrgB-mrgS .02]);

image(permute(repmat(cmapT.*repmat(abs(linspace(-maxR,maxR,length(cmapT))')<probabilityToCorrelation(18,.05/8),[1 3]),[1 1 10])*.5+repmat(cmapT.*repmat(abs(linspace(-maxR,maxR,length(cmapT))')>probabilityToCorrelation(18,.05/8),[1 3]),[1 1 10]),[3 1 2]))
xticklocs=linspace(1,128,7);
xticklocs([3 5])=[];
set(gca,'ytick',[],'xtick',xticklocs,'xticklabel',[-.75 -.5 0 .5 .75])
%% Trial correlations (Head)

h=1000;
nC=9;
nR=5;%3;
mrgB=.035/2;
mrgL=.04/2;
mrgT=.01/2;
mrgS=.015/2;
w=1900;
h=w*nR/nC;
ordNum=0;
if h>1100
    w=w*1100/h;
    h=1100;
end
if h>1900
    h=h*1900/w;
end
axSz=[1/nC 1/nR]*.95;

hfig3=figure;
set(hfig3,'position',[(1920-w)/2 (1200-h)/2 w h]);

cmapT=parula(128);
for curRating=1:5
    for curTrial=1:9
    
        ordNum=ordNum+1;
        ax2(ordNum)=axes;
        set(ax2(ordNum),'position',[mod(ordNum-1,nC)*axSz(1)+mrgB+mrgL 1-(floor((ordNum-1)/nC)+1)*axSz(2)+mrgB-mrgT axSz-mrgB-mrgS]);
        
        tempmap=(reshape(squeeze(corr(reshape(Nftrial(:,:,:,curTrial),size(Nf,1),[]),[rate1(curRating,:)+rate2(curRating,:)]')),size(Nb,2),[]));
        tempmapc=min(length(cmapT),max(1,65+round(tempmap/maxR*63)));
        tempmapc(isnan(tempmapc))=1;
        statIm=reshape(cmapT(tempmapc,:),size(tempmapc,1),size(tempmapc,2),[]);
        tempmat=repmat(tempmap,[1 1 3]);
        image(statIm.*(.5*(abs(tempmat)<probabilityToCorrelation(18,.05/8)))+statIm.*(abs(tempmat)>probabilityToCorrelation(18,.05/8)));
        set(gca,'xtick',tickInd,'xticklabel',binC(tickInd),'ytick',tickInd,'yticklabel',binC(tickInd));%,'clim',[-.5 .5]);
        hold on;
        if any(any(abs(tempmat(:))>probabilityToCorrelation(18,.05/8)))
            contour(abs(tempmat(:,:,1))>probabilityToCorrelation(18,.05/8),1,'color',[0 0 0],'linewidth',2);
            contour(abs(tempmat(:,:,1))>probabilityToCorrelation(18,.05/8),1,'color',[1 1 1],'linewidth',1);
        end
        
        for kk=1:length(tickInd)
            patchline([0 length(binC)]+.5,tickInd(kk)*[1 1],'edgecolor',aaltoColors(5),'edgealpha',.25);
            patchline(tickInd(kk)*[1 1],[0 length(binC)]+.5,'edgecolor',aaltoColors(5),'edgealpha',.25);
        end
        patchline([0 length(binC)]+.5,[0 length(binC)]+.5,'edgecolor',[1 1 1]*(mean(N(:))<.5),'edgealpha',.25);
        
        if ordNum==9*4+1
            ylabel('Face, participant 1 (°)');
            xlabel('Face, participant 2 (°)');
        end
        axis image
        set(gca,'ydir','normal')
        title([rateTitles{curRating} ' ' trialNames{curTrial}]);
    end
end
ordNum=ordNum-2;
ax2(ordNum)=axes;
set(ax2(ordNum),'position',[mod(ordNum-1,nC)*axSz(1)+mrgB+mrgL  1-(floor((ordNum-1)/nC)+1)*axSz(2)+mrgB-mrgT-.01  axSz(1)-mrgB-mrgS .02]);

image(permute(repmat(cmapT.*repmat(abs(linspace(-maxR,maxR,length(cmapT))')<probabilityToCorrelation(18,.05/8),[1 3]),[1 1 10])*.5+repmat(cmapT.*repmat(abs(linspace(-maxR,maxR,length(cmapT))')>probabilityToCorrelation(18,.05/8),[1 3]),[1 1 10]),[3 1 2]))
xticklocs=linspace(1,128,7);
xticklocs([3 5])=[];
set(gca,'ytick',[],'xtick',xticklocs,'xticklabel',[-.75 -.5 0 .5 .75])
%% Trial correlations (Body)

h=1000;
nC=9;
nR=5;%3;
mrgB=.035/2;
mrgL=.04/2;
mrgT=.01/2;
mrgS=.015/2;
w=1900;
h=w*nR/nC;
ordNum=0;
if h>1100
    w=w*1100/h;
    h=1100;
end
if h>1900
    h=h*1900/w;
end
axSz=[1/nC 1/nR]*.95;

hfig3=figure;
set(hfig3,'position',[(1920-w)/2 (1200-h)/2 w h]);

cmapT=parula(128);
for curRating=1:5
    for curTrial=1:9
    
        ordNum=ordNum+1;
        ax2(ordNum)=axes;
        set(ax2(ordNum),'position',[mod(ordNum-1,nC)*axSz(1)+mrgB+mrgL 1-(floor((ordNum-1)/nC)+1)*axSz(2)+mrgB-mrgT axSz-mrgB-mrgS]);
        
        tempmap=(reshape(squeeze(corr(reshape(Nbtrial(:,:,:,curTrial),size(Nb,1),[]),[rate1(curRating,:)+rate2(curRating,:)]')),size(Nb,2),[]));
        tempmapc=min(length(cmapT),max(1,65+round(tempmap/maxR*63)));
        tempmapc(isnan(tempmapc))=1;
        statIm=reshape(cmapT(tempmapc,:),size(tempmapc,1),size(tempmapc,2),[]);
        tempmat=repmat(tempmap,[1 1 3]);
        image(statIm.*(.5*(abs(tempmat)<probabilityToCorrelation(18,.05/8)))+statIm.*(abs(tempmat)>probabilityToCorrelation(18,.05/8)));
        set(gca,'xtick',tickInd,'xticklabel',binC(tickInd),'ytick',tickInd,'yticklabel',binC(tickInd));%,'clim',[-.5 .5]);
        hold on;
        if any(any(abs(tempmat(:))>probabilityToCorrelation(18,.05/8)))
            contour(abs(tempmat(:,:,1))>probabilityToCorrelation(18,.05/8),1,'color',[0 0 0],'linewidth',2);
            contour(abs(tempmat(:,:,1))>probabilityToCorrelation(18,.05/8),1,'color',[1 1 1],'linewidth',1);
        end
        
        for kk=1:length(tickInd)
            patchline([0 length(binC)]+.5,tickInd(kk)*[1 1],'edgecolor',aaltoColors(5),'edgealpha',.25);
            patchline(tickInd(kk)*[1 1],[0 length(binC)]+.5,'edgecolor',aaltoColors(5),'edgealpha',.25);
        end
        patchline([0 length(binC)]+.5,[0 length(binC)]+.5,'edgecolor',[1 1 1]*(mean(N(:))<.5),'edgealpha',.25);
        
        if ordNum==9*4+1
            ylabel('Face, participant 1 (°)');
            xlabel('Face, participant 2 (°)');
        end
        axis image
        set(gca,'ydir','normal')
        title(rateTitles{curRating});
    end
end
ordNum=ordNum-2;
ax2(ordNum)=axes;
set(ax2(ordNum),'position',[mod(ordNum-1,nC)*axSz(1)+mrgB+mrgL  1-(floor((ordNum-1)/nC)+1)*axSz(2)+mrgB-mrgT-.01  axSz(1)-mrgB-mrgS .02]);

image(permute(repmat(cmapT.*repmat(abs(linspace(-maxR,maxR,length(cmapT))')<probabilityToCorrelation(18,.05/8),[1 3]),[1 1 10])*.5+repmat(cmapT.*repmat(abs(linspace(-maxR,maxR,length(cmapT))')>probabilityToCorrelation(18,.05/8),[1 3]),[1 1 10]),[3 1 2]))
xticklocs=linspace(1,128,7);
xticklocs([3 5])=[];
set(gca,'ytick',[],'xtick',xticklocs,'xticklabel',[-.75 -.5 0 .5 .75])
%% 3D things (Only included a footnote; mean pitch of participants is not enough to improve classification)
ordNum=ordNum+1;
ax2(ordNum)=axes;
set(ax2(ordNum),'position',[mod(ordNum-1,nC)*axSz(1)+mrgB+mrgL 1-(floor((ordNum-1)/nC)+1)*axSz(2)+mrgB-mrgT axSz-mrgB-mrgS]);
% 3d plot
% figure
bins={(-10:5:90) (-10:5:90) (0:5:90)};
binsstretch=bins;
for k=1:3
    binsstretch{k}(1)=-inf;
    binsstretch{k}(end)=inf;
end
isostep=2;
isoscale=3; %This is used for resizing the 3D histogram before making isosurfaces
wsz=[1800 900];

% Make the 3D (or 4D) histograms of orienting duration
allH3d=repmat(zeros(cellfun(@length,bins)-1),[1 1 1 18]);
for subjectNo=1:18
    
    X=subData{subjectNo}(:,1);
    Y=subData{subjectNo}(:,2);
    Z=abs(mean(subData{subjectNo}(:,3:4),2));
    
    h3d=zeros(cellfun(@length,bins)-1);
    % camlight
    
    for x=1:length(bins{1})-1
        for y=1:length(bins{2})-1
            for z=1:length(bins{3})-1
                h3d(x,y,z)=sum((X>=binsstretch{1}(x)).*(X<binsstretch{1}(x+1)).*...
                               (Y>=binsstretch{2}(y)).*(Y<binsstretch{2}(y+1)).*...
                               (Z>=binsstretch{3}(z)).*(Z<binsstretch{3}(z+1)));
            end
        end
    end
    h3d=h3d/sum(h3d(:))*1000;
    maxVals(subjectNo)=max(h3d(:));
    allH3d(:,:,:,subjectNo)=h3d;
    
end

%correlations of gaze and ratings/personality traits
corh3d=reshape(corr(reshape(permute(convn(allH3d,gaussKernel([9 9 9],.5),'same'),[4 1 2 3]),size(allH3d,4),[]),rate1'+rate2'),size(h3d,1),size(h3d,2),size(h3d,3),[]);%reshape(corr(reshape(permute(imfilter(allH3d,gaussKernel([9 9 9],.5)),[4 1 2 3]),size(allH3d,4),[]),rate1'+rate2'),size(h3d,1),size(h3d,2),size(h3d,3),[]);

[XX,YY,ZZ]=ndgrid((bins{1}(1:end-1)+bins{1}(2:end))/2,(bins{2}(1:end-1)+bins{2}(2:end))/2,(bins{3}(1:end-1)+bins{3}(2:end))/2);
allGazeAng=[gzAng{1} ones(length(gzAng{1}),1);...
    gzAng{2} ones(length(gzAng{2}),1)*2;...
    gzAng{3} ones(length(gzAng{3}),1)*3;...
    gzAng{4} ones(length(gzAng{4}),1)*4];

allGazeAng=cat(2,allGazeAng(:,1),allGazeAng(:,2),mean(allGazeAng(:,3:4),2),allGazeAng(:,end));
%Run a naive Bayes classifier multiple times with balanced group sizes to
%get semi-stable estimates of the posterior probabilities
Posterior=[];
confMat=[];
%     idxDrop=idx(round(min(numConds)*.5)+1:end);
nTrain=floor(min(numConds)*.5);
fprintf('Classifying');

for iter=1:50
    fprintf('.');
    allGazePrune=[];
    allGazeTest=[];
    for k=1:4
        idx=find(allGazeAng(:,end)==k);%idx=find(allGazeAng(:,5)==k); FOR EXTENDING WITH PITCH DATA
        idx=idx(randperm(length(idx)));
        idxTest=idx((nTrain+1):(2*nTrain));
        idx=idx(1:nTrain);
        allGazePrune=[allGazePrune;allGazeAng(idx,:)]; %#ok<AGROW>
        allGazeTest=[allGazeTest;allGazeAng(idxTest,:)]; %#ok<AGROW>
    end
    %     allGazePrune(:,3)=0+rand(length(allGazePrune),1)*.0001;
    %     allGazeTest(:,3)=0+rand(length(allGazePrune),1)*.0001;
    mdl = fitcnb(allGazePrune(:,1:end-1),allGazePrune(:,end));
    for subjectNo=1:18
        predictedspecies3 = predict(mdl,cat(2,subData{subjectNo}(:,1),subData{subjectNo}(:,2),mean(subData{subjectNo}(:,3:4),2)));
        for ll=1:4
            gazePercentage(subjectNo,iter,ll)=sum(predictedspecies3==ll)/length(predictedspecies3);
        end
    end
end
fprintf('done\n');

cmap=autumn(round(ceil(quantile(maxVals(:),.9)/isostep)*3/2));
cmap(1:round(size(cmap,1)/3),:)=[];
cmap(end+1:round(ceil(max(maxVals(:))/isostep)*3/2),:)=1;
cmap=parula(length(cmap));
 
figure
k=5;
bgcmap=gray(128+50);%parula(128);%*3/4;
bgcmap(1:50,:)=[];
colormap (bgcmap);
marg=.06;
titls={'IMU' 'Kinect'};

title(rateTitles2{k},'horizontalalignment','center')


% X=subData{subjectNo}(:,1);
% Y=subData{subjectNo}(:,2);
% Z=abs(mean(subData{subjectNo}(:,3:4),2));%abs(allpitch{k}(:,1)+allpitch{k}(:,2))/2;
set(gcf,'renderer','opengl')
set(gcf,'color',[1 1 1])
set(gca,'color','none','Projection','perspective')
hold on
h3d=corh3d(:,:,:,k);
% h3d=permute(h3d,[2 1 3]);
predictedGrid=reshape(predict(mdl,[XX(:) YY(:) ZZ(:)]),length(bins{1})-1,length(bins{2})-1,[]);
% predictedGrid=permute(predictedGrid,[2 1 3]);
h3dalpha=abs(h3d);
h3dalpha(h3dalpha<probabilityToCorrelation(18,.05/8))=0;
% camlight
set(gca,'zdir','reverse')
axis([min(bins{1}) max(bins{1}) min(bins{2}) max(bins{2}) min(bins{3}) max(bins{3})]);
xlabel('Yaw 1 (°)')
ylabel('Yaw 2 (°)')
zlabel('Mean pitch (°)')
view(45,30)
grid on
surface([min(bins{1}) min(bins{1}); min(bins{1}) min(bins{1})],...
    [min(bins{2}) max(bins{2}); min(bins{2}) max(bins{2})],...
    [min(bins{3}) min(bins{3}); max(bins{3}) max(bins{3})],...
    'FaceColor', 'texturemap', 'CData', 255*squeeze(nansum(h3dalpha,1))'/size(h3d,1),...
    'CDataMapping', 'direct');%,'facealpha',.85);
%Sum over Y
surface([min(bins{1}) max(bins{1}); min(bins{1}) max(bins{1})],...
    [max(bins{2}) max(bins{2}); max(bins{2}) max(bins{2})],...
    [min(bins{3}) min(bins{3}); max(bins{3}) max(bins{3})],...
    'FaceColor', 'texturemap', 'CData', 255*squeeze(nansum(h3dalpha,2))'/size(h3d,2),...
    'CDataMapping', 'direct');%,'facealpha',.85);
%Sum over Z
surface([min(bins{1}) max(bins{1}); min(bins{1}) max(bins{1})],...
    [min(bins{2}) min(bins{2}); max(bins{2}) max(bins{2})],...
    [max(bins{3}) max(bins{3}); max(bins{3}) max(bins{3})],...
    'FaceColor', 'texturemap', 'CData', 255*squeeze(nansum(h3dalpha,3))'/size(h3d,3),...
    'CDataMapping', 'direct');%,'facealpha',.85);
h3d=(h3d+1);%*128+127;
cmap2=parula(256);
%     cmap2(1:round(size(cmap2,1)/3),:)=[];
%     cmap2(end+1:256,:)=1;
for xx=1:length(bins{1})-1
    imX=round(127*squeeze(h3d(xx,:,:))');
    imX(isnan(imX))=127;
    imXa=squeeze(h3dalpha(xx,:,:))';
    imXa(isnan(imXa))=0;
    hsurf=surface([bins{1}(xx) bins{1}(xx); bins{1}(xx) bins{1}(xx)]+diff(bins{1}(1:2))/2,...
        [min(bins{2}) max(bins{2}); min(bins{2}) max(bins{2})],...
        [min(bins{3}) min(bins{3}); max(bins{3}) max(bins{3})],...
        'FaceColor', 'texturemap', 'CData',reshape(cmap2(imX+1,:),size(imX,1),size(imX,2),[]),...
        'CDataMapping', 'direct','Facealpha','texturemap',...
        'alphadata',uint8(32*imXa),...
        'alphadatamapping','direct','edgecolor','none');
end
for yy=1:length(bins{2})-1
    imY=round(127*squeeze(h3d(:,yy,:))');
    imY(isnan(imY))=127;
    imYa=squeeze(h3dalpha(:,yy,:))';
    imYa(isnan(imYa))=0;
    hsurf=surface([min(bins{1}) max(bins{1}); min(bins{1}) max(bins{1})],...
        [bins{2}(yy) bins{2}(yy); bins{2}(yy) bins{2}(yy)]+diff(bins{2}(1:2))/2,...
        [min(bins{3}) min(bins{3}); max(bins{3}) max(bins{3})],...
        'FaceColor', 'texturemap', 'CData',reshape(cmap2(imY+1,:),size(imY,1),size(imY,2),[]),...
        'CDataMapping', 'direct','Facealpha','texturemap',...
        'alphadata',uint8(32*imYa),...
        'alphadatamapping','direct','edgecolor','none');
end
for zz=1:length(bins{3})-1
    imZ=round(127*squeeze(h3d(:,:,zz))');
    imZ(isnan(imZ))=127;
    imZa=squeeze(h3dalpha(:,:,zz))';
    imZa(isnan(imZa))=0;
    hsurf=surface([min(bins{1}) max(bins{1}); min(bins{1}) max(bins{1})],...
        [min(bins{2}) min(bins{2}); max(bins{2}) max(bins{2})],...
        [bins{3}(zz) bins{3}(zz); bins{3}(zz) bins{3}(zz)]+diff(bins{3}(1:2))/2,...
        'FaceColor', 'texturemap', 'CData',reshape(cmap2(imZ+1,:),size(imZ,1),size(imZ,2),[]),...
        'CDataMapping', 'direct','Facealpha','texturemap',...
        'alphadata',uint8(32*imZa),...
        'alphadatamapping','direct','edgecolor','none');
end
%         hsc=scatter3(X,Y,Z,'Marker','o','SizeData',2,'MarkerFaceColor',aaltoColors(2),'MarkerFaceAlpha',1,'MarkerEdgeColor','none');
%         for ll=1:4
%             for subjectNo=1:18
%                 scatter3(mean(subData{subjectNo}(dataLabels{subjectNo}==ll,3:4),2),-subData{subjectNo}(dataLabels{subjectNo}==ll,2),subData{subjectNo}(dataLabels{subjectNo}==ll,1),'Marker','o','SizeData',2,'MarkerFaceColor',colors(ll,:),'MarkerFaceAlpha',.15,'MarkerEdgeColor','none');
% %                 scatter3(-subData{subjectNo}(dataLabels{subjectNo}==ll,2),subData{subjectNo}(dataLabels{subjectNo}==ll,1),mean(subData{subjectNo}(dataLabels{subjectNo}==ll,3:4),2),'Marker','o','SizeData',2,'MarkerFaceColor',colors(ll,:),'MarkerFaceAlpha',.15,'MarkerEdgeColor','none');
%             end
%         end

for ll=1:4
    scatter3(XX(and(predictedGrid==ll,h3dalpha>0)),YY(and(predictedGrid==ll,h3dalpha>0)),ZZ(and(predictedGrid==ll,h3dalpha>0)),'Marker','o','SizeData',80,'LineWidth',1,'MarkerEdgeColor',colors(ll,:),'MarkerFaceAlpha',.2,'MarkerFaceColor','none');
    drawnow
end
%% Differences in joint orienting between conditions
%--------------------------------------------------------------------------
load([baseDir '/PreprocessedData/gazedata_with_correct_time_windows.mat'])
load([baseDir '/PreprocessedData/gazedata_with_correct_time_windows_reliability_third.mat'],'gazedata','gazedatarel','gazedataorig')
gzreli=[];
fullLabels1=[];
fullLabels2=[];
fullLabels3=[];
fullLabelsCons=[];

for k=1:18
    gazedatacons{k}=mode(cat(3,gazedataorig{k},gazedatarel{k},gazedata{k}),3);
end
gazedata=gazedatacons;
addpath('/home/juha/matlab-codes/')
addpath('/home/juha/matlab-codes/aalto/')
addpath('/home/juha/matlab-codes/nanconv/')

%% Make probability field for Naive Bayes (used to be kNN) classifier for visualization
gzAngOog=[];
colOrd=[2 3 1 5];
for k=1:4
    gzAng{k}=zeros(0,2);%4);
end
numConds=zeros(1,4);
clear ec sa oog other
for k=1:4
    for subjectNo=1:18
        if k<4
            ec{subjectNo,k}=and(and(gazedata{subjectNo}(3,:),gazedata{subjectNo}(4,:)),gazedata{subjectNo}(22,:)==k);
            sa{subjectNo,k}=and(and(gazedata{subjectNo}(5,:),gazedata{subjectNo}(6,:)),gazedata{subjectNo}(22,:)==k);
            oog{subjectNo,k}=or(and(and(gazedata{subjectNo}(3,:),gazedata{subjectNo}(6,:)),gazedata{subjectNo}(22,:)==k),...
                and(and(gazedata{subjectNo}(4,:),gazedata{subjectNo}(5,:)),gazedata{subjectNo}(22,:)==k));
            other{subjectNo,k}=and(~or(ec{subjectNo,k},or(sa{subjectNo,k},oog{subjectNo,k})),gazedata{subjectNo}(22,:)==k);
        else
            ec{subjectNo,k}=and(gazedata{subjectNo}(3,:),gazedata{subjectNo}(4,:));
            sa{subjectNo,k}=and(gazedata{subjectNo}(5,:),gazedata{subjectNo}(6,:));
            oog{subjectNo,k}=or(and(gazedata{subjectNo}(3,:),gazedata{subjectNo}(6,:)),...
                and(gazedata{subjectNo}(4,:),gazedata{subjectNo}(5,:)));
            other{subjectNo,k}=~or(ec{subjectNo,k},or(sa{subjectNo,k},oog{subjectNo,k}));
            numConds=numConds+[sum(ec{subjectNo,k}) sum(sa{subjectNo,k}) sum(oog{subjectNo,k}) sum(other{subjectNo,k})];
            %Make a list of all gaze angles in each condition
            nEC(subjectNo)=sum(ec{subjectNo,k})/length(ec{subjectNo,k});
            % FOR EXTENDING WITH PITCH DATA add the mean pitch for example
            % (3D) or both pitches (4D)
            gzAng{1}=[gzAng{1};[gazedata{subjectNo}(1,ec{subjectNo,k})' gazedata{subjectNo}(2,ec{subjectNo,k})']];% gazedata{subjectNo}(25,ec{subjectNo,k})' gazedata{subjectNo}(26,ec{subjectNo,k})']];
            gzAng{2}=[gzAng{2};[gazedata{subjectNo}(1,sa{subjectNo,k})' gazedata{subjectNo}(2,sa{subjectNo,k})']];% gazedata{subjectNo}(25,sa{subjectNo,k})' gazedata{subjectNo}(26,sa{subjectNo,k})']];
            gzAng{3}=[gzAng{3};[gazedata{subjectNo}(1,oog{subjectNo,k})' gazedata{subjectNo}(2,oog{subjectNo,k})']];% gazedata{subjectNo}(25,oog{subjectNo,k})' gazedata{subjectNo}(26,oog{subjectNo,k})']];
            gzAngOog=[gzAngOog;gazedata{subjectNo}(2,and(gazedata{subjectNo}(3,:)==1,gazedata{subjectNo}(6,:)==1))';gazedata{subjectNo}(1,and(gazedata{subjectNo}(4,:)==1,gazedata{subjectNo}(5,:)==1))'];%;gazedata{subjectNo}(26,and(gazedata{subjectNo}(3,:)==1,gazedata{subjectNo}(6,:)==1))';gazedata{subjectNo}(25,and(gazedata{subjectNo}(4,:)==1,gazedata{subjectNo}(5,:)==1))';]; %#ok<AGROW>
            gzAng{4}=[gzAng{4};[gazedata{subjectNo}(1,other{subjectNo,k})' gazedata{subjectNo}(2,other{subjectNo,k})']];% gazedata{subjectNo}(25,other{subjectNo,k})' gazedata{subjectNo}(26,other{subjectNo,k})']];
        end
        
        
        
    end
end
allGazeAng=[gzAng{1} ones(length(gzAng{1}),1);...
            gzAng{2} ones(length(gzAng{2}),1)*2;...
            gzAng{3} ones(length(gzAng{3}),1)*3;...
            gzAng{4} ones(length(gzAng{4}),1)*4];
% allGazeAng(allGazeAng==0)=nan;        

gridInd=-10:1:90;
% [XX,YY,ZZ]=ndgrid(gridInd,gridInd,-gridInd);
[XX,YY]=ndgrid(gridInd,gridInd);
% probPlane=repmat(XX,[1 1 4])*0;
% probScale=probPlane;
% numConds=[1 1 1 1];
% kNNk=100;
%mdl = fitcnb(X,Y);SVMmodelECvsJA=fitcsvm(gazeVec(idx,:),double(ec(idx))+1);labelsPredECvsJA=SVMmodelECvsJA.predict(gazeVec(idx,:));

% for x=1:length(XX)
%     for y=1:length(YY)
%         
%         [~,idx]=sort(sqrt((allGazeAng(:,1)-YY(y,x)).^2+(allGazeAng(:,2)-XX(y,x)).^2),'ascend');
%         probPlane(y,x,1)=sum(allGazeAng(idx(1:kNNk),3)==1)/kNNk;
%         probPlane(y,x,3)=sum(allGazeAng(idx(1:kNNk),3)==2)/kNNk;
%         probPlane(y,x,2)=sum(allGazeAng(idx(1:kNNk),3)==3)/kNNk;
%         probPlane(y,x,4)=sum(allGazeAng(idx(1:kNNk),3)==4)/kNNk;
%         probIm(y,x,:)=aaltoColors(2)*probPlane(y,x,1)+aaltoColors(3)*probPlane(y,x,3)+aaltoColors(1)*probPlane(y,x,2);
%         
%         probScale(y,x,:)=squeeze(probPlane(y,x,:)).*(numConds(:)./sum(numConds));
%         probScale(y,x,:)=squeeze(probScale(y,x,:)/sum(probScale(y,x,:),3));
%         probImScale(y,x,:)=aaltoColors(2)*probScale(y,x,1)+aaltoColors(3)*probScale(y,x,3)+aaltoColors(1)*probScale(y,x,2);
%         [~,temp]=max(probPlane(y,x,:));
%         maxProb(y,x)=temp(1);
%     end
% end

%Run a naive Bayes classifier multiple times with balanced group sizes to
%get semi-stable estimates of the posterior probabilities
Posterior=[];
for iter=1:50
    allGazePrune=[];
    for k=1:4
        idx=find(allGazeAng(:,3)==k);%idx=find(allGazeAng(:,5)==k); FOR EXTENDING WITH PITCH DATA
        idx=idx(randperm(length(idx)));
        idx(round(min(numConds)*1)+1:end)=[];
        allGazePrune=[allGazePrune;allGazeAng(idx,:)]; %#ok<AGROW>
    end
    mdl = fitcnb(allGazePrune(:,1:2),allGazePrune(:,3));%mdl = fitcnb(allGazePrune(:,1:4),allGazePrune(:,5)); FOR EXTENDING WITH PITCH DATA
    [predictedspecies,Posterior(:,:,iter),~] = predict(mdl,[YY(:) XX(:)]);% ZZ(:)]);
end
probPlane=reshape(mean(Posterior(:,[1 3 2 4],:),3),size(repmat(XX,[1 1 4])));
sz=size(probPlane);
probIm=repmat(permute(aaltoColors(2),[1 3 2]),[sz(1:2) 1]).*probPlane(:,:,1)+repmat(permute(aaltoColors(3),[1 3 2]),[sz(1:2) 1]).*probPlane(:,:,3)+repmat(permute(aaltoColors(1),[1 3 2]),[sz(1:2) 1]).*probPlane(:,:,2);
probScale=probPlane;
probImScale=probIm;
%%
soi={1:2 4:6 7:9};%{7 8 9};%[4 7] [5 8] [6 9]};%1:2 4:6 7:9};
hfig=figure;
w=1920-500;
scl=2.25;
h=w/4*scl;
mrg=.06;
mrkSz=10;
mrkAlpha=.9;
bgAlpha=.8;%.7;
set(hfig,'position',[(1920-w)/2 (1200-h)/2 w h],'color',[1 1 1])
plotnames={'Conversation','Cooperative','Competitive'};%'First' 'Second' 'Third'};%'Conversation','Cooperative','Competitive','All'};%'Conversation 1','Conversation 2','Practice'};%'Conversation','Cooperative','Competitive','All'};
% for  k=1:4
%     gzAng{k}=zeros(0,2);
% end
% gzAngOog=[];
for k=1:4
    axpos=[mrg/2+(k-1)*((.9)/4+mrg/2) mrg+.5*scl/2  (h/w/scl-mrg) (1/scl-mrg*2)];
    hax(k,1)=axes;
    set(hax(k,1),'position',axpos);
    if k==4
        
        hold on;
        
        for kk=1:4
            if kk~=3
                [hbar,hbin]=hist(gzAng{kk}(:),-50:5:120);
            else
                [hbar,hbin]=hist(gzAngOog(:),-50:5:120);
            end
            plot(hbin,hbar/sum(hbar),'color',aaltoColors(colOrd(kk)),'linewidth',2);
        end
        hleg=legend('Eye contact','Joint attention','Other-oriented','Other');
        set(hleg,'Location','northeast');
        xlabel('Yaw °');
        ylabel('% timepoints')
    else
        %         hax(k,1)=axes;
        
        image(probImScale(:,:,1:3)*bgAlpha+(1-bgAlpha));%(2/3*probIm+1/3))
        set(hax(k,1),'ydir','normal','visible','off');%,'position',[mrg/2+(k-1)*((.9)/4+mrg/2) mrg  (h/w-mrg) (1-mrg*2)],'visible','off');
        hax(k,2)=axes;
        set(hax(k,2),'position',axpos,'color','none','xtick',-30:15:90,'ytick',-30:15:90);
        title(plotnames{k});
        hold on;
        %First the joint attention, since it will otherwise cover everything
        for subjectNo=1:18
            
            scatter(gazedata{subjectNo}(1,sa{subjectNo,k}),gazedata{subjectNo}(2,sa{subjectNo,k}),'o','markerfacecolor',aaltoColors(3),'markeredgecolor',[0 0 0],'sizedata',mrkSz,'markerfacealpha',mrkAlpha,'markeredgealpha',mrkAlpha/2);
            %         gzAng{2}=[gzAng{2};[gazedata{subjectNo}(1,sa{subjectNo,k})' gazedata{subjectNo}(2,sa{subjectNo,k})']];
            
        end
        %Then "other" category
        for subjectNo=1:18
            scatter(gazedata{subjectNo}(1,other{subjectNo,k}),gazedata{subjectNo}(2,other{subjectNo,k}),'o','markerfacecolor',aaltoColors(5),'markeredgecolor',[0 0 0],'sizedata',mrkSz,'markerfacealpha',mrkAlpha,'markeredgealpha',mrkAlpha/2);
            %         gzAng{4}=[gzAng{4};[gazedata{subjectNo}(1,other{subjectNo,k})' gazedata{subjectNo}(2,other{subjectNo,k})']];
        end
        %Then other-oriented gaze, i.e. looking at the person who is looking at
        %the shared target
        for subjectNo=1:18
            scatter(gazedata{subjectNo}(1,oog{subjectNo,k}),gazedata{subjectNo}(2,oog{subjectNo,k}),'o','markerfacecolor',aaltoColors(1),'markeredgecolor',[0 0 0],'sizedata',mrkSz,'markerfacealpha',mrkAlpha,'markeredgealpha',mrkAlpha/2);
            %         gzAng{3}=[gzAng{3};[gazedata{subjectNo}(1,oog{subjectNo,k})' gazedata{subjectNo}(2,oog{subjectNo,k})']];
            %         gzAngOog=[gzAngOog;gazedata{subjectNo}(2,and(gazedata{subjectNo}(3,:)==1,gazedata{subjectNo}(6,:)==1))';gazedata{subjectNo}(1,and(gazedata{subjectNo}(4,:)==1,gazedata{subjectNo}(5,:)==1))'];
        end
        %And finally eye contact
        for subjectNo=1:18
            scatter(gazedata{subjectNo}(1,ec{subjectNo,k}),gazedata{subjectNo}(2,ec{subjectNo,k}),'o','markerfacecolor',aaltoColors(2),'markeredgecolor',[0 0 0],'sizedata',mrkSz,'markerfacealpha',mrkAlpha,'markeredgealpha',mrkAlpha/2);
            %         gzAng{1}=[gzAng{1};[gazedata{subjectNo}(1,ec{subjectNo,k})' gazedata{subjectNo}(2,ec{subjectNo,k})']];
            
        end
        
        grid on;
        axis([-10 90 -10 90]);
        %         ords=[1 2 3;2 1 3;3 1 2];
        conOrd=[1 3 2 4];
        smscale=3;
        [xx,yy]=ndgrid(-smscale:smscale,-smscale:smscale);
        strel=sqrt(xx.^2+yy.^2)<=(smscale*.7);%[0 1 0; 1 1 1; 0 1 0];
        for cc=1:1%4
            probThr=double(probScale(:,:,conOrd(cc))>=max(probScale,[],3));%imresize(imerode(imfilter(imresize(double(probScale(:,:,conOrd(cc))>=max(probScale,[],3)),smscale),gaussKernel([5 5],2)),strel),1/smscale);
%             Ccon=contourc(imerode(double(probScale(:,:,conOrd(cc))==max(probScale,[],3)),strel),[.5 .5]);
            Ccon=contourc(probThr,[.5 .5]);
            lineSeg=find(Ccon(1,:)==.5);
            lineSeg(end+1)=length(Ccon);
            for ll=1:length(lineSeg)-1
                plot(Ccon(1,(lineSeg(ll)+1):(lineSeg(ll+1)-1))-10,Ccon(2,(lineSeg(ll)+1):(lineSeg(ll+1)-1))-10,'k','linewidth',2);
                plot(Ccon(1,(lineSeg(ll)+1):(lineSeg(ll+1)-1))-10,Ccon(2,(lineSeg(ll)+1):(lineSeg(ll+1)-1))-10,'color',aaltoColors(colOrd(cc)));
            end
        end
        if k==1
            ylabel('Participant 1 yaw °')
            xlabel('Participant 2 yaw °')
        end
    end
    drawnow;
end
allGazeAng=[gzAng{1} ones(length(gzAng{1}),1);...
    gzAng{2} ones(length(gzAng{2}),1)*2;...
    gzAng{3} ones(length(gzAng{3}),1)*3;...
    gzAng{4} ones(length(gzAng{4}),1)*4];
allGazeAng(allGazeAng==0)=nan;

pairs=[1 2;1 3;2 3];
for k=1:size(pairs,1)
    titlesPaired{k}=sprintf('%s vs. %s',plotnames{pairs(k,1)},plotnames{pairs(k,2)});% Conversation vs. Cooperative','Conversation vs. Competitive','Cooperative vs. Competitive'};
end
% titlesPaired={'Conversation vs. Cooperative','Conversation vs. Competitive','Cooperative vs. Competitive'};


%
h3bins=-10:5:90;%-30:5:90
Nalltrials2=[];
NalltrialsMean=[];
for subjectNo=1:18
    for cond=1:9
        [Nalltrials2(:,:,cond,subjectNo),~]=hist3([relYaw{subjectNo}(logical(tWinAll{subjectNo}(:,cond)),1) relYaw{subjectNo}(logical(tWinAll{subjectNo}(:,cond)),2)],{h3bins h3bins});
        Nalltrials2(:,:,cond,subjectNo)=nanconv(Nalltrials2(:,:,cond,subjectNo)/sum(sum(Nalltrials2(:,:,cond,subjectNo))),gaussKernel([5 5],1),'nanout');
    end
    for cond=1:3
        [NalltrialsMean(:,:,cond,subjectNo),~]=hist3([relYaw{subjectNo}(logical(max(tWinAll{subjectNo}(:,soi{cond}),[],2)),1) relYaw{subjectNo}(logical(max(tWinAll{subjectNo}(:,soi{cond}),[],2)),2)],{h3bins h3bins});
        NalltrialsMean(:,:,cond,subjectNo)=nanconv(NalltrialsMean(:,:,cond,subjectNo)/sum(sum(NalltrialsMean(:,:,cond,subjectNo))),gaussKernel([5 5],1),'nanout');
    end
end
%ANOVA, mass univariate (R1 addition)
%Make condition and subject labels
subjectLabelImage=NalltrialsMean*0;
conditionLabelImage=NalltrialsMean*0;
spaceLabelImage=NalltrialsMean*0;
for k=1:18
    subjectLabelImage(:,:,:,k)=k;
end
for k=1:3
    conditionLabelImage(:,:,k,:)=k;
end
for k=1:21
    for l=1:21
        spaceLabelImage(k,l,:,:)=(k<11)+(l<11)*2;
    end
end
%This uses a 2-factor repeated measures ANOVA but with a constant second
%factor, just because the implementation of this function is much more
%efficient for mass univariate analysis than standard Matlab functions.
OrientAnova=rm_anova2multi(reshape(NalltrialsMean,[],3*18)',reshape(subjectLabelImage(1,1,:,:),[],1),reshape(conditionLabelImage(1,1,:,:),[],1),reshape(conditionLabelImage(1,1,:,:),[],1)*0+1,{'Condition' 'Constant'});
for k=1:3
%     [~,ptemp,~,statstemp]=ttest(mean(Nalltrials2(:,:,soi{pairs(k,1)},:),3),mean(Nalltrials2(:,:,soi{pairs(k,2)},:),3),[],[],4);
    [~,ptemp,~,statstemp]=ttest(NalltrialsMean(:,:,pairs(k,1),:),NalltrialsMean(:,:,pairs(k,2),:),[],[],4);
    tmax(k)=ceil(max(abs(statstemp.tstat(:)))*2)/2;
end
% Permutation test for significance
for cperm=1:1000
    for k=1:3
        %Random sign changes should not change statistics, if null is
        %correct (no difference between conditions)
%         x1=Nalltrials(:,:,soi{pairs(k,1)},:);
%         x2=Nalltrials(:,:,soi{pairs(k,2)},:);
%         x1=x1.*((rand(size(x1))>.5)*2-1);
%         x2=x2.*((rand(size(x2))>.5)*2-1);
        shuffle=randperm(18);
        i1=shuffle(1:9);
        i2=shuffle(10:18);
        ordperm=randperm(9);
        soiperm={ordperm(1:2) ordperm(3:6) ordperm(7:9)};
        x1=cat(4,mean(Nalltrials2(:,:,soiperm{pairs(k,1)},i1),3),mean(Nalltrials2(:,:,soiperm{pairs(k,2)},i2),3));
        x2=cat(4,mean(Nalltrials2(:,:,soiperm{pairs(k,2)},i1),3),mean(Nalltrials2(:,:,soiperm{pairs(k,1)},i2),3));
%         x1=x1.*((rand(size(x1))>.5)*2-1);
%         x2=x2.*((rand(size(x2))>.5)*2-1);
        [~,ptemp,~,statstemp]=ttest(x1,x2,[],[],4);
        minp(cperm,k)=min(nonzeros(ptemp(:)));
        maxt(cperm,k)=max(abs(nonzeros(statstemp.tstat(:))));
    end
end
tthr=quantile(maxt(:),.95)
pthr=quantile(minp(:),.05)
%
pthr=0.05;

for k=1:3
    axpos=[mrg/2+(k-1)*((.9)/4+mrg/2) mrg*2  (h/w/scl-mrg) (1/scl-mrg*2)];
    hax(k+4,1)=axes;
    
%     [~,ptemp,~,statstemp]=ttest(mean(Nalltrials2(:,:,soi{pairs(k,1)},:),3),mean(Nalltrials2(:,:,soi{pairs(k,2)},:),3),[],[],4);
    [~,ptemp,~,statstemp]=ttest(NalltrialsMean(:,:,pairs(k,1),:),NalltrialsMean(:,:,pairs(k,2),:),[],[],4);
%     mean(Nalltrials(:,:,soi{pairs(k,1)},:),3),mean(Nalltrials(:,:,soi{pairs(k,2)},:),3)
    cmapT=parula(128);
    
    temp=min(abs(statstemp.tstat(ptemp<pthr)));
    if any(temp)
        tstatsMin(k)=temp;
    else
        tstatsMin(k)=inf;
    end
    tstatMat=65+round(statstemp.tstat/max(tmax)*63);
    tstatMat(isnan(tstatMat))=1;
    statIm=reshape(cmapT(tstatMat,:),size(ptemp,1),size(ptemp,2),[]);
    image(statIm.*(.5*(ptemp>pthr))+statIm.*((ptemp<pthr)));
    hold on
    contour(reshape(OrientAnova.p(1,:),size(statIm(:,:,1)))<pthr,1,'color',aaltoColors(5),'linewidth',2);
	contour(ptemp<pthr,1,'color',[0 0 0],'linewidth',2);
    contour(ptemp<pthr,1,'color',[1 1 1],'linewidth',1);
    
    %    set(hc1,

    
    set(hax(k+4,1),'position',axpos,'visible','off','ydir','normal');
    
    hax(k+4,2)=axes;
    
    hold on;
    grid on
    axis([-10 90 -10 90])
    set(hax(k+4,2),'position',axpos,'color','none','xtick',-15:15:90,'ytick',-15:15:90);
      
    
    for cc=1:1%4
        probThr=double(probScale(:,:,conOrd(cc))>=max(probScale,[],3));%imresize(imerode(imfilter(imresize(double(probScale(:,:,conOrd(cc))>=max(probScale,[],3)),smscale),gaussKernel([5 5],2)),strel),1/smscale);
        Ccon=contourc(probThr,[.5 .5]);
        lineSeg=find(Ccon(1,:)==.5);
        lineSeg(end+1)=length(Ccon);
        for ll=1:length(lineSeg)-1
            curArea=[Ccon(1,(lineSeg(ll)+1):(lineSeg(ll+1)-1))-10;Ccon(2,(lineSeg(ll)+1):(lineSeg(ll+1)-1))-10];
%             curArea(:,end+1)=[mean(curArea')*100]-10;
            plot(curArea(1,:),curArea(2,:),'k','linewidth',2);
            plot(curArea(1,:),curArea(2,:),'color',aaltoColors(colOrd(cc)));
        end
    end
    title(titlesPaired{k});
    drawnow;
end

% Scatter plot of percentage of total samples classified as eye contact vs.
%manually annotated percentage
k=4;
axpos=[mrg/2+(k-1)*((.9)/4+mrg/2) mrg*2  (h/w/scl-mrg) (1/scl-mrg*2)];
hax(k+4,1)=axes;
set(hax(k+4,1),'position',axpos);

hold on

[mn,mi]=max(reshape(probPlane,[],4)');
labelIm=reshape(mi,101,101);
for subjectNo=1:18
% 	trialWins=logical(max(tWinAll{subjectNo}'));
    trialWins=true(size(tWinAll{subjectNo}(:,1)));
%     trialWins(:)=true;
    x=min(max(-10,round(relYaw{subjectNo}(trialWins,2))),90)+11;
    y=min(max(-10,round(relYaw{subjectNo}(trialWins,1))),90)+11;
    for t=1:length(x)
        predlabels{subjectNo}(t)=labelIm(y(t),x(t));
        nECraw(subjectNo)=sum(predlabels{subjectNo}==1)/length(predlabels{subjectNo}==1);
    end
end

% for k=1:4
%     plot(x(predlabels{subjectNo}==k)-11,y(predlabels{subjectNo}==k)-11,'o','markerfacecolor',aaltoColors(colOrd(k)),'markeredgecolor','none','markersize',5);
% end

% [b,bint] = regress(y,X);
% xval = min(nEC):0.01:max(nEC);
% yhat = b(1)+b(2)*xval;
% ylow = bint(1,1)+bint(2,1)*xval;
% yupp = bint(1,2)+bint(2,2)*xval;

y=nECraw(:);
x=nEC(:);
X=linspace(min(x),max(x),100);

%sort the data so the plotting works nicely
[~,xidx]=sort(x,'ascend');
x=x(xidx);
y=y(xidx);

[p,s]=polyfit(x,y,1);
[yfit,dy]=polyconf(p,X,s,'predopt','curve');

plot(nEC,nECraw,'o','markerfacecolor',aaltoColors(5),'markeredgecolor',[0 0 0]);
hold on;
plot(X,yfit,'k','linewidth',2);
plot(X,yfit-dy,'color',aaltoColors(5));
plot(X,yfit+dy,'color',aaltoColors(5));
text(.17,.2,sprintf('r=%.2f',corr(nEC(:),nECraw(:))));
axis([0 max(x) 0 max(yfit+dy)]);
xlabel('% eye contact, manually annotated');
ylabel('% eye contact, full data');

% Panel labels on an invisible axis on top of the figure
AxesH = axes('parent',hfig,'position', [0, 0, 1, 1],'Visible', 'off','XLim', [0, 1],'YLim', [0, 1],'NextPlot', 'add');
hold on
panelNames={'A' 'B' 'C' 'D' 'E' 'F' 'G' 'H' 'I'};
for k=1:4
    axpos=[mrg/2+(k-1)*((.9)/4+mrg/2) mrg+.5  (h/w/2-mrg) (1/2-mrg*2)];
    text((axpos(1)-mrg/2.5),(axpos(2)+.5-mrg*1.4),panelNames{k},'fontsize',12,'fontweight','bold');
    axpos=[mrg/2+(k-1)*((.9)/4+mrg/2) mrg  (h/w/2-mrg) (1/2-mrg*2)];
    text((axpos(1)-mrg/2.5),(axpos(2)+.5-mrg*1.4),panelNames{k+4},'fontsize',12,'fontweight','bold');
end

mv=.02;
text(.03,.53,'Class probabilities','fontweight','bold')
text(.1+mv,.56,'Eye contact')
text(.18+mv,.56,'Joint attention')
text(.13+mv,.49,'Other-oriented gaze')


hghts=linspace(.57,.5,4)-.005;
text(.45,hghts(1),'Eye contact');
text(.45,hghts(2),'Joint attention');
text(.45,hghts(3),'Other-oriented gaze');
text(.45,hghts(4),'Other');
for k=1:4
    plot(.55,hghts(k),'o','markerfacecolor',aaltoColors(colOrd(k)),'markeredgecolor',[0 0 0]);
    plot([.41 .43],hghts(k)*[1 1],'k','linewidth',3);
        plot([.41 .43],hghts(k)*[1 1],'color',aaltoColors(colOrd(k)),'linewidth',1.5);
end

text(.3,.53,'Winning class regions','fontweight','bold')
text(.57,.53,'Annotated gaze','fontweight','bold')
htt=text(.122,mrg/2.5,'T-value','HorizontalAlignment','center')

plot(mrg/2+((.9)/4+mrg/2)+[0 .02],mrg*[1 1],'k','linewidth',3);
plot(mrg/2+((.9)/4+mrg/2)+[0 .02],mrg*[1 1],'w','linewidth',1.5);
htt=text(mrg/2+((.9)/4+mrg/2)+.05,mrg,'Group difference, p<0.05')
        
[im,~,alpha]=imread([baseDir '/include/classifier_colormap_notext.png']);
haxCmap=axes;
him=image(im);
set(him,'AlphaData',alpha)
axis image;
set(haxCmap,'position',[.135+mv .4965 .06 .06],'visible','off');%,'xtick','off','ytick','off')

haxCbar=axes;
cbarIm=permute(repmat(parula(128),[1 1 10]),[3 1 2]);
scale=linspace(-max(tmax),max(tmax),length(cbarIm));
cbarIm(:,abs(scale)<min(tstatsMin),:)=.5*cbarIm(:,abs(scale)<min(tstatsMin),:);
image(cbarIm)
set(haxCbar,'position',[mrg/2 mrg  (h/w/scl-mrg) .02],'xtick',linspace(1,length(cbarIm),length(-max(tmax):.5:max(tmax))),'xticklabel',double(-max(tmax):.5:max(tmax)),'ytick',[])


%% Behavioral and personal variables, tables
rateTitles2={'Relatedness'
    'Enjoyment'
    'Choice'
    'Pressure'
    'Effort'
    'ACIPS'
    'AQ'
    'Age'};
[rateCors,pvals]=corr((rate1+rate2)'/2);
fid=fopen([baseDir '/behavioralRatingCors.csv'],'w');
fprintf('-------------------------------------------------------------------------------------------------------\n');
% rthr=probabilityToCorrelation(36,1-(.05/(8*7/2)));
fprintf(fid,',%s,%s,%s,%s,%s,%s,%s,%s\n',rateTitles2{:});
fprintf('\t\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n',rateTitles2{:});

% sigr=(abs(rateCors-eye(size(rateCors)))>rthr);
fprintf('-------------------------------------------------------------------------------------------------------\n');
for k=1:8
    fprintf(fid,'%s,',rateTitles2{k});
    fprintf('%s',rateTitles2{k});
    if length(rateTitles2{k})<7
        fprintf('\t');
    end
    
    for l=1:8
        if l>1 && length(rateTitles2{l-1})>6
            fprintf('\t');
        end
        fprintf(fid,'%.3f',rateCors(k,l));
        
        fprintf('\t%.3f',rateCors(k,l));
        if pvals(k,l)<(.05/(8*7/2))
            fprintf(fid,'**');
            fprintf('**');
        elseif pvals(k,l)<(.05)
            fprintf(fid,'*');
            fprintf('*');
        end
        if l<8
            fprintf(fid,',');
        end
    end
    fprintf(fid,'\n');
    fprintf('\n');
end
fclose(fid);
fprintf('-------------------------------------------------------------------------------------------------------\n');
fprintf('\n');

%% Write the table of subjective ratings of the interaction etc
fid=fopen([baseDir '/behavioralRatingsAndSTDEVs.csv'],'w');
fprintf('----------------------------------------------------------------------------------------------------------------------------------------------\n');
% rthr=probabilityToCorrelation(36,1-(.05/(8*7/2)));
fprintf(fid,',%s,%s,%s,%s,%s,%s,%s,%s\n',rateTitles2{:});
fprintf('\t\t%s\t%s\t%s\t\t%s\t%s\t\t%s\t\t%s\t\t%s\n',rateTitles2{:});

% sigr=(abs(rateCors-eye(size(rateCors)))>rthr);
fprintf('----------------------------------------------------------------------------------------------------------------------------------------------\n');
fprintf('Mean±std\t');
fprintf(fid,'Mean±std,');
[rateCors,pvals]=corr(rate1',rate2');
for k=1:8
    fprintf(fid,'%.1f±%.1f',mean([rate1(k,:) rate2(k,:)]),std([rate1(k,:) rate2(k,:)]));
    fprintf('%.1f±%.1f',mean([rate1(k,:) rate2(k,:)]),std([rate1(k,:) rate2(k,:)]));
    if k<8
        fprintf(fid,',');
        fprintf('\t');
    else
        fprintf(fid,'\n');
        fprintf('\n');
    end
end
fprintf('Min/Max\t');
fprintf(fid,'Min/Max,');
for k=1:8
    fprintf(fid,'%.1f/%.1f',min([rate1(k,:) rate2(k,:)]),max([rate1(k,:) rate2(k,:)]));
    fprintf('%.1f/%.1f',min([rate1(k,:) rate2(k,:)]),max([rate1(k,:) rate2(k,:)]));
    if k<8
        fprintf(fid,',');
        fprintf('\t');
    else
        fprintf(fid,'\n');
        fprintf('\n');
    end
end

fprintf(fid,'r,');
fprintf('r\t');
for k=1:8
    fprintf(fid,'%.2f',rateCors(k,k));
    fprintf('%.2f',rateCors(k,k));
    
    if k<8
        fprintf(fid,',');
        fprintf('\t');
    else
        fprintf(fid,'\n');
        fprintf('\n');
    end
end

fprintf(fid,'p,');
fprintf('p\t');
for k=1:8
    fprintf(fid,'%.4f',pvals(k,k));
    fprintf('%.4f',pvals(k,k));
    
    fprintf(fid,',');
    fprintf('\t');
end
fclose(fid);
fprintf('\n');
fprintf('----------------------------------------------------------------------------------------------------------------------------------------------\n');
fprintf('\n');

%% Write the table of correlations between subjective ratings and personal characteristics
% These do not seem to contribute much to our understanding beyond the two
% tables in the paper, so they are not reported (apart from the diagonal
% entries included in table 1).
rateTitles2={'Relatedness'
    'Enjoyment'
    'Choice'
    'Pressure'
    'Effort'
    'ACIPS'
    'AQ'
    'Age'};
[rateCors,pvals]=corr(rate1',rate2');
fid=fopen([baseDir '/behavioralRatingDyadCors.csv'],'w');
fprintf('-------------------------------------------------------------------------------------------------------\n');
% rthr=probabilityToCorrelation(36,1-(.05/(8*7/2)));
fprintf(fid,',%s,%s,%s,%s,%s,%s,%s,%s\n',rateTitles2{:});
fprintf('\t\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n',rateTitles2{:});

% sigr=(abs(rateCors-eye(size(rateCors)))>rthr);
fprintf('-------------------------------------------------------------------------------------------------------\n');
for k=1:8
    fprintf(fid,'%s,',rateTitles2{k});
    fprintf('%s',rateTitles2{k});
    if length(rateTitles2{k})<7
        fprintf('\t');
    end
    
    for l=1:8
        if l>1 && length(rateTitles2{l-1})>6
            fprintf('\t');
        end
        fprintf(fid,'%.3f',rateCors(k,l));
        
        fprintf('\t%.3f',rateCors(k,l));
        if pvals(k,l)<(.05/(8*7/2))
            fprintf(fid,'**');
            fprintf('**');
        elseif pvals(k,l)<(.05)
            fprintf(fid,'*');
            fprintf('*');
        end
        if l<8
            fprintf(fid,',');
        end
    end
    fprintf(fid,'\n');
    fprintf('\n');
end
fclose(fid);
fprintf('-------------------------------------------------------------------------------------------------------\n');
fprintf('\n');
%% Write the table of correlations between difference vs. mean subjective ratings and personal characteristics
% These do not contribute to our understanding of the results in the
% current study, so they are omitted. These mayd be interesting for the
% "misattunement hypothesis" in future if e.g. relatedness being negatively
% correlated with difference of social traits (ACIPS).
rateTitles2={'Relatedness'
    'Enjoyment'
    'Choice'
    'Pressure'
    'Effort'
    'ACIPS'
    'AQ'
    'Age'};
[rateCors,pvals]=corr(abs(rate1'-rate2'),rate1'+rate2');
fid=fopen([baseDir '/behavioralRatingDyadDiffVsMeanCors.csv'],'w');
fprintf('-------------------------------------------------------------------------------------------------------\n');
% rthr=probabilityToCorrelation(36,1-(.05/(8*7/2)));
fprintf(fid,',%s,%s,%s,%s,%s,%s,%s,%s\n',rateTitles2{:});
fprintf('\t\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n',rateTitles2{:});

% sigr=(abs(rateCors-eye(size(rateCors)))>rthr);
fprintf('-------------------------------------------------------------------------------------------------------\n');
for k=1:8
    fprintf(fid,'%s,',rateTitles2{k});
    fprintf('%s diff.',rateTitles2{k});
%     if length(rateTitles2{k})<7
%         fprintf('\t');
%     end
    
    for l=1:8
%         if l>1 && length(rateTitles2{l-1})>6
%             fprintf('\t');
%         end
        fprintf(fid,'%.3f',rateCors(k,l));
        
        fprintf('\t%.3f',rateCors(k,l));
        if pvals(k,l)<(.05/(8*7/2))
            fprintf(fid,'**');
            fprintf('**');
        elseif pvals(k,l)<(.05)
            fprintf(fid,'*');
            fprintf('*');
        end
        if l<8
            fprintf(fid,',');
        end
    end
    fprintf(fid,'\n');
    fprintf('\n');
end
fclose(fid);
fprintf('-------------------------------------------------------------------------------------------------------\n');
fprintf('\n');