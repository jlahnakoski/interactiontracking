%Run zitternix_paper_analyses.m first (at least the main loop of it)
clear
addpath('/home/juha/matlab-codes');
addpath('/home/juha/matlab-codes/aalto');
dataDir='/run/media/juha/ADOS_backup';
cd(dataDir);
load('gazedata_with_correct_time_windows_reliability.mat','gazedata');
gazedatarel=gazedata;
load('timeWindowsWihtManualAdjustment.mat'); %These are the corrected timestamps! Add the to other analyses!!!
load('gazedata_with_correct_time_windows.mat');%This is the old data because we want to use the same timepoints
gazedataorig=gazedata;
load('ZitternixRegisteredData.mat','d','dataAll','ord','twinFull','gazedatafull');
tWinAll=tWinAllNew;
ordNum=1;
gzreli=[];
fullLabels1=[];
fullLabels2=[];
for ordNum=ordNum:length(d)%2:2%length(d)
    subjectNo=ord(ordNum);
    
    ec=and(gazedata{subjectNo}(3,:),gazedata{subjectNo}(4,:));
    ec2=and(gazedatarel{subjectNo}(3,:),gazedatarel{subjectNo}(4,:));
    ja=and(gazedata{subjectNo}(5,:),gazedata{subjectNo}(6,:));
    ja2=and(gazedatarel{subjectNo}(5,:),gazedatarel{subjectNo}(6,:));
    oog=or(and(gazedata{subjectNo}(3,:),gazedata{subjectNo}(6,:)),and(gazedata{subjectNo}(4,:),gazedata{subjectNo}(5,:)));
    oog2=or(and(gazedatarel{subjectNo}(3,:),gazedatarel{subjectNo}(6,:)),and(gazedatarel{subjectNo}(4,:),gazedatarel{subjectNo}(5,:)));
    away=~or(or(ec,ja),oog);
    away2=~or(or(ec2,ja2),oog2);
    fullLabels1=cat(1,fullLabels1,[ec(:) ja(:) oog(:) away(:)]);
    fullLabels2=cat(1,fullLabels2,[ec2(:) ja2(:) oog2(:) away2(:)]);
    subLabelsInclude{subjectNo}=min([ec(:) ja(:) oog(:) away(:)]==[ec2(:) ja2(:) oog2(:) away2(:)],[],2)==0;
    gzreli(:,subjectNo)=[sum(and(ec,ec2))/sum(or(ec,ec2)) ...
                 sum(and(ja,ja2))/sum(or(ja,ja2)) ...
                 sum(and(oog,oog2))/sum(or(oog,oog2)) ...
                 sum(and(away,away2))/sum(or(away,away2))];
	gzDice(:,subjectNo)=[dice(ec,ec2) dice(ja,ja2) dice(oog,oog2) dice(away,away2)];
	gzPrc(:,subjectNo,1)=[mean(ec) mean(ja) mean(oog) mean(away)];
    gzPrc(:,subjectNo,2)=[mean(ec2) mean(ja2) mean(oog2) mean(away2)];
end

gzFullDice=[dice(fullLabels1(:,1),fullLabels2(:,1))...
            dice(fullLabels1(:,2),fullLabels2(:,2))...
            dice(fullLabels1(:,3),fullLabels2(:,3))...
            dice(fullLabels1(:,4),fullLabels2(:,4))];
figure;
for k=1:4
    subplot(1,4,k);
    plot(mean(gzPrc(k,:,:),3),gzDice(k,:),'o');
end
%
Nall=[];
NallN=[];
ordNum=1;
%%
for ordNum=ordNum:length(d)%2:2%length(d)
    
    subjectNo=ord(ordNum);
    fprintf('---------------------------------------------------------\n');
    fprintf('Dyad %i, %s\n',ordNum,d(subjectNo).name);
    fprintf('---------------------------------------------------------\n');
    %dataAll{subjectNo,1}: %Data for the participant 1 (target for slave Kinect) from slave Kinect
    %dataAll{subjectNo,2}: Data for participant 2 from slave Kinect
    %dataAll{subjectNo,3}: Data for participant 2 from master Kinect (target of master) registered to Slave coordinate system
    %In columns: Host time, head X,Y,Z, all joints and gaze point X,Y,Z
    %twinFull{subjectNo}: Linearly spaced time points for interpolation
    %twinFullKin{subjectNo}: Time points in Kinect time for loading video
    %frames (two columns, first for master second for slave)
    
    %List saved video frame timestamps for Master Kinect
    dtemp=dir(sprintf('%s/zitternixSubjects/%s/Master/%s*',dataDir,d(subjectNo).name,d(subjectNo).name));%dir(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Master/%s*',d(subjectNo).name,d(subjectNo).name));
    dtemp(~[dtemp(:).isdir])=[];
    dfrm1=dir(sprintf('%s/ADOS_Master/%s/*.png',dataDir,dtemp.name));%dir(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Master/%s/*.png',d(subjectNo).name,dtemp.name));
    nfrm1=zeros(length(dfrm1),1);
    for k=1:length(dfrm1)
        nfrm1(k)=str2double(dfrm1(k).name(1:end-4));
    end
    nfrm1=sort(nfrm1,'ascend');
    [hdrMast{subjectNo},t0mast{subjectNo},t02mast{subjectNo}]=readADOSheader(sprintf('%s/zitternixSubjects/%s/Master/%s.txt',dataDir,d(subjectNo).name,dtemp.name));
    
    %List saved video frame timestamps for Slave Kinect
    dtemp=dir(sprintf('%s/zitternixSubjects/%s/Slave/%s*',dataDir,d(subjectNo).name,d(subjectNo).name));
    dtemp(~[dtemp(:).isdir])=[];
    dfrm2=dir(sprintf('%s/ADOS_Slave/%s/*.png',dataDir,dtemp.name));%dfrm2=dir(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Slave/%s/*.png',d(subjectNo).name,dtemp.name));
    nfrm2=zeros(length(dfrm2),1);
    for k=1:length(dfrm2)
        nfrm2(k)=str2double(dfrm2(k).name(1:end-4));
    end
    nfrm2=sort(nfrm2,'ascend');
    [hdr{subjectNo},t0slav{subjectNo},t02slav{subjectNo}]=readADOSheader(sprintf('%s/zitternixSubjects/%s/Slave/%s.txt',dataDir,d(subjectNo).name,dtemp.name));
    
    %Select a random sample of the video frames, 50 from conversation
    %tasks and 50 from Zitternix?
    lenVid=min(nfrm2(end)-nfrm2(1),nfrm1(end)-nfrm1(1));
    
    conversation{subjectNo}=find(max(tWinAll{subjectNo}(:,[1 2]),[],2)); %Remember that conditions have been reorganized (conv1, conv2, practice, zitternix1...)
    cooperative{subjectNo}=find(max(tWinAll{subjectNo}(:,4:6),[],2));
    competitive{subjectNo}=find(max(tWinAll{subjectNo}(:,7:9),[],2));
    %Randomize the order, so we sample randomly from different timepoints
%     conversation{subjectNo}=conversation{subjectNo}(randperm(length(conversation{subjectNo})));
%     cooperative{subjectNo}=cooperative{subjectNo}(randperm(length(cooperative{subjectNo})));
%     competitive{subjectNo}=competitive{subjectNo}(randperm(length(competitive{subjectNo})));
%     tSample=twinFull{subjectNo}([conversation{subjectNo}(1:50); cooperative{subjectNo}(1:50);competitive{subjectNo}(1:50)]);% round(rand(100,1)*(lenVid-1)+1);%linspace(1,lenVid,50);%lenVid/3000);
%     
%     condition=[ones(50,1);ones(50,1)*2;ones(50,1)*3];
% %     tSample=tSample*100; %Multiply by 100 to get milliseconds since 
% %     tSample=tSample*100; %Multiply by 100 to get milliseconds since the thing has been resampled at 10Hz
%     %Randomize the order again to avoid bias
%     randord=randperm(length(tSample));
%     tSample=tSample(randord);
%     condition=condition(randord);
    tSample=gazedata{subjectNo}(19,:);
    condition=gazedata{subjectNo}(22,:);
    %%
    %Let's interpolate the data to the video frame timestamps, seems like a
    %fabulous idea.. let's see if it works
    targetLoc{subjectNo}=[mean(dataAll{subjectNo,1}(2,:)) 0 mean(dataAll{subjectNo,3}(4,:))];%[-.5 0 2.3];
    
    %Temporary timecourse variables for calculating distances etc.
    XYZ2=interp1(dataAll{subjectNo,3}(1,:),dataAll{subjectNo,3}([2:4 80:82],:)',...
        twinFull{subjectNo})';
    XYZ1=interp1(dataAll{subjectNo,1}(1,:),dataAll{subjectNo,1}([2:4 80:82],:)',...
        twinFull{subjectNo})';
    XYZn=interp1(dataAll{subjectNo,2}(1,:),dataAll{subjectNo,2}([2:4 80:82],:)',...
        twinFull{subjectNo})';
    
    %Little Gaussian filter with 0.3-second sigma
    for kj=1:6
        XYZ1(kj,:)=conv(XYZ1(kj,:),gaussKernel(27,3),'same');
        XYZ2(kj,:)=conv(XYZ2(kj,:),gaussKernel(27,3),'same');
        XYZn(kj,:)=conv(XYZn(kj,:),gaussKernel(27,3),'same');
    end
    
    %Distances between participants at all timepoints based on both sensor
    %estimates of the location of participant 2
    distance{subjectNo}=zeros(length(XYZ1),2);
    distance{subjectNo}(:,1)=sqrt(mean((XYZ1(1:3,:)-XYZ2(1:3,:)).^2));
    distance{subjectNo}(:,2)=sqrt(mean((XYZ1(1:3,:)-XYZn(1:3,:)).^2));
    %
    %Angles for participant 1
    % 	ang12=atand((XYZ2(1,:)-XYZ1(1,:))./-(XYZ2(3,:)-XYZ1(3,:)));
    %     ang1=atand((XYZ1(4,:)-XYZ1(1,:))./-(XYZ1(6,:)-XYZ1(3,:)));
    %     ang1a=ang12-ang1;
    ang12wrap=atan2d(XYZ2(1,:)-XYZ1(1,:),-(XYZ2(3,:)-XYZ1(3,:)));
    ang12=ang12wrap+360*(ang12wrap<0);
    ang21=180+ang12;
    
    %Gaze angle of participant 1 in Kinect coordinates and in relation tofigure;plot(dataKin(90,:),dataKin(7,:),'.')
    %participant 2
    ang1wrap=atan2d(XYZ1(4,:)-XYZ1(1,:),(XYZ1(6,:)-XYZ1(3,:)));
    ang1=180-(ang1wrap+360*(ang1wrap<0));%+90;
    ang1a=ang12-ang1;%ang2+(ang2<0)*180-ang12;
    
    %Angles for participant 2, because of the orientation the calculation
    %differs somewhat from participant 1
    ang2wrap=atan2d(XYZ2(4,:)-XYZ2(1,:),-(XYZ2(6,:)-XYZ2(3,:)));
    ang2=ang2wrap+360*(ang2wrap<0);%+90;
    ang2a=ang2-ang21;%Reverse the order (sign) because the angles are mostly negative %ang2+(ang2<0)*180-ang12;
    
    %Angles from participants to target
    %     ang1t=atand((targetLoc{subjectNo}(1)-XYZ1(1,:))./-(targetLoc{subjectNo}(3)-XYZ1(3,:)));
    %     ang2t=atand((targetLoc{subjectNo}(1)-XYZ2(1,:))./-(targetLoc{subjectNo}(3)-XYZ2(3,:)));
    ang1twrap=atan2d(targetLoc{subjectNo}(1)-XYZ1(1,:),targetLoc{subjectNo}(3)-XYZ1(3,:)) ;
    ang1t=180-(ang1twrap+360*(ang1twrap<0));%+90;
    %     ang1at=ang12-ang1t;%ang2+(ang2<0)*180-ang12;
    
    ang2twrap=atan2d(targetLoc{subjectNo}(1)-XYZ2(1,:),-(targetLoc{subjectNo}(3)-XYZ2(3,:)));
    ang2t=ang2twrap+360*(ang2twrap<0);%+90;
    
    
    
    %Angle from participant gaze direction to target direction
    ang1at=ang1-ang1t;
    ang2at=ang2-ang2t;%+(ang2<0)*180-(180-ang2t);
    
    %Normalize the angle range between 0 (partner) and 1 (target)
    ang1T=abs(ang1t-ang12); %Total angle between partner and target
    ang2T=abs(ang2t-ang21);
    ang1N=ang1a./ang1T;
    ang2N=ang2a./ang2T;
    
    relYaw{subjectNo}=[ang1a(:) ang2a(:)];
    relYawNorm{subjectNo}=[ang1N(:) ang2N(:)];
    
    ptch1=atand((XYZ1(5,:)-XYZ1(2,:))./sqrt((XYZ1(6,:)-XYZ1(3,:)).^2+(XYZ1(4,:)-XYZ1(1,:)).^2));
    ptch2=atand((XYZ2(5,:)-XYZ2(2,:))./sqrt((XYZ2(6,:)-XYZ2(3,:)).^2+(XYZ2(4,:)-XYZ2(1,:)).^2));
    
    pitchSimple{subjectNo}=[ptch1(:) ptch2(:)];
    
    [N,Ca]=hist3([ang1a(:) ang2a(:)],{-30:5:90 -30:5:90});
    Nall(:,:,subjectNo)=N/sum(N(:));
    
    [Nn,Cn]=hist3([ang1N(:) ang2N(:)],{-1:.125:3 -1:.125:3});
    NallN(:,:,subjectNo)=Nn/sum(Nn(:));
    
    for trial=1:9
        ctrial=logical(tWinAll{subjectNo}(:,trial));
        [Ntrials(:,:,trial),Ctrials(:,:,trial)]=hist3([ang1a(ctrial)' ang2a(ctrial)'],{-30:5:90 -30:5:90});
        Nalltrials(:,:,trial,subjectNo)=Ntrials(:,:,trial)/sum(sum(Ntrials(:,:,trial)));
        
        [Nntrials(:,:,trial),Cntrials(:,:,trial)]=hist3([ang1N(ctrial)' ang2N(ctrial)'],{-1:.125:3 -1:.125:3});
        Nnalltrials(:,:,trial,subjectNo)=Nntrials(:,:,trial)/sum(sum(Nntrials(:,:,trial)));
    end
    
    dataAll{subjectNo,3}(83,:)=interp1(twinFull{subjectNo},ang2a,dataAll{subjectNo,3}(1,:));
    dataAll{subjectNo,1}(83,:)=interp1(twinFull{subjectNo},ang1a,dataAll{subjectNo,1}(1,:));
    dataAll{subjectNo,3}(84,:)=interp1(twinFull{subjectNo},ang2N,dataAll{subjectNo,3}(1,:));
    dataAll{subjectNo,1}(84,:)=interp1(twinFull{subjectNo},ang1N,dataAll{subjectNo,1}(1,:));
    dataAll{subjectNo,3}(85,:)=interp1(twinFull{subjectNo},pitchSimple{subjectNo}(:,1),dataAll{subjectNo,3}(1,:));
    dataAll{subjectNo,1}(85,:)=interp1(twinFull{subjectNo},pitchSimple{subjectNo}(:,2),dataAll{subjectNo,1}(1,:));
    dataAtFrames{subjectNo,1}=interp1(dataAll{subjectNo,3}(1,:),dataAll{subjectNo,3}',nfrm1-t02mast{subjectNo});
    dataAtFrames{subjectNo,2}=interp1(dataAll{subjectNo,1}(1,:),dataAll{subjectNo,1}',nfrm2-t02slav{subjectNo});

    
    %%
    hfig=figure;
    hax=axes;
    him=image(zeros(180,640,3));
    
    set(hfig,'position',[(1920-(640*2+360))/2 1200-(180*2)-100 640*2+360 180*2]);
    set(hax,'position',[0 0 (640*2)/(640*2+360) 1]);
    hax2=axes;
    set(hax2,'position',[(640*2)/(640*2+360)+((360)/(640*2+360)*.1) .1 (360)/(640*2+360)*.8 .8]);
    hplt=plot(nan,nan);
    axis([-30 90 -30 90]);  
%     axis([-30 90 -30 90]);
    hold on;
    grid on
    t=1;
    distTemp1=abs(nfrm1-(tSample(t)+t02mast{subjectNo}));
    distTemp2=abs(nfrm2-(tSample(t)+t02slav{subjectNo}));
    t1=find(distTemp1==min(distTemp1),1);
    t2=find(distTemp2==min(distTemp2),1);
%     im1=imread(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Master/%s/%i.png',d(subjectNo).name,dtemp.name,nfrm1(t1)));
%     im2=imread(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Slave/%s/%i.png',d(subjectNo).name,dtemp.name,nfrm2(t2)));
    im1=imread(sprintf('%s/ADOS_Master/%s/%i.png',dataDir,dtemp.name,nfrm1(t1)));
    im2=imread(sprintf('%s/ADOS_Slave/%s/%i.png',dataDir,dtemp.name,nfrm2(t2)));
    set(him,'cdata',cat(2,im1,im2));
    
    t=0;
    %%
    while t<length(tSample)
        t=t+1;
        fprintf('Sample %i:\n',t);
        distTemp1=abs(nfrm1-(tSample(t)+t02mast{subjectNo}));
        distTemp2=abs(nfrm2-(tSample(t)+t02slav{subjectNo}));
        t1=find(distTemp1==min(distTemp1),1);
        t2=find(distTemp2==min(distTemp2),1);
        im1=imread(sprintf('%s/ADOS_Master/%s/%i.png',dataDir,dtemp.name,nfrm1(t1)));
        im2=imread(sprintf('%s/ADOS_Slave/%s/%i.png',dataDir,dtemp.name,nfrm2(t2)));
%         im1=imread(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Master/%s/%i.png',d(subjectNo).name,dtemp.name,nfrm1(t1)));
%         im2=imread(sprintf('/nas/Juha/ADOS_zitternix/Subjects/%s/Slave/%s/%i.png',d(subjectNo).name,dtemp.name,nfrm2(t2)));
        set(him,'cdata',cat(2,im1,im2));
        hp=plot(dataAtFrames{subjectNo,1}(t1,83),dataAtFrames{subjectNo,2}(t2,83),'ko');
%         hp=plot(dataAtFrames{subjectNo,1}(t1,84),dataAtFrames{subjectNo,2}(t2,84),'ko');
        gazedata{subjectNo}(1:2,t)=[dataAtFrames{subjectNo,1}(t1,83),dataAtFrames{subjectNo,2}(t2,83)];
% %         gazedata{subjectNo}(3,t)=input('Subject 1 at partner? ');
% %         gazedata{subjectNo}(4,t)=input('Subject 2 at partner? ');
% %         gazedata{subjectNo}(5,t)=input('Subject 1 at Zitternix? ');
% %         gazedata{subjectNo}(6,t)=input('Subject 2 at Zitternix? ');
        
        %If the two earlier raters did not agree on something, the third
        %one is asked to solve the stalemate..
        if subLabelsInclude{subjectNo}(t) 
            tempdata=input('S1->S2,S2->S1,S1->Target,S2->target? (hit "-" to go back to correct a mistake)\n','s');
            if length(tempdata)<4
                if strcmp(tempdata(1),'-') %If the first input was a minus sign, move to the previous frame
                    t=t-2;
                else %if the input was too short, ask for it again
                    tempdata=input('Failed, please re-enter: S1->S2,S2->S1,S1->Target,S2->target?\n','s');
                end
            else
                gazedata{subjectNo}(3,t)=str2num(tempdata(1));
                gazedata{subjectNo}(4,t)=str2num(tempdata(2));
                gazedata{subjectNo}(5,t)=str2num(tempdata(3));
                gazedata{subjectNo}(6,t)=str2num(tempdata(4));
            end
            gazedata{subjectNo}(7:12,t)=[dataAtFrames{subjectNo,1}(t1,4:6),dataAtFrames{subjectNo,2}(t2,4:6)];
            gazedata{subjectNo}(13:18,t)=[dataAtFrames{subjectNo,1}(t1,80:82),dataAtFrames{subjectNo,2}(t2,80:82)];
            gazedata{subjectNo}(19,t)=tSample(t);
            gazedata{subjectNo}(20:21,t)=[t1 t2];
            gazedata{subjectNo}(22,t)=condition(t);
            gazedata{subjectNo}(23:24,t)=[dataAtFrames{subjectNo,1}(t1,83),dataAtFrames{subjectNo,2}(t2,83)];%non-normalized yaw
            gazedata{subjectNo}(25:26,t)=[dataAtFrames{subjectNo,1}(t1,85),dataAtFrames{subjectNo,2}(t2,85)];%pitch
            gazedataFull{subjectNo,1}(:,t)=dataAtFrames{subjectNo,1}(t1,:);
            gazedataFull{subjectNo,2}(:,t)=dataAtFrames{subjectNo,2}(t2,:);
        end
        
        if min(gazedata{subjectNo}(3:4,t))==1
            set(hp,'markeredgecolor',aaltoColors(2),'marker','*');
        elseif max(gazedata{subjectNo}(3:4,t))==1 && max(gazedata{subjectNo}(5:6,t))==1
            set(hp,'markeredgecolor',aaltoColors(6),'marker','*');
        elseif min(gazedata{subjectNo}(5:6,t))==1
            set(hp,'markeredgecolor',aaltoColors(3),'marker','*');
        else
            set(hp,'markeredgecolor',aaltoColors(5));
        end
        drawnow;
%         pause(1/30)
    end
%     set(hfig,'position',[500 500 500 500]);
%     delete(hax);
%     delete(hax2);
    %histograms of relative locations for each gaze combination (partner vs
    %zitternix vs mix)
    [N1,C1]=hist3(gazedata{subjectNo}(1:2,and(gazedata{subjectNo}(3,:),gazedata{subjectNo}(4,:)))',{-10:5:90 -10:5:90});
    [N2,C2]=hist3(gazedata{subjectNo}(1:2,and(gazedata{subjectNo}(5,:),gazedata{subjectNo}(6,:)))',{-10:5:90 -10:5:90});
    [N3,C3]=hist3(gazedata{subjectNo}(1:2,or(and(gazedata{subjectNo}(3,:),gazedata{subjectNo}(6,:)),and(gazedata{subjectNo}(4,:),gazedata{subjectNo}(5,:))))',{-10:5:90 -10:5:90});
    [N4,C4]=hist3(gazedata{subjectNo}(1:2,max(gazedata{subjectNo}(3:6,:))==0)',{-10:5:90 -10:5:90});
    gk=gaussKernel([5 5],1.2);
    N1g=imfilter(N1,gk);
    N2g=imfilter(N2,gk);
    N3g=imfilter(N3,gk);
    N4g=imfilter(N4,gk);
    N1g=N1g/max(N1g(:));
    N2g=N2g/max(N2g(:));
    N3g=N3g/max(N3g(:));
    N4g=N4g/max(N4g(:));
%     imagesc(cat(3,N1g/max(N1g(:)),N2g/max(N2g(:)),N3g));
    gazeDistributions{subjectNo}=cat(3,N1,N2,N3,N4,N1g,N2g,N3g,N4g);
    fprintf('done\n');
    
    close(hfig);
end
%%
save('gazedata_with_correct_time_windows_reliability_third.mat','gazedata','gazedatarel','gazedataorig','gazeDistributions','Ntrials','Ctrials','Nalltrials','Nntrials','Cntrials','Nnalltrials')